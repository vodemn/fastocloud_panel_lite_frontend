**We Respect and Protect Your Privacy**

 - We only collect necessary information

 - We will only collect your usage information for analytic purposes

 - We protect your data

 - You have access to your data

**We don’t Share Customer Information**

FastoCloud does not share customer information of any kind with anyone or any third party. We will
not sell or rent your name or personal information to any third party. We do not sell, rent or
provide outside access to our mailing list or any data we store. Any data that a user stores via
our facilities is wholly owned by that user or business. At anytime a user or business is free to
take their data and leave, or to simply delete their data from our facilities.

**WE PROTECT YOUR INFORMATION**

We implement a variety of security measures to maintain the safety of your personal information
when you place an order. We offer the use of a secure server. After a transaction, your private
information (credit cards, financials, etc.) will not be stored on our servers.

**We Follow Laws**

FastoCloud may release personal information if the partnership is required to by law, search
warrant, subpoena, court order or fraud investigation. We may also use personal information in a
manner that does not identify you specifically nor allow you to be contacted but does identify
certain criteria about our Site’s users in general (such as we may inform third parties about the
number of registered users, number of unique visitors, number of FastoRedis instances linked,
average number of keys by instance, and the pages most frequently browsed, etc.).

**We Talk**

If a customer or potential customer of FastoRedis has any specific questions, concerns or
complaints about our privacy policies or the methods in which we collect the necessary data to run
our business, they are encouraged to contact us and one of our team members will respond promptly
and will work with them to find an amicable resolution to the issue.

**CONTACTING US**

If there are any questions regarding this privacy policy you may contact us using the information
available on the support page.