import 'package:fastocloud_dart_models/models.dart';
import 'package:fastotv_dart/json_rpc.dart';

class ServerAddEvent {
  final Server server;

  ServerAddEvent(this.server);
}

class ServerEditEvent {
  final Server server;

  ServerEditEvent(this.server);
}

class ServerRemoveEvent {
  final Server server;

  ServerRemoveEvent(this.server);
}

class JsonRpcEvent {
  final JsonRpcResponse data;

  JsonRpcEvent(Map<String, dynamic> params) : data = JsonRpcResponse.fromJson(params);
}
