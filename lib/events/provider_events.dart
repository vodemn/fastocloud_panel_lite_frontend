import 'package:fastocloud_dart_models/models.dart';

class ProviderAddEvent {
  final Provider sub;

  ProviderAddEvent(this.sub);
}

class ProviderEditEvent {
  final Provider sub;

  ProviderEditEvent(this.sub);
}

class ProviderRemoveEvent {
  final Provider sub;

  ProviderRemoveEvent(this.sub);
}
