class Partner {
  static const LINK_FIELD = 'link';
  static const LOGO_FIELD = 'logo';
  static const NAME_FIELD = 'name';

  final String link;
  final String logo;
  final String name;

  Partner({required this.name, required this.logo, required this.link});

  factory Partner.fromJson(Map<String, dynamic> json) {
    final link = json[LINK_FIELD];
    final logo = json[LOGO_FIELD];
    final name = json[NAME_FIELD];
    return Partner(name: name, logo: logo, link: link);
  }

  Map<String, dynamic> toJson() {
    return {LINK_FIELD: link, LOGO_FIELD: logo, NAME_FIELD: name};
  }
}
