class PricingType {
  static const int _MONTHLY = 0;
  static const int _ANNUALY = 1;
  static const int _LIFETIME = 2;

  final int _value;

  const PricingType._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    switch (_value) {
      case _MONTHLY:
        return 'Per month';
      case _ANNUALY:
        return 'Per year';
      case _LIFETIME:
        return 'For a lifetime';
    }

    return '';
  }

  static List<PricingType> get values => [MONTHLY, ANNUALY, LIFETIME];

  static const PricingType MONTHLY = PricingType._(_MONTHLY);
  static const PricingType ANNUALY = PricingType._(_ANNUALY);
  static const PricingType LIFETIME = PricingType._(_LIFETIME);
}

class PricingItem {
  final String name;
  final int? monthlyPrice;
  final String? description;

  const PricingItem(this.name, this.monthlyPrice, [this.description]);

  const PricingItem.free(this.name, [this.description]) : monthlyPrice = 0;

  const PricingItem.negotiable(this.name, [this.description]) : monthlyPrice = null;

  const PricingItem.hourly(this.name, [this.description]) : monthlyPrice = null;

  String get monthly => _formatPrice();

  String get annualy => _formatPrice(10);

  String get lifetime => _formatPrice(24);

  String price(PricingType type) {
    switch (type) {
      case PricingType.MONTHLY:
        return monthly;
      case PricingType.ANNUALY:
        return annualy;
      case PricingType.LIFETIME:
        return lifetime;
      default:
        return monthly;
    }
  }

  String _formatPrice([int multiplier = 1]) {
    if (monthlyPrice == null) {
      return 'Negotiable';
    } else if (monthlyPrice == 0) {
      return 'Free';
    } else {
      return '${monthlyPrice! * multiplier}\$';
    }
  }
}

class PricingConsultation extends PricingItem {
  const PricingConsultation(String name, int monthlyPrice) : super(name, monthlyPrice);

  @override
  String price(PricingType type) {
    return '${_formatPrice()} per hour';
  }
}

class PricingPromotion extends PricingItem {
  const PricingPromotion(String name, int monthlyPrice) : super(name, monthlyPrice);

  @override
  String price(PricingType type) {
    return '${_formatPrice()} for 1000 leads, per month';
  }
}
