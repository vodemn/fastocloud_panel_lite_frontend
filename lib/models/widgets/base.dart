import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/models/widgets/save_image.dart';
import 'package:fastotv_dart/commands_info/meta_url.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/widgets.dart';

class HostAndPortTextField extends StatelessWidget {
  final HostAndPort value;

  final String hostHint;
  final String hostError;
  final String portHint;
  final String portError;
  final int ratio;

  const HostAndPortTextField(
      {required this.value,
      this.hostHint = 'Host',
      this.hostError = 'Input host',
      this.portHint = 'Port',
      this.portError = 'Input port',
      this.ratio = 2});

  @override
  Widget build(BuildContext context) {
    final children = <Widget>[Expanded(flex: ratio, child: _host()), Expanded(child: _port())];
    return Row(crossAxisAlignment: CrossAxisAlignment.start, children: children);
  }

  // private:
  Widget _host() {
    return TextFieldEx(
        validator: (String text) {
          return HostAndPort.isValidHost(text) ? null : hostError;
        },
        hintText: hostHint,
        errorText: hostError,
        init: value.host,
        onFieldChanged: (val) {
          value.host = val;
        });
  }

  Widget _port() {
    return NumberTextField.integer(
        hintText: 'Port',
        initInt: value.port,
        canBeEmpty: false,
        onFieldChangedInt: (val) {
          if (val != null) value.port = val;
        });
  }
}

class SizeField extends StatelessWidget {
  final Size value;

  const SizeField(this.value);

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_widthField(), const Text('x'), _heightField()]);
  }

  Widget _widthField() {
    return Expanded(
        child: NumberTextField.integer(
            hintText: 'Width',
            initInt: value.width,
            minInt: Size.MIN_WIDTH,
            onFieldChangedInt: (term) {
              if (term != null) value.width = term;
            }));
  }

  Widget _heightField() {
    return Expanded(
        child: NumberTextField.integer(
            hintText: 'Height',
            initInt: value.height,
            minInt: Size.MIN_HEIGHT,
            onFieldChangedInt: (term) {
              if (term != null) value.height = term;
            }));
  }
}

class PointField extends StatelessWidget {
  final Point value;

  const PointField(this.value);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[_widthField(), _heightField()]);
  }

  Widget _widthField() {
    return Expanded(
        child: NumberTextField.integer(
            hintText: 'X',
            initInt: value.x,
            onFieldChangedInt: (term) {
              if (term != null) value.x = term;
            }));
  }

  Widget _heightField() {
    return Expanded(
        child: NumberTextField.integer(
            hintText: 'Y',
            initInt: value.y,
            onFieldChangedInt: (term) {
              if (term != null) value.y = term;
            }));
  }
}

class AspectRatioField extends StatelessWidget {
  final Rational value;

  const AspectRatioField(this.value);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[_numField(), _denField()]);
  }

  Widget _numField() {
    return Expanded(
        child: NumberTextField.integer(
            hintText: 'Num',
            initInt: value.numerator,
            onFieldChangedInt: (term) {
              if (term != null) value.numerator = term;
            }));
  }

  Widget _denField() {
    return Expanded(
        child: NumberTextField.integer(
            hintText: 'Den',
            initInt: value.denominator,
            onFieldChangedInt: (term) {
              if (term != null) value.denominator = term;
            }));
  }
}

class LogoField extends StatelessWidget {
  final Logo value;

  const LogoField(this.value);

  @override
  Widget build(BuildContext context) {
    final content = <Widget>[pathField(), sizeField(), posField(), alphaField()];
    return Column(mainAxisSize: MainAxisSize.min, children: content);
  }

  Widget pathField() {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        minSymbols: FilePath.MIN_PATH_LENGTH,
        maxSymbols: FilePath.MAX_PATH_LENGTH,
        hintText: 'Path',
        errorText: 'Enter path',
        init: value.path,
        onFieldChanged: (term) {
          value.path = term;
        });
  }

  Widget sizeField() {
    return SizeField(value.size);
  }

  Widget posField() {
    return PointField(value.position);
  }

  Widget alphaField() {
    return NumberTextField.decimal(
        hintText: 'Alpha',
        initDouble: value.alpha,
        minDouble: Alpha.MIN,
        maxDouble: Alpha.MAX,
        onFieldChangedDouble: (term) {
          if (term != null) value.alpha = term;
        });
  }
}

class RsvgLogoField extends StatelessWidget {
  final RsvgLogo value;

  const RsvgLogoField(this.value);

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisSize: MainAxisSize.min, children: <Widget>[pathField(), sizeField(), posField()]);
  }

  Widget pathField() {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        minSymbols: FilePath.MIN_PATH_LENGTH,
        maxSymbols: FilePath.MAX_PATH_LENGTH,
        hintText: 'Path',
        errorText: 'Enter path',
        init: value.path,
        onFieldChanged: (term) {
          value.path = term;
        });
  }

  Widget sizeField() {
    return SizeField(value.size);
  }

  Widget posField() {
    return PointField(value.position);
  }
}

class MetaUrlField extends StatelessWidget {
  final MetaUrl value;
  final void Function() onDelete;

  const MetaUrlField(this.value, this.onDelete);

  @override
  Widget build(BuildContext context) {
    final content = <Widget>[
      Expanded(
          child: Column(
              mainAxisSize: MainAxisSize.min, children: <Widget>[_nameField(), _uriFiled()])),
      _delete()
    ];
    return Row(mainAxisSize: MainAxisSize.min, children: content);
  }

  Widget _nameField() {
    return TextFieldEx(
        hintText: 'Name',
        init: value.name,
        onFieldChanged: (val) {
          value.name = val;
        });
  }

  Widget _uriFiled() {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'Url',
        init: value.url,
        onFieldChanged: (val) {
          value.url = val;
        });
  }

  Widget _delete() {
    return IconButton(tooltip: 'Remove', icon: const Icon(Icons.close), onPressed: onDelete);
  }
}

class MachineLearningField extends StatefulWidget {
  final MachineLearning init;

  const MachineLearningField(this.init);

  @override
  _MachineLearningFieldState createState() {
    return _MachineLearningFieldState();
  }
}

class _MachineLearningFieldState extends State<MachineLearningField> {
  MachineLearning get value => widget.init;

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      _backendField(),
      _modelPathField(),
      _trackingField(),
      _dumpField(),
      _overlayField()
    ]);
  }

  Widget _backendField() {
    return DropdownButtonEx<MlBackend>(
        hint: value.backend.toHumanReadable(),
        value: value.backend,
        values: MlBackend.values,
        onChanged: (c) {
          setState(() {
            value.backend = c!;
          });
        },
        itemBuilder: (MlBackend value) {
          return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
        });
  }

  Widget _modelPathField() {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'Model URL',
        init: value.modelUrl,
        onFieldChanged: (term) {
          value.modelUrl = term;
        });
  }

  Widget _trackingField() {
    return StateCheckBox(
        title: 'Tracking',
        init: value.tracking,
        onChanged: (tracking) {
          value.tracking = tracking;
        });
  }

  Widget _dumpField() {
    return SaveImagesTile(value);
  }

  Widget _overlayField() {
    return StateCheckBox(
        title: 'Overlay',
        init: value.overlay,
        onChanged: (overlay) {
          value.overlay = overlay;
        });
  }
}
