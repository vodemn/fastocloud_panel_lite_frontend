import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/widgets.dart';

class StreamLinkField extends StatefulWidget {
  final PyFastoStream init;

  const StreamLinkField(this.init);

  @override
  _StreamLinkFieldState createState() {
    return _StreamLinkFieldState();
  }
}

class _StreamLinkFieldState extends State<StreamLinkField> {
  PyFastoStream get value => widget.init;

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_preferFiled(), _httpProxyFiled(), _httpsProxyFiled()]);
  }

  Widget _httpProxyFiled() {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'Http proxy',
        init: value.httpProxy,
        onFieldChanged: (val) {
          value.httpProxy = val;
        });
  }

  Widget _httpsProxyFiled() {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'Https proxy',
        init: value.httpsProxy,
        onFieldChanged: (val) {
          value.httpsProxy = val;
        });
  }

  Widget _preferFiled() {
    return DropdownButtonExTypes<int, QualityPrefer>(
        hint: 'Prefer type',
        value: value.prefer.toInt(),
        values: QualityPrefer.values,
        onChanged: (c) {
          setState(() {
            value.prefer = QualityPrefer.fromInt(c!);
          });
        },
        itemBuilder: (QualityPrefer value) {
          return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value.toInt());
        });
  }
}

class AesBits {
  final int _value;

  const AesBits._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == 12) {
      return 'AES-128';
    } else if (_value == 24) {
      return 'AES-192';
    }
    return 'AES-265';
  }

  factory AesBits.fromInt(int value) {
    if (value == 12) {
      return AesBits.AES128;
    } else if (value == 24) {
      return AesBits.AES192;
    }
    return AesBits.AES256;
  }

  static List<AesBits> get values => [AES128, AES192, AES256];

  static const AesBits AES128 = AesBits._(12);
  static const AesBits AES192 = AesBits._(24);
  static const AesBits AES256 = AesBits._(32);
}

class SrtKeyField extends StatefulWidget {
  final SrtKey init;

  const SrtKeyField(this.init);

  @override
  _SrtKeyFieldState createState() {
    return _SrtKeyFieldState();
  }
}

class _SrtKeyFieldState extends State<SrtKeyField> {
  SrtKey get value => widget.init;

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_passFiled(), _keyLenFiled()]);
  }

  Widget _passFiled() {
    return PassWordTextField(
        hintText: 'Passphrase',
        errorText: 'Enter a passphrase',
        init: value.passphrase,
        onFieldChanged: (term) {
          value.passphrase = term;
        });
  }

  Widget _keyLenFiled() {
    return DropdownButtonExTypes<int, AesBits>(
        hint: 'Encryption type',
        value: value.keyLen,
        values: AesBits.values,
        onChanged: (c) {
          setState(() {
            value.keyLen = c!;
          });
        },
        itemBuilder: (AesBits value) {
          return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value.toInt());
        });
  }
}
