import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/sections/common.dart';
import 'package:fastocloud_pro_panel/common/streams/vods/scan_folder_dialog.dart';
import 'package:fastocloud_pro_panel/events/server_events.dart';
import 'package:fastocloud_pro_panel/models/widgets/urls/base.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/widgets.dart';
import 'package:pubsub_common/models.dart';
import 'package:pubsub_common/widgets.dart';

class OutputUrlField<T extends OutputUrl> extends StatefulWidget {
  final T init;
  final void Function()? onDelete;
  final void Function(T url)? onTest;

  const OutputUrlField(this.init, this.onDelete, this.onTest);

  @override
  _OutputFieldState createState() => _OutputFieldState<T, OutputUrlField<T>>();
}

class _OutputFieldState<T extends OutputUrl, S extends OutputUrlField<T>> extends State<S> {
  T get value => widget.init;

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Expanded(child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[uriField()])),
      _delete()
    ]);
  }

  Widget uriField() {
    return Row(mainAxisSize: MainAxisSize.min, children: [
      Expanded(child: rawUrlField()),
      if (widget.onTest != null)
        FlatButtonEx.filled(
            text: 'Test',
            onPressed: () {
              widget.onTest!(value);
            })
    ]);
  }

  TextFieldEx rawUrlField() {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'URL',
        errorText: 'Enter URL',
        init: value.uri,
        onFieldChanged: (term) {
          value.uri = term;
        });
  }

  Widget _delete() {
    if (widget.onDelete == null) {
      return const SizedBox();
    }
    return IconButton(tooltip: 'Remove', icon: const Icon(Icons.close), onPressed: widget.onDelete);
  }
}

class HttpOutputUrlField extends OutputUrlField<HttpOutputUrl> {
  const HttpOutputUrlField(
      HttpOutputUrl uri, void Function()? onDelete, void Function(OutputUrl url)? onTest)
      : super(uri, onDelete, onTest);

  @override
  _HttpOutputFieldState createState() {
    return _HttpOutputFieldState();
  }
}

class _HttpOutputFieldState extends _OutputFieldState<HttpOutputUrl, HttpOutputUrlField> {
  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Expanded(child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[uriField()])),
      _delete()
    ]);
  }
}

class SrtOutputUrlField extends OutputUrlField<SrtOutputUrl> {
  const SrtOutputUrlField(
      SrtOutputUrl uri, void Function()? onDelete, void Function(OutputUrl url)? onTest)
      : super(uri, onDelete, onTest);

  @override
  _SrtOutputFieldState createState() {
    return _SrtOutputFieldState();
  }
}

class _SrtOutputFieldState extends _OutputFieldState<SrtOutputUrl, SrtOutputUrlField> {
  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Expanded(
          child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[uriField(), _modeField(), _srtKeyField()])),
      _delete()
    ]);
  }

  Widget _modeField() {
    return DropdownButtonEx<SrtMode>(
        hint: 'SRT mode',
        value: value.mode,
        values: SrtMode.values,
        onChanged: (c) {
          setState(() {
            value.mode = c!;
          });
        },
        itemBuilder: (SrtMode value) {
          return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
        });
  }

  Widget _srtKeyField() {
    return OptionalFieldTile(
        title: 'Key',
        init: value.srtKey != null,
        onChanged: (val) {
          value.srtKey = val ? SrtKey('', 32) : null;
        },
        builder: () => SrtKeyField(value.srtKey!));
  }
}

class RtmpOutputUrlField extends OutputUrlField<RtmpOutputUrl> {
  const RtmpOutputUrlField(
      RtmpOutputUrl uri, void Function()? onDelete, void Function(OutputUrl url)? onTest)
      : super(uri, onDelete, onTest);

  @override
  _RtmpOutputFieldState createState() {
    return _RtmpOutputFieldState();
  }
}

class _RtmpOutputFieldState extends _OutputFieldState<RtmpOutputUrl, RtmpOutputUrlField> {
  late final TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController(text: value.uri);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Expanded(child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[uriField()])),
      _template(),
      _delete()
    ]);
  }

  @override
  TextFieldEx rawUrlField() => super.rawUrlField().copyWith(controller: _controller);

  Widget _template() {
    return IconButton(
        tooltip: 'Choose template', icon: const Icon(Icons.source), onPressed: addUrl);
  }

  void addUrl() async {
    final IRtmpOutputUrl? output = await showDialog<IRtmpOutputUrl>(
        context: context,
        builder: (context) {
          return IRtmpPickerDialog(value.id);
        });
    if (output != null) {
      showDialog<EditResult<IRtmpOutputUrl>>(
          context: context,
          builder: (context) {
            return RtmpOutputDialog.add(output: output);
          }).then((result) {
        if (result!.action == EditType.SAVE) {
          setState(() {
            _controller.text = result.value.uri;
          });
        }
      });
    }
  }
}

class HLSOutputUrlField extends OutputUrlField<HttpOutputUrl> {
  final String? sid;
  final Stream<JsonRpcEvent>? rpc;
  final String? proxyDirectory;
  final String Function(String file)? onPrepareOutputTemplate;
  final bool isExternal;

  const HLSOutputUrlField(
      HttpOutputUrl uri, void Function()? onDelete, void Function(OutputUrl url)? onTest,
      {this.sid,
      this.rpc,
      this.proxyDirectory,
      this.onPrepareOutputTemplate,
      this.isExternal = false})
      : super(uri, onDelete, onTest);

  @override
  _HLSOutputUrlFieldState createState() {
    return _HLSOutputUrlFieldState();
  }
}

class _HLSOutputUrlFieldState<T extends HLSOutputUrlField>
    extends _OutputFieldState<HttpOutputUrl, T> {
  late final TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController(text: value.uri);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget uriField() {
    final bool canTest = widget.onTest != null;
    final bool canScan = !(widget.onPrepareOutputTemplate == null ||
        widget.sid == null ||
        widget.rpc == null ||
        widget.proxyDirectory == null);

    return Row(mainAxisSize: MainAxisSize.min, children: [
      Expanded(child: rawUrlField().copyWith(controller: _controller)),
      if (canTest) _testButton(),
      if (canScan) const SizedBox(width: 4),
      if (canScan) _scanButton()
    ]);
  }

  Widget _testButton() {
    return FlatButtonEx.filled(
        text: 'Test',
        onPressed: () {
          widget.onTest!(value);
        });
  }

  Widget _scanButton() {
    return FlatButtonEx.filled(
        text: 'Scan',
        onPressed: () {
          showDialog(
              context: context,
              builder: (context) {
                return ScanFolderDialog(widget.sid!, widget.rpc!, widget.onPrepareOutputTemplate!,
                    path: widget.proxyDirectory!, visiblePath: false);
              }).then((path) {
            if (path == null) {
              return;
            }
            setState(() {
              value.uri = path;
              _controller.text = path;
            });
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isExternal) {
      return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
        Expanded(child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[uriField()])),
        _delete()
      ]);
    }

    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Expanded(
          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
        uriField(),
        _httpRoot(),
        _chunkDuration(),
        _httpPlaylistField(),
        _hlsSinkTypeField(),
        _hlsField()
      ])),
      _delete()
    ]);
  }

  Widget _httpRoot() {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.root],
        hintText: 'Http root',
        init: value.httpRoot,
        onFieldChanged: (term) {
          value.httpRoot = term;
        });
  }

  Widget _chunkDuration() {
    return OptionalFieldTile(
        title: 'Chunk duration',
        init: value.chunkDuration != null,
        onChanged: (checked) {
          value.chunkDuration = checked ? 10 : null;
        },
        builder: () {
          return TextFieldEx(
              formatters: <TextInputFormatter>[FilteringTextInputFormatter.allow(RegExp(r'\d+'))],
              hintText: 'Chunk duration',
              keyboardType: const TextInputType.numberWithOptions(),
              errorText: 'Input chunk duration',
              init: value.chunkDuration.toString(),
              onFieldChanged: (val) {
                value.chunkDuration = int.tryParse(val);
              });
        });
  }

  Widget _httpPlaylistField() {
    return OptionalFieldTile(
        title: 'Playlist URL',
        init: value.playlistRoot != null,
        onChanged: (checked) {
          value.playlistRoot = checked ? 'http://0.0.0.0/fastocloud/hls' : null;
        },
        builder: () {
          return TextFieldEx(
              formatters: <TextInputFormatter>[TextFieldFilter.root],
              hintText: 'Playlist URL',
              errorText: 'Input playlist URL',
              init: value.playlistRoot,
              onFieldChanged: (val) {
                value.playlistRoot = val;
              });
        });
  }

  Widget _hlsSinkTypeField() {
    return DropdownButtonEx<HlsSinkType?>(
        hint: 'HLS Sink type',
        value: value.hlsSinkType,
        values: HlsSinkType.values,
        onChanged: (c) {
          setState(() {
            value.hlsSinkType = c;
          });
        },
        itemBuilder: (HlsSinkType? value) {
          return DropdownMenuItem(child: Text(value!.toHumanReadable()), value: value);
        });
  }

  Widget _hlsField() {
    return DropdownButtonEx<HlsType?>(
        hint: 'HLS type',
        value: value.hlsType,
        values: HlsType.values,
        onChanged: (c) {
          setState(() {
            value.hlsType = c;
          });
        },
        itemBuilder: (HlsType? value) {
          return DropdownMenuItem(child: Text(value!.toHumanReadable()), value: value);
        });
  }
}

class NginxHttpOutputUrlField extends HLSOutputUrlField {
  final String? nginxUrl;

  const NginxHttpOutputUrlField(HttpOutputUrl uri, void Function()? onDelete,
      void Function(OutputUrl url)? onTest, this.nginxUrl)
      : super(uri, onDelete, onTest, isExternal: false);

  @override
  _NginxHttpOutputUrlFieldState createState() {
    return _NginxHttpOutputUrlFieldState();
  }
}

class _NginxHttpOutputUrlFieldState<T extends NginxHttpOutputUrlField>
    extends _HLSOutputUrlFieldState<T> {
  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Expanded(
          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
        uriField(),
        _httpRoot(),
        _chunkDuration(),
        _httpPlaylistField(),
        _hlsSinkTypeField(),
        _hlsField(),
        _httpNginx()
      ])),
      _delete()
    ]);
  }

  Widget _httpNginx() {
    if (widget.nginxUrl == null || widget.nginxUrl!.isEmpty) {
      return const SizedBox();
    }

    return TextFieldEx.readOnly(hint: 'Nginx URL', init: widget.nginxUrl!);
  }
}
