import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/sections/common.dart';
import 'package:fastocloud_pro_panel/common/streams/vods/scan_folder_dialog.dart';
import 'package:fastocloud_pro_panel/events/server_events.dart';
import 'package:fastocloud_pro_panel/models/widgets/urls/base.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/widgets.dart';

class InputUrlField<T extends InputUrl> extends StatefulWidget {
  final T init;
  final void Function()? onDelete;
  final void Function(T url)? onTest;

  const InputUrlField(this.init, this.onDelete, this.onTest);

  @override
  _InputFieldState createState() => _InputFieldState<T, InputUrlField<T>>();
}

class _InputFieldState<T extends InputUrl, S extends InputUrlField<T>> extends State<S> {
  T get value => widget.init;

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Expanded(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[uriRow()])),
      _delete()
    ]);
  }

  Widget uriRow() {
    return Row(
        mainAxisSize: MainAxisSize.min, children: [Expanded(child: uriField()), testButton()]);
  }

  Widget uriField() {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'URL',
        errorText: 'Enter URL',
        init: widget.init.uri,
        onFieldChanged: (term) {
          value.uri = term;
        });
  }

  Widget testButton() {
    return FlatButtonEx.filled(
        text: 'Test',
        onPressed: () {
          widget.onTest?.call(value);
        });
  }

  Widget _delete() {
    if (widget.onDelete == null) {
      return const SizedBox();
    }
    return IconButton(tooltip: 'Remove', icon: const Icon(Icons.close), onPressed: widget.onDelete);
  }
}

class HttpInputUrlField extends InputUrlField<HttpInputUrl> {
  const HttpInputUrlField(
      HttpInputUrl uri, void Function()? onDelete, void Function(InputUrl url)? onTest)
      : super(uri, onDelete, onTest);

  @override
  _HttpInputFieldState createState() {
    return _HttpInputFieldState();
  }
}

class _HttpInputFieldState extends _InputFieldState<HttpInputUrl, HttpInputUrlField> {
  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Expanded(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[uriRow(), _userAgentField(), _streamLinkField(), _httpProxy()])),
      _delete()
    ]);
  }

  Widget _httpProxy() {
    return OptionalFieldTile(
        title: 'HTTP proxy',
        init: value.proxy != null,
        onChanged: (checked) {
          value.proxy = checked ? "http://username:password@proxy.com:8080" : null;
        },
        builder: () {
          return TextFieldEx(
              formatters: <TextInputFormatter>[TextFieldFilter.url],
              hintText: 'Http proxy',
              init: value.proxy,
              onFieldChanged: (val) {
                value.proxy = val;
              });
        });
  }

  Widget _userAgentField() {
    return DropdownButtonEx<UserAgent?>(
        hint: 'User agent',
        value: value.userAgent,
        values: UserAgent.values,
        onChanged: (c) {
          setState(() {
            value.userAgent = c;
          });
        },
        itemBuilder: (UserAgent? value) {
          return DropdownMenuItem(child: Text(value!.toHumanReadable()), value: value);
        });
  }

  Widget _streamLinkField() {
    return OptionalFieldTile(
        title: 'Streamlink',
        init: value.streamLink != null,
        onChanged: (val) {
          value.streamLink = val ? PyFastoStream() : null;
        },
        builder: () => StreamLinkField(value.streamLink!));
  }
}

class UdpInputUrlField extends InputUrlField<UdpInputUrl> {
  const UdpInputUrlField(
      UdpInputUrl uri, void Function()? onDelete, void Function(InputUrl url)? onTest)
      : super(uri, onDelete, onTest);

  @override
  _UdpInputFieldState createState() {
    return _UdpInputFieldState();
  }
}

class _UdpInputFieldState extends _InputFieldState<UdpInputUrl, UdpInputUrlField> {
  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Expanded(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[uriRow(), _ifaceFiled(), _programNumberFiled()])),
      _delete()
    ]);
  }

  Widget _ifaceFiled() {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'Multicat iface',
        init: value.multicastIface,
        onFieldChanged: (term) {
          value.multicastIface = term;
        });
  }

  Widget _programNumberFiled() {
    return NumberTextField.integer(
        hintText: 'Program Number',
        canBeEmpty: false,
        initInt: value.programNumber,
        onFieldChangedInt: (term) {
          value.programNumber = term;
        });
  }
}

class SrtInputUrlField extends InputUrlField<SrtInputUrl> {
  const SrtInputUrlField(
      SrtInputUrl uri, void Function()? onDelete, void Function(InputUrl url)? onTest)
      : super(uri, onDelete, onTest);

  @override
  _SrtInputFieldState createState() {
    return _SrtInputFieldState();
  }
}

class _SrtInputFieldState extends _InputFieldState<SrtInputUrl, SrtInputUrlField> {
  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Expanded(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[uriRow(), _modeField(), _srtKeyField()])),
      _delete()
    ]);
  }

  Widget _modeField() {
    return DropdownButtonEx<SrtMode>(
        hint: 'SRT mode',
        value: value.mode,
        values: SrtMode.values,
        onChanged: (c) {
          setState(() {
            value.mode = c!;
          });
        },
        itemBuilder: (SrtMode value) {
          return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
        });
  }

  Widget _srtKeyField() {
    return OptionalFieldTile(
        title: 'Key',
        init: value.srtKey != null,
        onChanged: (val) {
          value.srtKey = val ? SrtKey('', 32) : null;
        },
        builder: () => SrtKeyField(value.srtKey!));
  }
}

String _fileConversion(String file) {
  return 'file://$file';
}

class FileInputUrlField extends InputUrlField<FileInputUrl> {
  final String sid;
  final Stream<JsonRpcEvent> rpc;
  final void Function(String)? onScanFoler;
  final String Function(String file) onPrepareOutputTemplate;

  const FileInputUrlField(FileInputUrl uri, void Function()? onDelete,
      void Function(InputUrl url)? onTest, this.onScanFoler, this.sid, this.rpc,
      {this.onPrepareOutputTemplate = _fileConversion})
      : super(uri, onDelete, onTest);

  @override
  _FileInputFieldState createState() {
    return _FileInputFieldState();
  }
}

class _FileInputFieldState extends _InputFieldState<FileInputUrl, FileInputUrlField> {
  late final TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController(text: value.uri);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Expanded(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[uriRow()])),
      _delete()
    ]);
  }

  @override
  Widget uriRow() {
    return Row(mainAxisSize: MainAxisSize.min, children: [
      Expanded(child: uriField()),
      testButton(),
      const SizedBox(width: 4),
      if (widget.onScanFoler != null) FlatButtonEx.filled(text: 'Scan folder', onPressed: _onScan)
    ]);
  }

  @override
  Widget uriField() {
    return TextFieldEx(
        controller: _controller,
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'File',
        errorText: 'Enter file path',
        init: value.uri,
        onFieldChanged: (term) {
          value.uri = term;
        });
  }

  // private:
  void _onScan() {
    showDialog(
        context: context,
        builder: (context) {
          return ScanFolderDialog(widget.sid, widget.rpc, widget.onPrepareOutputTemplate);
        }).then((path) {
      if (path == null) {
        return;
      }
      setState(() {
        _controller.text = path;
      });
      widget.onScanFoler!(path);
    });
  }
}
