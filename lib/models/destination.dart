import 'package:fastocloud_pro_panel/routing/route_config.dart';
import 'package:flutter/widgets.dart';

class Destination {
  final RouteConfig route;
  final String title;
  final IconData icon;

  const Destination(this.route, this.title, this.icon);
}
