import 'package:fastocloud_pro_panel/profile.dart';
import 'package:fastocloud_pro_panel/routing/route_parser.dart';
import 'package:fastocloud_pro_panel/routing/router_delegate.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:fastocloud_pro_panel/theme.dart';
import 'package:flutter/material.dart';

void main() {
  setupLocator();
  runApp(FastoCloudPanel());
}

class FastoCloudPanel extends StatelessWidget {
  final delegate = RouterDelegateEx();

  @override
  Widget build(BuildContext context) {
    return ProfileInfo(child: Builder(builder: (context) {
      final profile = ProfileInfo.of(context)!.profile;
      return MaterialApp.router(
          debugShowCheckedModeBanner: false,
          theme: themeData(),
          routerDelegate: delegate,
          routeInformationParser: RouteParser(profile));
    }));
  }
}
