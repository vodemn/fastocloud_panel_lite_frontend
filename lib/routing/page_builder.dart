import 'package:fastocloud_pro_panel/common/history/history_page.dart';
import 'package:fastocloud_pro_panel/dashboard/dashboard_navigation.dart';
import 'package:fastocloud_pro_panel/dashboard/profile/profile.dart';
import 'package:fastocloud_pro_panel/dashboard/providers/providers_page.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/server_details.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/servers_page.dart';
import 'package:fastocloud_pro_panel/pages/download/downloads.dart';
import 'package:fastocloud_pro_panel/pages/header.dart';
import 'package:fastocloud_pro_panel/pages/home/home_page.dart';
import 'package:fastocloud_pro_panel/pages/home/privacy_policy.dart';
import 'package:fastocloud_pro_panel/pages/home/terms.dart';
import 'package:fastocloud_pro_panel/pages/login/sign_in.dart';
import 'package:fastocloud_pro_panel/pages/login/sign_up.dart';
import 'package:fastocloud_pro_panel/pages/pricing/pricing_page.dart';
import 'package:fastocloud_pro_panel/routing/page_not_found.dart';
import 'package:fastocloud_pro_panel/routing/route_config.dart';
import 'package:fastocloud_pro_panel/routing/routes.dart';
import 'package:flutter/material.dart';

class PageBuilder {
  final BuildContext context;

  PageBuilder(this.context);

  MaterialPage buildPage(RouteConfig state) {
    final Widget child = generateRoute(state);
    return MaterialPage(child: child);
  }

  Widget generateRoute(RouteConfig state) {
    final uri = state.uri;
    final String route = uri.path;
    switch (route) {
      // available without login
      case HOME_ROUTE:
        return _getHome(const HomePage(), isHome: true);
      case DOWNLOADS_ROUTE:
        return _getHome(const DownloadsPage());
      case PRICING_ROUTE:
        return _getHome(const PricingPage(), isHome: true);
      case SIGN_UP_ROUTE:
        return const SignUpPage();
      case SIGN_IN_ROUTE:
        return const SignInPage(token: null);
      case CREATE_PROVIDER_ROUTE:
        return const SignUpPage.provider();
      case CONFIRM_EMAIL:
        final String? token = uri.queryParameters['token'];
        return SignInPage(token: token);
      case TERMS_ROUTE:
        return const TermsAndConditions();
      case PRIVACY_ROUTE:
        return const PrivacyPolicy();
      // only for logged users
      case PROFILE_ROUTE:
        return _getDashboard(const Profile(), route);
      case PROVIDERS_ROUTE:
        return _getDashboard(const ProvidersWidget(), route);
      case SERVERS_ROUTE:
        return _handleServerRoute(state);
      default:
        return Center(child: PageNotFound());
    }
  }

  // private:
  Widget _getHome(Widget child, {isHome = false}) {
    return DashboardHeader(child, isHome: isHome);
  }

  Widget _getDashboard(Widget child, String route) {
    return _getHome(DashBoardNavigation(route, child));
  }

  Widget _handleServerRoute(RouteConfig state) {
    final uri = state.uri;
    final String route = uri.path;
    final String? id = uri.queryParameters['id'];
    if (id != null) {
      if (uri.queryParameters[HISTORY_ROUTE] != null) {
        return _getDashboard(HistoryPage.server(id), route);
      }
      return _getDashboard(ServerDetailsPage(id), route);
    }
    return _getDashboard(const ServersPage(), route);
  }
}
