const String HOME_ROUTE = '/';
const String DOWNLOADS_ROUTE = '/downloads';
const String PRICING_ROUTE = '/pricing';
const String SIGN_IN_ROUTE = '/login';
const String SIGN_UP_ROUTE = '/join';
const String CREATE_PROVIDER_ROUTE = '/create';
const String CONFIRM_EMAIL = '/confirm_email';
const String TERMS_ROUTE = '/terms';
const String PRIVACY_ROUTE = '/privacy';

const String PROFILE_ROUTE = '/profile';
const String SERVERS_ROUTE = '/servers';
const String PROVIDERS_ROUTE = '/providers';

const String HISTORY_ROUTE = '/history';
