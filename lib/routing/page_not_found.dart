import 'package:fastocloud_pro_panel/constants.dart';
import 'package:fastocloud_pro_panel/routing/router_delegate.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';

class PageNotFound extends StatelessWidget {
  static const double _size = 96;

  @override
  Widget build(BuildContext context) {
    final content = Center(
        child: SingleChildScrollView(
            child: Column(mainAxisAlignment: MainAxisAlignment.center, children: const [
      Icon(Icons.search, size: _size),
      Text('Page not found', style: TextStyle(fontSize: _size / 2))
    ])));
    return Scaffold(
        appBar: AppBar(
            elevation: 0,
            leading: const CustomAssetLogo(LOGO_PATH, width: 320, height: 180),
            title: _homeButton(context)),
        body: content);
  }

  Widget _homeButton(BuildContext context) {
    return InkWell(
        child: Row(mainAxisSize: MainAxisSize.min, children: const [Text(PROJECT)]),
        onTap: () {
          RouterDelegateEx.of(context).toHome();
        });
  }
}
