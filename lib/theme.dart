import 'package:flutter/material.dart';

ThemeData themeData() {
  return ThemeData(
      primarySwatch: Colors.blue,
      colorScheme: const ColorScheme.light(primary: Colors.blue),
      accentColor: Colors.blueAccent,
      appBarTheme: const AppBarTheme(
          iconTheme: IconThemeData(color: Colors.white),
          actionsIconTheme: IconThemeData(color: Colors.white)),
      inputDecorationTheme: const InputDecorationTheme(
          errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.redAccent)),
          border: OutlineInputBorder(),
          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.blueAccent))));
}
