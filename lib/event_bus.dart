import 'package:event_bus/event_bus.dart';

class FastoEventBus {
  static Future<FastoEventBus> getInstance() async {
    _instance ??= FastoEventBus();
    return _instance!;
  }

  void publish(dynamic event) {
    _bus.fire(event);
  }

  Stream<T> subscribe<T>() {
    return _bus.on<T>();
  }

  // private:
  static FastoEventBus? _instance;
  final _bus = EventBus(sync: true);
}
