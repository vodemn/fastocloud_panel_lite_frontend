import 'dart:async';
import 'dart:convert';

import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/models/product.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter_common/loader.dart';

class DownloadsLoader extends ItemBloc<List<Product>> {
  @override
  Future<PackagesDataState> loadDataRoute() {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchGet('/downloads');
    return response.then((value) {
      final List<Product> result = [];
      final data = json.decode(value.body);
      data['downloads'].forEach((s) {
        final res = Product.fromJson(s);
        result.add(res);
      });
      return PackagesDataState(result);
    }, onError: (error) {
      throw error;
    });
  }
}

class PackagesDataState extends ItemDataState<List<Product>> {
  PackagesDataState(List<Product> data) : super(data);
}
