import 'package:fastocloud_pro_panel/constants.dart';
import 'package:fastocloud_pro_panel/dashboard/dashboard_navigation.dart';
import 'package:fastocloud_pro_panel/models/destination.dart';
import 'package:fastocloud_pro_panel/pages/home/home_page.dart';
import 'package:fastocloud_pro_panel/profile.dart';
import 'package:fastocloud_pro_panel/routing/route_config.dart';
import 'package:fastocloud_pro_panel/routing/router_delegate.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

class NavigationDrawer extends StatefulWidget {
  final void Function(RouteConfig) navigate;

  const NavigationDrawer(this.navigate);

  @override
  _NavigationDrawerState createState() {
    return _NavigationDrawerState();
  }
}

class _NavigationDrawerState extends State<NavigationDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
            padding: EdgeInsets.zero,
            children: [_logo(), ...commonTiles(), ...dashboardTiles(), const _VersionTile()]));
  }

  List<Widget> commonTiles() {
    final List<Destination> destinations = HomePage.destinations();
    return List<Widget>.generate(destinations.length, (index) {
          return _tile(destinations[index]);
        }) +
        const [Divider()];
  }

  List<Widget> dashboardTiles() {
    if (ProfileInfo.of(context)!.profile == null) {
      return [];
    }

    final bool isAdmin = ProfileInfo.of(context)!.profile!.isAdmin();
    final List<Destination> destinations = DashBoardNavigation.destinations(isAdmin);
    return List<Widget>.generate(destinations.length, (index) {
          return _tile(destinations[index]);
        }) +
        const [Divider()];
  }

  Widget _logo() {
    return const Align(
        child: DrawerHeader(child: CustomAssetLogo(LOGO_PATH, width: 320, height: 180)));
  }

  Widget _tile(Destination d) {
    return ListTile(
        leading: Icon(d.icon),
        title: Text(d.title),
        onTap: () {
          RouterDelegateEx.of(context).setNewRoutePath(d.route);
        });
  }
}

class _VersionTile extends StatelessWidget {
  const _VersionTile();

  @override
  Widget build(BuildContext context) {
    final package = locator<PackageManager>();
    final String version = package.version();
    return ListTile(leading: const Icon(Icons.apps), title: Text('Version: $version'));
  }
}
