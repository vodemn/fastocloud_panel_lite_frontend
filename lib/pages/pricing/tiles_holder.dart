import 'package:fastocloud_pro_panel/models/pricing_item.dart';
import 'package:fastocloud_pro_panel/pages/home/common/layout.dart';
import 'package:fastocloud_pro_panel/pages/home/common/padding.dart';
import 'package:fastocloud_pro_panel/pages/pricing/pricing_tile.dart';
import 'package:fastocloud_pro_panel/pages/pricing/type_picker.dart';
import 'package:flutter/material.dart';

class PricingSection extends StatefulWidget {
  final String title;
  final List<PricingItem> items;
  final bool canChangePeriod;

  const PricingSection({required this.title, required this.items, this.canChangePeriod = true});

  static _PricingSectionState? of(BuildContext context) {
    return context.findAncestorStateOfType<_PricingSectionState>();
  }

  @override
  _PricingSectionState createState() {
    return _PricingSectionState();
  }
}

class _PricingSectionState extends State<PricingSection> {
  bool _expanded = false;
  static const int _maxCount = 9;

  bool get _canExpand => _maxCount < widget.items.length;

  int get _effectiveLength => !_expanded && _canExpand ? _maxCount : widget.items.length;
  PricingType _type = PricingType.MONTHLY;

  PricingType get currentType => _type;

  @override
  Widget build(BuildContext context) {
    return Center(child: HomeLayoutBuilder(_mobile(context), _desktop(context)));
  }

  Widget _desktop(BuildContext context) {
    const double spacing = 64;
    const double runSpacing = 56;
    const int count = 3;
    final List<Widget> _tiles = List<Widget>.generate(_effectiveLength, (index) {
      return PricingTile(widget.items[index]);
    });
    if (widget.items.length < 3) {
      _tiles.addAll(List<Widget>.generate(count - widget.items.length, (index) {
        return const SizedBox(width: PricingTile.width);
      }));
    }
    return HomePageContentPadding(
        height: null,
        child: _Decoration(
            padding: 48,
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              Container(
                  constraints: const BoxConstraints(
                      maxWidth:
                          PricingTile.width * count + (spacing - PricingTile.radius) * (count - 1)),
                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                    _title(),
                    if (widget.canChangePeriod) PricingTypeSwitch(_setType)
                  ])),
              const SizedBox(height: 64),
              Wrap(spacing: spacing, runSpacing: runSpacing, children: _tiles),
              const SizedBox(height: 24),
              _collapseButton()
            ])));
  }

  Widget _mobile(BuildContext context) {
    final List<Widget> _tiles = List<Widget>.generate(_effectiveLength, (index) {
      return Padding(
          padding: EdgeInsets.only(bottom: index == widget.items.length - 1 ? 0 : 16),
          child: PricingTile(widget.items[index]));
    });
    return _Decoration(
        margin: 16,
        padding: 16,
        child: Column(children: [
          const SizedBox(height: 16),
          _title(),
          if (widget.canChangePeriod) const SizedBox(height: 16),
          if (widget.canChangePeriod)
            Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [PricingTypeSwitch(_setType)]),
          const SizedBox(height: 32),
          ..._tiles,
          _collapseButton()
        ]));
  }

  Widget _title() {
    return Text(widget.title,
        textAlign: TextAlign.center,
        maxLines: 2,
        style: const TextStyle(fontSize: 28, fontWeight: FontWeight.w800));
  }

  Widget _collapseButton() {
    if (!_canExpand) {
      return const SizedBox();
    }
    return Padding(
        padding: const EdgeInsets.only(top: 16.0),
        child: InkWell(
            onTap: () {
              setState(() {
                _expanded = !_expanded;
              });
            },
            child: Text('Show ${_expanded ? 'less' : 'more'}',
                style: const TextStyle(fontSize: 20, color: Color.fromRGBO(102, 102, 102, 1)))));
  }

  void _setType(PricingType type) {
    setState(() {
      _type = type;
    });
  }
}

class _Decoration extends StatelessWidget {
  final Widget child;
  final double margin;
  final double padding;

  const _Decoration({required this.child, this.margin = 0, this.padding = 0});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(margin),
        padding: EdgeInsets.all(padding),
        decoration: BoxDecoration(
            color: Theme.of(context).cardColor, borderRadius: BorderRadius.circular(48)),
        child: child);
  }
}
