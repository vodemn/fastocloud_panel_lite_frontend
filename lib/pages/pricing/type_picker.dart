import 'package:fastocloud_pro_panel/models/pricing_item.dart';
import 'package:fastocloud_pro_panel/pages/pricing/tiles_holder.dart';
import 'package:flutter/material.dart';

class PricingTypeSwitch extends StatelessWidget {
  final void Function(PricingType type) onChanged;

  const PricingTypeSwitch(this.onChanged);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(color: Colors.black, borderRadius: BorderRadius.circular(8)),
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(children: [
              _itemButton(context, PricingType.MONTHLY),
              const Padding(padding: EdgeInsets.all(4.0), child: VerticalDivider()),
              _itemButton(context, PricingType.ANNUALY),
              const Padding(padding: EdgeInsets.all(4.0), child: VerticalDivider()),
              _itemButton(context, PricingType.LIFETIME)
            ])));
  }

  Widget _itemButton(BuildContext context, PricingType type) {
    return InkWell(
        hoverColor: Colors.transparent,
        splashColor: Colors.transparent,
        onTap: () {
          onChanged(type);
        },
        child: Text(type.toHumanReadable(), style: style(context, type)));
  }

  TextStyle style(BuildContext context, PricingType type) {
    if (type == PricingSection.of(context)!.currentType) {
      return const TextStyle(color: Colors.white);
    } else {
      return const TextStyle(color: Colors.white54);
    }
  }
}
