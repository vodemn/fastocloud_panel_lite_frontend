import 'package:fastocloud_pro_panel/models/pricing_item.dart';
import 'package:fastocloud_pro_panel/pages/home/common/padding.dart';
import 'package:fastocloud_pro_panel/pages/pricing/tiles_holder.dart';
import 'package:flutter/material.dart';

class PricingPage extends StatelessWidget {
  const PricingPage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(children: <Widget>[
      Padding(
          padding: EdgeInsets.only(top: 56, bottom: 56 - HomePageContentPadding.value.bottom),
          child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                  text: 'Fasto',
                  style: TextStyle(fontSize: 48, color: Theme.of(context).accentColor),
                  children: const <TextSpan>[
                    TextSpan(text: 'Cloud pricing', style: TextStyle(color: Colors.white))
                  ]))),
      const PricingSection(title: 'IPTV', items: [
        PricingItem('FastoCloud IPTV panel', 50, 'External stream, VODS'),
        PricingItem('FastoCloud Media part', 15, '+ Restream, Endcode, Catchups, Timeshifts, PPV'),
        PricingItem('FastoCloud PRO Media part', 25, 'Media part + Probe & folder scanning'),
        PricingItem.free('FastoTV players'),
        PricingItem('Branded Android player', 35, 'Play store 100\$ single pay'),
        PricingItem('Branded IOS player', 100, 'AppStore 400\$ single pay'),
        PricingItem('Branded TvOS player', 100, 'AppStore 400\$ single pay'),
        PricingItem('Branded PC player', 15),
        PricingItem('Branded Roku', 100, 'RokuStore 400\$ single pay'),
        PricingItem('Subscribers website', 25),
        PricingItem('Subscribers website + Payments', 50),
        PricingItem('FastoCloud LoadBalance', 5),
        PricingItem('FasctoTV EPG', 5),
        PricingItem('Fasctoloud EPG', 5)
      ]),
      const PricingSection(title: 'CCTV', items: [
        PricingItem.free('FastoCloud CCTV panel'),
        PricingItem('FastoCloud Media part', 15, 'Restream, Endcode, Catchups, Timeshifts, PPV'),
        PricingItem('FastoCloud PRO Media part', 25, 'Media part + Probe & folder scanning'),
        PricingItem.negotiable('Players'),
        PricingItem('Subscribers website', 25),
        PricingItem('Subscribers website + Payments', 50),
        PricingItem('FastoCloud LoadBalance', 5)
      ]),
      const PricingSection(title: 'CV', items: [
        PricingItem.free('FastoCloud panel'),
        PricingItem('FastoCloud PRO ML Media part', 200)
      ]),
      const PricingSection(canChangePeriod: false, title: 'Additional', items: [
        PricingConsultation('Consultation', 50),
        PricingPromotion('Marketing promotion', 70),
        PricingItem.negotiable('Custom development')
      ])
    ]));
  }
}
