import 'package:fastocloud_pro_panel/models/pricing_item.dart';
import 'package:fastocloud_pro_panel/pages/pricing/tiles_holder.dart';
import 'package:flutter/material.dart';

class PricingTile extends StatelessWidget {
  static const double height = 196;
  static const double width = 256;
  static const double radius = 24;
  final PricingItem item;

  const PricingTile(this.item);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(16),
        height: height,
        width: width,
        decoration: BoxDecoration(
            color: Theme.of(context).accentColor, borderRadius: BorderRadius.circular(radius)),
        child: Center(
            child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text(item.name,
              style: const TextStyle(fontSize: 30, color: Colors.white),
              textAlign: TextAlign.center),
          Text(item.description ?? '',
              style: const TextStyle(fontSize: 18, color: Colors.white70),
              textAlign: TextAlign.center),
          Text(item.price(PricingSection.of(context)!.currentType),
              style: const TextStyle(fontSize: 24, color: Color.fromRGBO(7, 51, 32, 1)),
              textAlign: TextAlign.center)
        ])));
  }
}
