import 'package:fastocloud_pro_panel/pages/drawer.dart';
import 'package:fastocloud_pro_panel/pages/home/common/padding.dart';
import 'package:fastocloud_pro_panel/profile.dart';
import 'package:fastocloud_pro_panel/routing/route_config.dart';
import 'package:fastocloud_pro_panel/routing/router_delegate.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/utils.dart';
import 'package:responsive_builder/responsive_builder.dart';

class DashboardHeader extends StatelessWidget {
  final GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
  final Widget child;
  final bool isHome;

  DashboardHeader(this.child, {this.isHome = false});

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInformation) {
      return Theme(
          data: isHome
              ? ThemeData.dark().copyWith(
                  primaryColor: Colors.black,
                  accentColor: const Color.fromRGBO(76, 155, 119, 1),
                  scaffoldBackgroundColor: const Color.fromRGBO(22, 22, 22, 1),
                  cardColor: const Color.fromRGBO(39, 39, 39, 1))
              : Theme.of(context),
          child: Scaffold(
              key: _drawerKey,
              appBar: _header(context, sizingInformation.isMobile),
              drawer: NavigationDrawer((route) => _navigate(context, route)),
              body: child));
    });
  }

  AppBar _header(BuildContext context, bool isMobile) {
    if (isHome && !isMobile) {
      return AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: HomePageContentPadding(
              height: kToolbarHeight,
              padding: const EdgeInsets.symmetric(horizontal: 48.0),
              child: Row(children: [
                _HomeTitle(isHome),
                const Spacer(),
                ..._headerActions(context, isMobile)
              ])));
    }
    return AppBar(
        automaticallyImplyLeading: false,
        leading: _leadingButton(isMobile),
        title: Row(children: [_HomeTitle(isHome)]),
        actions: _headerActions(context, isMobile));
  }

  Widget? _leadingButton(isMobile) {
    if (isMobile) {
      return IconButton(icon: const Icon(Icons.menu), onPressed: _openDrawer);
    } else {
      return null;
    }
  }

  List<Widget> _headerActions(BuildContext context, bool isMobile) {
    const List<Widget> base = [
      _HeaderButton('Download', RouteConfig.downloads()),
      _HeaderButton('Pricing', RouteConfig.pricing())
    ];
    if (ProfileInfo.of(context)!.profile != null) {
      return [
        if (!isMobile) ...base,
        const _HeaderButton('Profile', RouteConfig.profile()),
        _logOutButton(context)
      ];
    } else {
      return [
        if (!isMobile) ...base,
        const _HeaderButton('Sign in', RouteConfig.signIn()),
        if (!isMobile) const _HeaderButton('Sign up', RouteConfig.signUp()),
        const SizedBox(width: 16)
      ];
    }
  }

  Widget _logOutButton(BuildContext context) {
    return IconButton(
        icon: const Icon(Icons.exit_to_app),
        tooltip: 'Log in/out',
        onPressed: () {
          _navigate(context, const RouteConfig.home());
          ProfileInfo.of(context)!.logout();
        });
  }

  void _navigate(BuildContext context, RouteConfig route) {
    RouterDelegateEx.of(context).setNewRoutePath(route);
  }

  void _openDrawer() {
    _drawerKey.currentState!.openDrawer();
  }
}

class _HeaderButton extends StatelessWidget {
  final String title;
  final RouteConfig route;

  const _HeaderButton(this.title, this.route);

  @override
  Widget build(BuildContext context) {
    final style = TextButton.styleFrom(
        primary: Colors.white, padding: const EdgeInsets.symmetric(horizontal: 22));

    return TextButton(
        style: style,
        child: Text(title, style: const TextStyle(fontWeight: FontWeight.bold)),
        onPressed: () {
          RouterDelegateEx.of(context).setNewRoutePath(route);
        });
  }
}

class _HomeTitle extends StatelessWidget {
  final bool isHome;

  const _HomeTitle(this.isHome);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        highlightColor: Colors.transparent,
        hoverColor: Colors.transparent,
        splashColor: Colors.transparent,
        child: SizedBox(height: kToolbarHeight, child: Center(child: _title(context))),
        onTap: () {
          RouterDelegateEx.of(context).toHome();
        });
  }

  Widget _title(BuildContext context) {
    if (isHome) {
      return RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
              text: 'Fasto',
              style:
                  DefaultTextStyle.of(context).style.copyWith(color: Theme.of(context).accentColor),
              children: <TextSpan>[
                TextSpan(
                    text: 'Cloud',
                    style:
                        TextStyle(color: backgroundColorBrightness(Theme.of(context).primaryColor)))
              ]));
    }

    return Text('FastoCloud',
        style: TextStyle(color: Theme.of(context).appBarTheme.actionsIconTheme!.color));
  }
}
