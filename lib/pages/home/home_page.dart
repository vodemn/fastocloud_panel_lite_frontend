import 'dart:convert';

import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/localization/translations.dart';
import 'package:fastocloud_pro_panel/models/destination.dart';
import 'package:fastocloud_pro_panel/pages/home/sections/become_client.dart';
import 'package:fastocloud_pro_panel/pages/home/sections/footer.dart';
import 'package:fastocloud_pro_panel/pages/home/sections/introduction.dart';
import 'package:fastocloud_pro_panel/pages/home/sections/partners.dart';
import 'package:fastocloud_pro_panel/pages/home/sections/services.dart';
import 'package:fastocloud_pro_panel/routing/route_config.dart';
import 'package:fastocloud_pro_panel/routing/router_delegate.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';

class HomePage extends StatefulWidget {
  const HomePage();

  static List<Destination> destinations() {
    return const <Destination>[
      Destination(RouteConfig.home(), TR_HOME, Icons.home),
      Destination(RouteConfig.downloads(), TR_DOWNLOADS, Icons.download_outlined)
    ];
  }

  @override
  _HomePageState createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    final fetcher = locator<Fetcher>();
    fetcher.getInfo().then((response) {
      final data = json.decode(response.body);
      final int count = data['providers_count'];
      if (count == 0) {
        WidgetsBinding.instance!.addPostFrameCallback((_) {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                    title: const Text('Hello!'),
                    content: const Text('There are no providers yet. Want to create one?',
                        softWrap: true),
                    actions: <Widget>[
                      FlatButtonEx.notFilled(text: 'Cancel', onPressed: Navigator.of(context).pop),
                      FlatButtonEx.filled(
                          text: 'Create',
                          onPressed: () {
                            Navigator.of(context).pop();
                            RouterDelegateEx.of(context)
                                .setNewRoutePath(const RouteConfig.createProvider());
                          })
                    ]);
              });
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(children: <Widget>[
      const IntroductionHomeSection(),
      Image.asset('install/assets/home/drops_black.png', fit: BoxFit.fitHeight),
      const ServicesHomeSection(),
      const BecomeClientHomeSction(),
      Container(
          width: MediaQuery.of(context).size.width,
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                  colors: [Colors.transparent, Colors.white],
                  begin: FractionalOffset(0.0, 0.0),
                  end: FractionalOffset(0.0, 0.01),
                  stops: [0.0, 1.0])),
          child: Column(children: [
            SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Image.asset('install/assets/home/drops_grey.png', fit: BoxFit.fitWidth)),
            const OurPartnersHomeSection()
          ])),
      const HomeFooter()
    ]));
  }
}
