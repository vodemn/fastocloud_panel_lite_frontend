import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_markdown/flutter_markdown.dart';

class TermsAndConditions extends StatelessWidget {
  const TermsAndConditions();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(children: [
      Expanded(
          child: FutureBuilder(
              future: rootBundle.loadString('install/assets/home/terms_conditions.md'),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Markdown(data: snapshot.data.toString());
                }
                return const Center(child: CircularProgressIndicator());
              }))
    ]));
  }
}
