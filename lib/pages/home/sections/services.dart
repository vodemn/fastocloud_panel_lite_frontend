import 'package:fastocloud_pro_panel/pages/home/common/padding.dart';
import 'package:flutter/material.dart';

class ServicesHomeSection extends StatelessWidget {
  const ServicesHomeSection();

  @override
  Widget build(BuildContext context) {
    return HomePageContentPadding(
        padding: const EdgeInsets.symmetric(vertical: 32),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          const Text('Our services', style: TextStyle(fontSize: 48)),
          SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(children: [
                _card(context, 'player', 'Video players', """
Features:
Cross-platform video players for: Android, IOS, tvOS, LG, Samsung
Windows, Linux, MacOSX, FreeBSD, Raspbian/Armbian with CPU/GPU decoding and postprocessing 
"""),
                _card(context, 'iptv', 'IPTV software', """
Features:
MiddleWare, Restream, Transcode (CPU/GPU), Timeshifts, Catchups, Vods/Series,
Restream/Transcode from online streaming services like Youtube, Twitch
AD insertion, Remote players control, Load Balancing,
Pay per view channels,Channels on demand
HTTP Live streaming (HLS) server-side support"""),
                _card(context, 'cctv', 'CCTV software', """
Features:
CCTV monitoring service 24/7, 
Any input/output formats, 
Motion detection, Remote access and Backups,
Software NVR, Self-hosted servers, Cloud servers"""),
                _card(context, 'vision', 'Computer vision', """
Features:
Detect objects in real-time video and send email/push notifications
Objects classification, open doors, time tracking, object tracking, lock/unlock devices
Fire/Smoke detection, Cloud support (Amazon Kinesis), 
ML Devices(NVIDIA, Intel Movidius)
DL Frameworks(Tensorflow, NCSDK, Cafee, Pytorch)""")
              ]))
        ]));
  }

  Widget _card(BuildContext context, String image, String title, String content) {
    return Container(
        margin: const EdgeInsets.all(16),
        height: 312,
        width: 266,
        decoration: BoxDecoration(
            color: Theme.of(context).accentColor,
            borderRadius: BorderRadius.circular(30),
            border: Border.all(color: Colors.white)),
        child: Center(
            child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          SizedBox.fromSize(
              size: const Size.square(150),
              child: Image.asset('install/assets/home/services/$image.png')),
          Text(title, style: const TextStyle(fontSize: 28, color: Colors.white)),
          TextButton(
              style: TextButton.styleFrom(primary: Colors.white70),
              child: const Text('View details'),
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (_) {
                      return _DetailsDialog(title, content);
                    });
              })
        ])));
  }
}

class _DetailsDialog extends StatelessWidget {
  final String title;
  final String text;

  const _DetailsDialog(this.title, this.text);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        backgroundColor: Theme.of(context).accentColor,
        title: Text(title, style: const TextStyle(color: Colors.black)),
        content:
            SizedBox(width: 196, child: Text(text, style: const TextStyle(color: Colors.black))));
  }
}
