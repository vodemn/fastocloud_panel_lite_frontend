import 'dart:async';
import 'dart:convert';

import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/models/partner.dart';
import 'package:fastocloud_pro_panel/pages/home/common/padding.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:url_launcher/url_launcher.dart';

class OurPartnersHomeSection extends StatefulWidget {
  const OurPartnersHomeSection();

  @override
  _OurPartnersHomeSectionState createState() {
    return _OurPartnersHomeSectionState();
  }
}

class _OurPartnersHomeSectionState extends State<OurPartnersHomeSection>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return HomePageContentPadding(
        padding: const EdgeInsets.symmetric(vertical: 32),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: HomePageContentPadding.value.left),
                  child: const Text('Our friends',
                      style: TextStyle(fontSize: 48, color: Colors.black))),
              const SizedBox(height: 48),
              _partners()
            ]));
  }

  Widget _partners() {
    return FutureBuilder<Response>(
        future: locator<Fetcher>().fetchGet('/partners'),
        builder: (context, snap) {
          if (snap.hasData) {
            if (snap.data!.statusCode == 200) {
              final List<_Card> partners = [];
              final data = json.decode(snap.data!.body);
              data['partners'].forEach((s) {
                final res = Partner.fromJson(s);
                partners.add(_Card(res.logo, res.link));
              });
              return _AnimatedPartners(partners);
            }
          }
          return const Center(child: CircularProgressIndicator());
        });
  }
}

class _AnimatedPartners extends StatefulWidget {
  final List<_Card> partners;

  const _AnimatedPartners(this.partners);

  @override
  _AnimatedPartnersState createState() => _AnimatedPartnersState();
}

class _AnimatedPartnersState extends State<_AnimatedPartners> {
  final ScrollController _controller = ScrollController();
  late Timer _timer;

  @override
  void initState() {
    super.initState();
    _init();
  }

  void _init() {
    _timer = Timer.periodic(const Duration(milliseconds: 1500), (timer) {
      final double _current = _controller.offset;
      _controller.animateTo(_current + _Card.cardSize,
          curve: Curves.linear, duration: const Duration(milliseconds: 500));
    });
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: _Card.cardSize,
        child: ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            controller: _controller,
            scrollDirection: Axis.horizontal,
            itemBuilder: (BuildContext context, int index) {
              return Row(children: widget.partners);
            }));
  }
}

class _Card extends StatelessWidget {
  static const cardSize = 236.0;
  final String image;
  final String link;

  const _Card(this.image, this.link);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.all(48),
        height: 140,
        width: 140,
        child: Center(
            child: InkWell(
                onTap: () {
                  launch(link);
                },
                child: Image.network(image))));
  }
}
