import 'dart:convert';

import 'package:fastocloud_pro_panel/constants.dart';
import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/localization/translations.dart';
import 'package:fastocloud_pro_panel/pages/home/common/layout.dart';
import 'package:fastocloud_pro_panel/pages/home/common/padding.dart';
import 'package:fastocloud_pro_panel/routing/route_config.dart';
import 'package:fastocloud_pro_panel/routing/router_delegate.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:http/http.dart';

class HomeFooter extends StatefulWidget {
  static const _Text _pubsub =
      _Text('Copyright\u00a9 2014-2021 FastoGT. All right reserved.', color: Colors.white54);

  const HomeFooter();

  @override
  _HomeFooterState createState() => _HomeFooterState();
}

class _HomeFooterState extends State<HomeFooter> with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
        color: Colors.black, child: HomeLayoutBuilder(_mobile(context), _desktop(context)));
  }

  Widget _desktop(BuildContext context) {
    return Column(children: [
      Padding(
          padding: const EdgeInsets.symmetric(vertical: 36.0),
          child: HomePageContentPadding(
              height: null,
              padding: const EdgeInsets.symmetric(horizontal: 64.0),
              child: Center(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [_resources(), const _Support(), _about(), const _Socials()])))),
      HomeFooter._pubsub
    ]);
  }

  Widget _mobile(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Padding(padding: const EdgeInsets.all(8.0), child: _resources()),
      Padding(padding: const EdgeInsets.all(8.0), child: _about()),
      const Padding(padding: EdgeInsets.all(8.0), child: _Support()),
      const Padding(padding: EdgeInsets.all(8.0), child: _Socials()),
      const Center(child: HomeFooter._pubsub)
    ]);
  }

  Widget _resources() {
    return _TextColumn('Recources', [
      _ClickableText('PubSubMe', () {
        launchExternalUrl("https://pubsub.me/");
      }),
      _ClickableText('YoloDump', () {
        launchExternalUrl("https://yolodump.com/");
      }),
      _ClickableText('FastoCams', () {
        launchExternalUrl("https://fastocams.com/");
      })
    ]);
  }

  Widget _about() {
    return _TextColumn(TR_ABOUT, [
      _ClickableText('Privacy policy', () {
        RouterDelegateEx.of(context).setNewRoutePath(const RouteConfig.privacy());
      }),
      _ClickableText('Terms & conditions', () {
        RouterDelegateEx.of(context).setNewRoutePath(const RouteConfig.terms());
      }),
      _ClickableText('Contact us', () {
        final Uri _emailLaunchUri = Uri(scheme: 'mailto', path: CONTACT_US_EMAIL);
        launchExternalUrl(_emailLaunchUri.toString());
      })
    ]);
  }
}

class _Support extends StatelessWidget {
  const _Support();

  @override
  Widget build(BuildContext context) {
    final fetcher = locator<Fetcher>();
    return FutureBuilder<Response>(
        future: fetcher.fetchGet('/support'),
        builder: (context, snap) {
          if (snap.hasData) {
            if (snap.data!.statusCode == 200) {
              final data = json.decode(snap.data!.body);
              final support = data['support'];
              final String email = support['contact_email'];
              return _TextColumn(TR_EDUCATION_AND_SUPPORT, [
                _ClickableText('Installation', () {
                  launchExternalUrl(
                      'https://github.com/fastogt/fastocloud_env/wiki/Install-package');
                }),
                _ClickableText('F.A.Q', () {
                  launchExternalUrl('https://github.com/fastogt/fastocloud_admin/wiki/F.A.Q');
                }),
                _ClickableText('Support', () {
                  launchExternalUrl('mailto:$email');
                })
              ]);
            }
          }
          return const _TextColumn(TR_EDUCATION_AND_SUPPORT, []);
        });
  }
}

class _Socials extends StatelessWidget {
  const _Socials();

  @override
  Widget build(BuildContext context) {
    final fetcher = locator<Fetcher>();
    return FutureBuilder<Response>(
        future: fetcher.fetchGet('/socials'),
        builder: (context, snap) {
          if (snap.hasData) {
            if (snap.data!.statusCode == 200) {
              final data = json.decode(snap.data!.body);
              final socials = data['socials'];
              final String discord = socials['discord'];
              final String facebook = socials['facebook'];
              final String github = socials['github'];
              final String gitlab = socials['gitlab'];
              final String instagram = socials['instagram'];
              final String linkedin = socials['linkedin'];
              final String youtube = socials['youtube'];
              return _TextColumn(TR_FOLLOW_US, [
                _ClickableText('FaceBook', () {
                  launchExternalUrl(facebook);
                }),
                _ClickableText('Discord', () {
                  launchExternalUrl(discord);
                }),
                _ClickableText('YouTube', () {
                  launchExternalUrl(youtube);
                }),
                _ClickableText('Instagram', () {
                  launchExternalUrl(instagram);
                }),
                _ClickableText('LinkedIn', () {
                  launchExternalUrl(linkedin);
                }),
                _ClickableText('GitHub', () {
                  launchExternalUrl(github);
                }),
                _ClickableText('GitLab', () {
                  launchExternalUrl(gitlab);
                })
              ]);
            }
          }
          return const _TextColumn(TR_FOLLOW_US, []);
        });
  }
}

class _TextColumn extends StatelessWidget {
  final String title;
  final List<Widget> items;

  const _TextColumn(this.title, this.items);

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[_Text(title, color: Colors.white54, size: 24)] + items);
  }
}

class _ClickableText extends StatelessWidget {
  final String text;
  final void Function() onTap;
  final double size;

  const _ClickableText(this.text, this.onTap, {this.size = 20});

  @override
  Widget build(BuildContext context) {
    return InkWell(
        highlightColor: Colors.transparent,
        hoverColor: Colors.transparent,
        splashColor: Colors.transparent,
        child: _Text(text, size: size),
        onTap: onTap);
  }
}

class _Text extends StatelessWidget {
  final String title;
  final Color color;
  final double size;

  const _Text(this.title, {this.size = 20, this.color = Colors.white});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(title, style: TextStyle(color: color, fontSize: size), softWrap: true));
  }
}
