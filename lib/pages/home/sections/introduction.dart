import 'package:fastocloud_pro_panel/pages/home/common/padding.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class IntroductionHomeSection extends StatelessWidget {
  const IntroductionHomeSection();

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(desktop: _desktop(context), mobile: _mobile());
  }

  Widget _desktop(BuildContext context) {
    return Container(
        color: Colors.black,
        child: Center(
            child: HomePageContentPadding(
                padding: const EdgeInsets.symmetric(horizontal: 64.0),
                child: Row(children: [
                  Expanded(flex: 2, child: _text()),
                  Padding(
                      padding: const EdgeInsets.symmetric(vertical: 24),
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(double.maxFinite),
                          child: Image.asset('install/assets/home/1.png')))
                ]))));
  }

  Widget _mobile() {
    return Container(
        color: Colors.black,
        child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 32), child: _text()));
  }

  Widget _text() {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: const <Widget>[
          Text("Software services\nfor video suppliers",
              style: TextStyle(fontSize: 64, fontWeight: FontWeight.bold, height: 1)),
          SizedBox(height: 32),
          Text(
              "PC, Mobile, Web, Clouds/Servers, TV software\nwith various video control functions!",
              style: TextStyle(fontSize: 32))
        ]);
  }
}
