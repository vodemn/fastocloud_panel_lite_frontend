import 'package:fastocloud_pro_panel/pages/home/common/layout.dart';
import 'package:fastocloud_pro_panel/pages/home/common/padding.dart';
import 'package:flutter/material.dart';

class BecomeClientHomeSction extends StatelessWidget {
  const BecomeClientHomeSction();

  static const List<Widget> _cards = [
    _Card('isp', 'ISP'),
    _Card('corporate', 'Corporate'),
    _Card('startup', 'Startup'),
    _Card('reseller', 'Reseller'),
    _Card('mobile_operator', 'Mobile operator'),
    _Card('school', 'Online school'),
    _Card('cable_operator', 'Cable operator'),
    _Card('blogger', 'Video blogger'),
    _Card('media', 'Media app')
  ];

  @override
  Widget build(BuildContext context) {
    return Center(child: HomeLayoutBuilder(_mobile(context), _desktop(context)));
  }

  Widget _desktop(BuildContext context) {
    return HomePageContentPadding(
        height: null,
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          _title(),
          const SizedBox(height: 16),
          Wrap(alignment: WrapAlignment.center, children: _cards)
        ]));
  }

  Widget _mobile(BuildContext context) {
    return Column(children: [
      _title(),
      const SizedBox(height: 16),
      SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8), child: Row(children: _cards)))
    ]);
  }

  Widget _title() {
    return RichText(
        textAlign: TextAlign.center,
        text: const TextSpan(
            text: 'You have all chances to become\n',
            style: TextStyle(color: Colors.white70, fontSize: 32),
            children: <TextSpan>[
              TextSpan(
                  text: 'our client',
                  style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white)),
              TextSpan(text: ' if you are')
            ]));
  }
}

class _Card extends StatelessWidget {
  final String title;
  final String image;

  const _Card(this.image, this.title);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.all(8),
        height: 184,
        width: 200,
        decoration: BoxDecoration(
            color: Theme.of(context).cardColor, borderRadius: BorderRadius.circular(30)),
        child: Center(
            child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          SizedBox.fromSize(
              size: const Size.square(80),
              child: Image.asset('install/assets/home/client/$image.png')),
          const SizedBox(height: 16),
          Text(title, style: const TextStyle(fontSize: 24, color: Colors.white))
        ])));
  }
}
