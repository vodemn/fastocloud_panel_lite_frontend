import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class LoginBorderWrap extends StatelessWidget {
  final Widget child;
  final double width;

  const LoginBorderWrap({required this.child, this.width = 400});

  @override
  Widget build(BuildContext context) {
    final cont = Container(
        width: width,
        decoration: BoxDecoration(
            border: Border.all(
                color: Theme.of(context).brightness == Brightness.light
                    ? Colors.black38
                    : Colors.white38),
            borderRadius: const BorderRadius.all(Radius.circular(8.0))),
        child: child);
    return Padding(padding: const EdgeInsets.all(16.0), child: cont);
  }
}
