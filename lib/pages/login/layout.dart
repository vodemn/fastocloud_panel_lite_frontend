import 'package:fastocloud_pro_panel/common/logo.dart';
import 'package:fastocloud_pro_panel/pages/login/border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

abstract class LoginLayout<T extends StatefulWidget> extends State<T> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: ScrollableEx(builder: (controller) {
          return Center(child: SingleChildScrollView(controller: controller, child: _layouts()));
        }));
  }

  Widget _layouts() {
    return LayoutBuilder(builder: (context, constraints) {
      if (constraints.maxWidth <= 600) {
        return columnChildren();
      } else {
        return LoginBorderWrap(child: columnChildren());
      }
    });
  }

  Widget logoHeader(String title) {
    return Column(mainAxisSize: MainAxisSize.min, children: [
      const LogoButton(),
      const SizedBox(height: 24),
      Text(title, style: const TextStyle(fontWeight: FontWeight.w500, fontSize: 24)),
      const SizedBox(height: 16)
    ]);
  }

  Widget columnChildren();

  Widget mainButton(String title, void Function() onTap) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: SizedBox(
            width: double.maxFinite,
            height: 48,
            child: FlatButtonEx.filled(text: title, onPressed: onTap)));
  }
}
