import 'dart:convert';

import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/dashboard/profile/profile_loader.dart';
import 'package:fastocloud_pro_panel/dashboard/providers/providers_loader.dart';
import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/pages/login/layout.dart';
import 'package:fastocloud_pro_panel/profile.dart';
import 'package:fastocloud_pro_panel/routing/route_config.dart';
import 'package:fastocloud_pro_panel/routing/router_delegate.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/errors.dart';
import 'package:flutter_common/loader.dart';
import 'package:flutter_common/widgets.dart';
import 'package:http/http.dart';

class SignUpPage extends StatefulWidget {
  final bool initProvider;

  const SignUpPage() : initProvider = false;

  const SignUpPage.provider() : initProvider = true;

  @override
  // ignore: no_logic_in_create_state
  _SignUpPageState createState() {
    if (initProvider) {
      return _SignUpPageProviderState();
    }
    return _SignUpPageState();
  }
}

class _SignUpPageState extends LoginLayout<SignUpPage> {
  bool _canSignUp = false;

  late Future<Response> _future;

  String _email = '';
  String _password = '';
  String _firstName = '';
  String _lastName = '';

  bool _termsAndConditions = false;
  bool _checkEmail = false;

  String _currentCountry = '';
  String _currentLang = '';
  final List<List<String>> _countries = [];
  final List<List<String>> _languages = [];

  final FocusNode _signUpFocus = FocusNode();

  @override
  void initState() {
    super.initState();
    _init();
  }

  @override
  void dispose() {
    super.dispose();
    _signUpFocus.dispose();
  }

  void _init() {
    final fetcher = locator<Fetcher>();
    _future = fetcher.fetchGet('/locale');
  }

  @override
  Widget columnChildren() {
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: _checkEmail
                ? <Widget>[
                    CheckEmail(),
                    const SizedBox(height: 16),
                    FlatButtonEx.filled(text: 'Dismiss', onPressed: _toSignIn)
                  ]
                : <Widget>[
                    logoHeader('Create account'),
                    _names(),
                    _emailField(),
                    _passwordField(),
                    _locales(),
                    _termsAndConditionsText(),
                    const SizedBox(height: 8),
                    mainButton('Sign up', _signUp),
                    FlatButtonEx.notFilled(text: 'Have an account?', onPressed: () => _toSignIn())
                  ]));
  }

  Widget _names() {
    return LayoutBuilder(builder: (context, constraints) {
      if (constraints.maxWidth <= 600) {
        return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[_firstNameField(), _lastNameField()]);
      } else {
        return Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
          Expanded(child: _firstNameField()),
          Expanded(child: _lastNameField())
        ]);
      }
    });
  }

  Widget _firstNameField() {
    return TextFieldEx(
        hintText: 'First name',
        errorText: 'Enter your first name',
        init: _firstName,
        onFieldChanged: (term) {
          _firstName = term;
          _validate();
        },
        autofocus: true);
  }

  Widget _lastNameField() {
    return TextFieldEx(
        hintText: 'Last name',
        errorText: 'Enter your last name',
        init: _lastName,
        onFieldChanged: (term) {
          _lastName = term;
          _validate();
        });
  }

  Widget _emailField() {
    return TextFieldEx(
        hintText: 'Email',
        errorText: 'Enter your email adress',
        init: _email,
        onFieldChanged: (term) {
          _email = term;
          _validate();
        },
        onFieldSubmit: (term) => FocusScope.of(context).requestFocus(_signUpFocus),
        keyboardType: TextInputType.emailAddress,
        autofocus: true);
  }

  Widget _passwordField() {
    return PassWordTextField(
        hintText: 'Password',
        errorText: 'Enter a password',
        onFieldChanged: (term) {
          _password = term;
          _validate();
        },
        onFieldSubmit: (term) => FocusScope.of(context).requestFocus(_signUpFocus),
        init: _password);
  }

  Widget _locales() {
    return FutureBuilder<Response>(
        future: _future,
        builder: (context, snap) {
          if (snap.hasData) {
            if (snap.data!.statusCode == 200) {
              final locale = json.decode(snap.data!.body);
              for (final List c in locale['countries']) {
                _countries.add(c.cast<String>());
              }
              for (final List l in locale['languages']) {
                _languages.add(l.cast<String>());
              }
              _currentCountry = locale['current_country'];
              _currentLang = locale['current_language'];

              return Row(children: <Widget>[
                Expanded(flex: 3, child: _country()),
                Expanded(flex: 2, child: _language())
              ]);
            }
          }
          return const CircularProgressIndicator();
        });
  }

  Widget _country() {
    return LocalePicker(
        current: _currentCountry,
        values: _countries,
        title: 'Country',
        onChanged: (c) => _currentCountry = c!);
  }

  Widget _language() {
    return LocalePicker(
        current: _currentLang,
        values: _languages,
        title: 'Language',
        onChanged: (c) => _currentLang = c!);
  }

  Widget _termsAndConditionsText() {
    return CheckboxListTile(
        title: RichText(
            text: TextSpan(children: [
          const TextSpan(text: 'I have read ', style: TextStyle(color: Colors.black)),
          TextSpan(
              text: 'Terms and conditions',
              style: TextStyle(color: Theme.of(context).accentColor),
              recognizer: TapGestureRecognizer()..onTap = () => _onTerms())
        ])),
        value: _termsAndConditions,
        onChanged: (value) {
          setState(() {
            _termsAndConditions = value!;
          });
          _validate();
        });
  }

  void _validate() {
    final _fields = _email.isNotEmpty &&
        _password.isNotEmpty &&
        _firstName.isNotEmpty &&
        _lastName.isNotEmpty &&
        _termsAndConditions;
    if (_canSignUp != _fields) {
      setState(() {
        _canSignUp = _fields;
      });
    }
  }

  void _toSignIn() {
    RouterDelegateEx.of(context).setNewRoutePath(const RouteConfig.signIn());
  }

  void _onTerms() {
    RouterDelegateEx.of(context).setNewRoutePath(const RouteConfig.terms());
  }

  void _signUp() {
    _validate();
    if (!_canSignUp) {
      return;
    }

    final fetcher = locator<Fetcher>();
    final email = _email.toLowerCase().trim();
    final signupResponse = fetcher.fetchPost('/signup', {
      'email': email,
      'password': _password,
      'first_name': _firstName,
      'last_name': _lastName,
      'country': _currentCountry,
      'language': _currentLang
    }, [
      200,
      201
    ]);
    signupResponse.then((response) {
      if (response.statusCode == 201) {
        setState(() => _checkEmail = true);
      } else if (response.statusCode == 200) {
        final ProviderLoader _loader = ProviderLoader();
        _loader.load();
        _loader.stream().listen((state) {
          if (state is ProviderDataState) {
            final Provider profile = state.data;
            ProfileInfo.of(context)!.updateProfile(profile);
            RouterDelegateEx.of(context).toProfile();
          } else if (state is ItemErrorState) {
            showError(context, state.error as ErrorEx);
          }
        });
      }
    }, onError: (error) {
      showError(context, error);
    });
  }
}

class CheckEmail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const NonAvailableBuffer(
        icon: Icons.mail, message: 'Check your inbox for\nconfirmation message');
  }
}

class _SignUpPageProviderState extends _SignUpPageState {
  @override
  void _signUp() {
    _validate();
    if (!_canSignUp) {
      return;
    }

    final email = _email.toLowerCase().trim();
    final Provider _provider = Provider.createDefault()
      ..email = email
      ..password = _password
      ..firstName = _firstName
      ..lastName = _lastName
      ..country = _currentCountry
      ..language = _currentLang;
    ProvidersLoader().createProvider(_provider).then((response) {
      if (response.statusCode == 200) {
        _toSignIn();
      }
    }, onError: (error) {
      showError(context, error);
    });
  }
}
