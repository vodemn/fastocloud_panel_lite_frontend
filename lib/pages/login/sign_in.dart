import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/dashboard/profile/profile_loader.dart';
import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/pages/login/layout.dart';
import 'package:fastocloud_pro_panel/profile.dart';
import 'package:fastocloud_pro_panel/routing/route_config.dart';
import 'package:fastocloud_pro_panel/routing/router_delegate.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:fastocloud_pro_panel/shared_prefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/errors.dart';
import 'package:flutter_common/loader.dart';
import 'package:flutter_common/widgets.dart';

class SignInPage extends StatefulWidget {
  final String? token;

  const SignInPage({required this.token});

  @override
  _SignInPageState createState() {
    return _SignInPageState();
  }
}

class _SignInPageState extends LoginLayout<SignInPage> {
  bool _canSignIn = false;

  String _email = '';
  String _password = '';

  bool _remember = false;

  final FocusNode _signInFocus = FocusNode();

  @override
  void initState() {
    super.initState();
    _init();
    final settings = locator<LocalStorageService>();
    _remember = settings.check();
    if (_remember) {
      _email = settings.email() ?? '';
      _password = settings.password() ?? '';
      _validate();
      WidgetsBinding.instance!.addPostFrameCallback((_) => _signIn());
    }
  }

  @override
  void dispose() {
    super.dispose();
    _signInFocus.dispose();
  }

  @override
  Widget columnChildren() {
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              logoHeader('Sign in'),
              TextFieldEx(
                  hintText: 'Email',
                  errorText: 'Enter your email adress',
                  init: _email,
                  onFieldChanged: (term) {
                    _email = term;
                  },
                  onFieldSubmit: (term) => FocusScope.of(context).requestFocus(_signInFocus),
                  keyboardType: TextInputType.emailAddress,
                  autofocus: true),
              PassWordTextField(
                  hintText: 'Password',
                  errorText: 'Enter a password',
                  onFieldChanged: (term) {
                    _password = term;
                  },
                  onFieldSubmit: (term) => FocusScope.of(context).requestFocus(_signInFocus),
                  init: _password),
              _rememberMe(),
              mainButton('Sign in', _signIn),
              FlatButtonEx.notFilled(text: 'Create account', onPressed: _toSignUp)
            ]));
  }

  Widget _rememberMe() {
    return CheckboxListTile(
        value: _remember,
        onChanged: (bool? value) {
          setState(() {
            _remember = value!;
          });
          final settings = locator<LocalStorageService>();
          settings.setCheck(_remember);
        },
        title: const Text('Remember me'),
        controlAffinity: ListTileControlAffinity.trailing);
  }

  void _init() {
    final token = widget.token;
    if (token != null) {
      final fetcher = locator<Fetcher>();
      final confirmEmailResponse = fetcher.fetchGet('/confirm_email/$token');
      confirmEmailResponse.then((response) {
        WidgetsBinding.instance!.addPostFrameCallback((_) => showDialog(
            context: context,
            builder: (context) {
              return const AlertDialog(
                  title: Text('Email confirmed'), content: Text('Thanks for registration'));
            }));
      }, onError: (error) {
        showError(context, error);
      });
    }
  }

  void _validate() {
    final _fields = _email.isNotEmpty && _password.isNotEmpty;
    _canSignIn = _fields;
  }

  void _toSignUp() {
    RouterDelegateEx.of(context).setNewRoutePath(const RouteConfig.signUp());
  }

  void _signIn() {
    _validate();
    if (!_canSignIn) {
      return;
    }

    final fetcher = locator<Fetcher>();
    final email = _email.toLowerCase().trim();
    if (_remember) {
      final settings = locator<LocalStorageService>();
      settings.setEmail(_email);
      settings.setPassword(_password);
    }
    final signinResponse = fetcher.login('/signin', {'email': email, 'password': _password});
    signinResponse.then((response) {
      final ProviderLoader _loader = ProviderLoader();
      _loader.load();
      _loader.stream().listen((state) {
        if (state is ProviderDataState) {
          final Provider profile = state.data;
          ProfileInfo.of(context)!.updateProfile(profile);
          RouterDelegateEx.of(context).toProfile();
        } else if (state is ItemErrorState) {
          showError(context, state.error as ErrorEx);
        }
      });
    }, onError: (error) {
      showError(context, error);
    });
  }
}
