import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:player/common/states.dart';
import 'package:player/controller.dart';
import 'package:player/widgets/timeline.dart';

abstract class _OutputBottomControls extends StatefulWidget {
  final PlayerController controller;
  final void Function()? onPrev;
  final void Function()? onNext;

  const _OutputBottomControls({required this.controller, this.onPrev, this.onNext});
}

abstract class _OutputBottomControlsState<T extends _OutputBottomControls> extends State<T> {
  PlayerController get controller => widget.controller;

  Color get disabled => Theme.of(context).disabledColor;

  Color get active => Colors.black;

  Widget previousButton() {
    if (widget.onPrev == null) {
      return const SizedBox();
    }
    return PlayerButtons.previous(onPressed: widget.onPrev!, color: active);
  }

  Widget nextButton() {
    if (widget.onNext == null) {
      return const SizedBox();
    }
    return PlayerButtons.next(onPressed: widget.onNext!, color: active);
  }

  Widget playButton() {
    return PlayerButtons.play(
        onPressed: () {
          controller.play();
          setState(() {});
        },
        color: active);
  }

  Widget pauseButton() {
    return PlayerButtons.pause(
        onPressed: () {
          controller.pause();
          setState(() {});
        },
        color: active);
  }

  Widget playPauseButton() {
    return PlayerStateListener(widget.controller, builder: (_) {
      if (controller.isPlaying()) {
        return pauseButton();
      }
      return playButton();
    }, placeholder: PlayerButtons.play(color: disabled));
  }
}

class StreamBottomControls extends _OutputBottomControls {
  const StreamBottomControls(
      {required PlayerController controller, void Function()? onPrev, void Function()? onNext})
      : super(controller: controller, onPrev: onPrev, onNext: onNext);

  @override
  _StreamBottomControlsState createState() {
    return _StreamBottomControlsState();
  }
}

class _StreamBottomControlsState extends _OutputBottomControlsState<StreamBottomControls> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[previousButton(), playPauseButton(), nextButton()]));
  }
}

class VodBottomControls extends _OutputBottomControls {
  const VodBottomControls(
      {required PlayerController controller, void Function()? onPrev, void Function()? onNext})
      : super(controller: controller, onPrev: onPrev, onNext: onNext);

  @override
  _VodBottomControlsState createState() {
    return _VodBottomControlsState();
  }
}

class _VodBottomControlsState extends _OutputBottomControlsState<VodBottomControls> {
  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      _timeLine(),
      Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            previousButton(),
            _seekBackward(),
            playPauseButton(),
            _seekForward(),
            nextButton()
          ]))
    ]);
  }

  Widget _timeLine() {
    return LitePlayerTimeline(controller);
  }

  Widget _seekBackward() {
    return PlayerStateListener(widget.controller, builder: (_) {
      return PlayerButtons.seekBackward(onPressed: controller.seekBackward, color: active);
    }, placeholder: PlayerButtons.seekBackward(color: disabled));
  }

  Widget _seekForward() {
    return PlayerStateListener(widget.controller, builder: (_) {
      return PlayerButtons.seekForward(onPressed: controller.seekForward, color: active);
    }, placeholder: PlayerButtons.seekForward(color: disabled));
  }
}
