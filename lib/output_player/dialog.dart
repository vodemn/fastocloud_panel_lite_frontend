import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/output_player/bottom_controls.dart';
import 'package:fastocloud_pro_panel/output_player/output_list.dart';
import 'package:flutter/material.dart';
import 'package:player/controller.dart';
import 'package:player/widgets/player.dart';
import 'package:responsive_builder/responsive_builder.dart';

class OutputPlayerDialog extends StatefulWidget {
  final String title;
  final StreamType type;
  final List<OutputUrl> outputUrls;

  const OutputPlayerDialog(this.title, this.type, this.outputUrls);

  @override
  _OutputPlayerDialogState createState() {
    return _OutputPlayerDialogState();
  }
}

class _OutputPlayerDialogState extends State<OutputPlayerDialog> {
  late PlayerController _controller;

  final playerKey = GlobalKey();

  int currentPos = 0;

  bool get _isVod =>
      widget.type == StreamType.CATCHUP ||
      widget.type == StreamType.VOD_PROXY ||
      widget.type == StreamType.VOD_RELAY ||
      widget.type == StreamType.VOD_ENCODE;

  OutputUrl get currentUrl {
    return widget.outputUrls[currentPos];
  }

  @override
  void initState() {
    super.initState();
    _controller = PlayerController(initLink: currentUrl.uri);
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInformation) {
      if (sizingInformation.deviceScreenType == DeviceScreenType.mobile) {
        return _dialog(_mobile());
      }
      return _dialog(_desktop());
    });
  }

  Widget _dialog(Widget child) {
    return AlertDialog(
        title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[Text(widget.title), _closeButton()]),
        titlePadding: const EdgeInsets.fromLTRB(24, 8, 8, 8),
        contentPadding: const EdgeInsets.all(0),
        content: child);
  }

  Widget _mobile() {
    return SingleChildScrollView(
        child: SizedBox(
            width: double.maxFinite,
            child: Column(children: <Widget>[
              _playerArea(),
              _bottomControls(),
              const Divider(),
              _list(true)
            ])));
  }

  Widget _desktop() {
    double height = MediaQuery.of(context).size.height - 24 * 2 - 72;
    if (height > 600) {
      height = 600;
    }
    return SizedBox(
        height: height,
        width: (height - 56) * (16 / 9) + 128,
        child: Row(children: <Widget>[
          Expanded(
              child: SingleChildScrollView(
                  child: Column(children: <Widget>[_playerArea(), _bottomControls()]))),
          const VerticalDivider(width: 0),
          SizedBox(width: 128, child: _list(false))
        ]));
  }

  Widget _closeButton() {
    return IconButton(
        icon: const Icon(Icons.close, color: Colors.black), onPressed: Navigator.of(context).pop);
  }

  Widget _list(bool shrinkWrap) {
    return OutputUrlsList(widget.outputUrls, currentPos, Colors.black, _moveToUrl, shrinkWrap);
  }

  Widget _playerArea() {
    return LitePlayer(controller: _controller);
  }

  Widget _bottomControls() {
    return _isVod
        ? VodBottomControls(controller: _controller, onPrev: _moveToPrevUrl, onNext: _moveToNextUrl)
        : StreamBottomControls(
            controller: _controller, onPrev: _moveToPrevUrl, onNext: _moveToNextUrl);
  }

  void _playChannel() {
    _controller.setVideoLink(currentUrl.uri);
  }

  void _moveToPrevUrl() {
    if (widget.outputUrls.length > 1) {
      currentPos == 0 ? currentPos = widget.outputUrls.length - 1 : currentPos--;
      _playChannel();
    }
  }

  void _moveToNextUrl() {
    if (widget.outputUrls.length > 1) {
      currentPos == widget.outputUrls.length - 1 ? currentPos = 0 : currentPos++;
      _playChannel();
    }
  }

  void _moveToUrl(int index) {
    if (index != currentPos) {
      currentPos = index;
      _playChannel();
    }
  }
}
