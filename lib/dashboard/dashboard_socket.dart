import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/event_bus.dart';
import 'package:fastocloud_pro_panel/events/provider_events.dart';
import 'package:fastocloud_pro_panel/events/server_events.dart';
import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:socket_io_client/socket_io_client.dart' as io;

class LogSocket {
  late io.Socket _socketIO;
  final String id;

  static const PROVIDER_ADDED = 'provider_added';
  static const PROVIDER_UPDATED = 'provider_updated';
  static const PROVIDER_REMOVED = 'provider_removed';

  static const SERVER_ADDED = 'server_added';
  static const SERVER_UPDATED = 'server_updated';
  static const SERVER_REMOVED = 'server_removed';

  LogSocket(this.id) {
    _initSocket();
  }

  void _initSocket() {
    final fetcher = locator<Fetcher>();
    final eventBus = locator<FastoEventBus>();
    _socketIO = fetcher.createLogSocketIO();

    _socketIO.on(PROVIDER_ADDED, (json) {
      final _subs = Provider.fromJson(json);
      eventBus.publish(ProviderAddEvent(_subs));
    });
    _socketIO.on(PROVIDER_UPDATED, (json) {
      final _subs = Provider.fromJson(json);
      eventBus.publish(ProviderEditEvent(_subs));
    });
    _socketIO.on(PROVIDER_REMOVED, (json) {
      final _subs = Provider.fromJson(json);
      eventBus.publish(ProviderRemoveEvent(_subs));
    });

    _socketIO.on(SERVER_ADDED, (json) {
      final _server = Server.fromJson(json);
      final _contains = _checkProfile(_server.providers);
      if (_contains) {
        eventBus.publish(ServerAddEvent(_server));
      }
    });
    _socketIO.on(SERVER_UPDATED, (json) {
      final _server = Server.fromJson(json);
      final _contains = _checkProfile(_server.providers);
      if (_contains) {
        eventBus.publish(ServerEditEvent(_server));
      }
    });
    _socketIO.on(SERVER_REMOVED, (json) {
      final _server = Server.fromJson(json);
      final _contains = _checkProfile(_server.providers);
      if (_contains) {
        eventBus.publish(ServerRemoveEvent(_server));
      }
    });
  }

  bool _checkProfile(List<ServerProvider> providers) {
    for (final ServerProvider p in providers) {
      if (p.id == id) {
        return true;
      }
    }
    return false;
  }

  void dispose() {
    _socketIO.disconnect();
    _socketIO.destroy();
  }
}
