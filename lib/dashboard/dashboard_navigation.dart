import 'package:fastocloud_pro_panel/dashboard/drawer.dart';
import 'package:fastocloud_pro_panel/localization/translations.dart';
import 'package:fastocloud_pro_panel/models/destination.dart';
import 'package:fastocloud_pro_panel/routing/route_config.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class DashBoardNavigation extends StatelessWidget {
  final String route;
  final Widget child;

  static List<Destination> destinations(bool isAdmin) {
    return isAdmin
        ? const [
            Destination(RouteConfig.profile(), TR_PROFILE, Icons.dashboard),
            Destination(RouteConfig.providers(), TR_PROVIDERS, Icons.accessibility),
            Destination(RouteConfig.servers(), TR_SERVERS, Icons.wifi_tethering)
          ]
        : const [Destination(RouteConfig.profile(), TR_PROFILE, Icons.dashboard)];
  }

  const DashBoardNavigation(this.route, this.child);

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInformation) {
      if (sizingInformation.isDesktop) {
        return Row(children: <Widget>[
          DashboardDrawer(route),
          const VerticalDivider(thickness: 1, width: 0),
          Expanded(child: child)
        ]);
      } else {
        return child;
      }
    });
  }
}
