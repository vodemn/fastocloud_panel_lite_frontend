import 'package:fastocloud_pro_panel/constants.dart';
import 'package:fastocloud_pro_panel/dashboard/dashboard_navigation.dart';
import 'package:fastocloud_pro_panel/models/destination.dart';
import 'package:fastocloud_pro_panel/profile.dart';
import 'package:fastocloud_pro_panel/routing/route_config.dart';
import 'package:fastocloud_pro_panel/routing/router_delegate.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

class DashboardDrawer extends StatefulWidget {
  final String route;

  const DashboardDrawer(this.route);

  @override
  _DashboardDrawerState createState() {
    return _DashboardDrawerState();
  }
}

class _DashboardDrawerState extends State<DashboardDrawer> {
  @override
  Widget build(BuildContext context) {
    return DesktopDrawer((collapsed) {
      return [_logo(), ...dashboardTiles(collapsed), _version(collapsed)];
    });
  }

  List<Widget> dashboardTiles(bool collapsed) {
    final bool isAdmin = ProfileInfo.of(context)!.profile?.isAdmin() ?? false;
    final List<Destination> destinations = DashBoardNavigation.destinations(isAdmin);
    return List<Widget>.generate(destinations.length, (index) {
      return _tile(destinations[index], collapsed);
    });
  }

  Widget _logo() {
    return const Align(
        child: DrawerHeader(child: CustomAssetLogo(LOGO_PATH, width: 320, height: 180)));
  }

  Widget _tile(Destination d, bool collapsed) {
    return _DrawerTile(
        selected: widget.route == d.route.path,
        icon: d.icon,
        title: d.title,
        collapsed: collapsed,
        onPressed: () {
          _navigate(d.route);
        });
  }

  Widget _version(bool collapsed) {
    final package = locator<PackageManager>();
    final String version = package.version();
    return _DrawerTile(icon: Icons.info, title: version, collapsed: collapsed);
  }

  void _navigate(RouteConfig route) {
    RouterDelegateEx.of(context).setNewRoutePath(route);
  }
}

class DesktopDrawer extends StatefulWidget {
  final List<Widget> Function(bool) tiles;

  const DesktopDrawer(this.tiles);

  @override
  _DesktopDrawerState createState() {
    return _DesktopDrawerState();
  }
}

class _DesktopDrawerState extends State<DesktopDrawer> {
  bool _collapsed = false;
  bool _collapsedTile = false;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
        width: _collapsed ? 56 : 210,
        duration: const Duration(milliseconds: 200),
        onEnd: () {
          if (!_collapsed) {
            setState(() => _collapsedTile = false);
          }
        },
        child: Column(children: [
          Expanded(
              child: Align(
                  alignment: Alignment.topLeft,
                  child: SingleChildScrollView(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: widget.tiles(_collapsedTile))))),
          const Divider(thickness: 1, height: 0),
          _DrawerTile(
              icon: _collapsedTile ? Icons.arrow_forward_ios : Icons.arrow_back_ios,
              onPressed: () {
                setState(() {
                  _collapsed = !_collapsed;
                  if (_collapsed) {
                    _collapsedTile = true;
                  }
                });
              },
              collapsed: _collapsedTile,
              title: 'Collapse sidebar')
        ]));
  }
}

class _DrawerTile extends StatelessWidget {
  final IconData icon;
  final void Function()? onPressed;
  final String title;
  final bool selected;
  final bool collapsed;

  const _DrawerTile(
      {required this.icon,
      required this.title,
      this.onPressed,
      this.selected = false,
      this.collapsed = false});

  @override
  Widget build(BuildContext context) {
    if (!collapsed) {
      return _tile(context);
    }
    return Tooltip(message: title, child: _tile(context));
  }

  Widget _tile(BuildContext context) {
    Color? color;
    if (selected) {
      color = Theme.of(context).primaryColor;
    }
    return SizedBox(
        height: 56,
        child: InkWell(
            onTap: onPressed?.call,
            child: Row(children: [
              Padding(padding: const EdgeInsets.all(16), child: Icon(icon, color: color)),
              if (!collapsed)
                Text(title,
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: color, fontWeight: selected ? FontWeight.bold : FontWeight.normal))
            ])));
  }
}
