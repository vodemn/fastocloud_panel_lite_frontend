import 'dart:async';
import 'dart:convert';

import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter_common/loader.dart';
import 'package:http/http.dart';

class ProvidersLoader extends ItemBloc<List<Provider>> {
  @override
  Future<ProvidersDataState> loadDataRoute() {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchGet('/provider/providers');
    return response.then((value) {
      final List<Provider> result = [];
      final data = json.decode(value.body);
      data['providers'].forEach((s) {
        final res = Provider.fromJson(s);
        result.add(res);
      });
      return ProvidersDataState(result);
    }, onError: (error) {
      throw error;
    });
  }

  Future<Response> createProvider(Provider provider) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchPost('/provider/create', provider.toJson(), [200]);
    return response;
  }

  Future<Response> removeProviderByID(String sid) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchDelete('/provider/remove/$sid', [201]);
    return response;
  }

  Future<Response> addProvider(Provider provider) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchPost('/provider/add', provider.toJson(), [201]);
    return response;
  }

  Future<Response> editProvider(Provider provider) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchPatch('/provider/edit/${provider.id}', provider.toJson(), [201]);
    return response;
  }
}

class ProvidersDataState extends ItemDataState<List<Provider>> {
  ProvidersDataState(List<Provider> data) : super(data);
}
