import 'dart:convert';

import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/add_edit_dialog.dart';
import 'package:fastocloud_pro_panel/dashboard/providers/providers_loader.dart';
import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:http/http.dart';

class ProviderDialog extends StatefulWidget {
  final Provider init;
  final ProvidersLoader loader;

  ProviderDialog.add(this.loader) : init = Provider.createDefault();

  ProviderDialog.edit(this.loader, Provider provider) : init = provider.copy() {
    init.password = null; // should be save without password changes
  }

  @override
  _ProviderDialogState createState() => _ProviderDialogState();
}

class _ProviderDialogState extends State<ProviderDialog> {
  late Provider _prov;
  late Future<Response> _future;

  final List<List<String>> _countries = [];
  final List<List<String>> _languages = [];

  @override
  void initState() {
    super.initState();
    _prov = widget.init;
    _init();
  }

  bool isAdd() {
    return _prov.id == null;
  }

  @override
  Widget build(BuildContext context) {
    return AddEditDialog.narrow(
        add: isAdd(),
        children: <Widget>[
          _firstNameField(),
          _lastNameField(),
          _emailField(),
          _passwordField(),
          _status(),
          _credits(),
          _type(),
          _locales()
        ],
        onSave: _save);
  }

  Widget _firstNameField() {
    return TextFieldEx(
        hintText: 'First name',
        errorText: 'Enter your first name',
        minSymbols: Provider.MIN_NAME,
        maxSymbols: Provider.MAX_NAME,
        init: _prov.firstName,
        onFieldChanged: (term) {
          _prov.firstName = term;
        });
  }

  Widget _lastNameField() {
    return TextFieldEx(
        hintText: 'Last name',
        errorText: 'Enter your last name',
        minSymbols: Provider.MIN_NAME,
        maxSymbols: Provider.MAX_NAME,
        init: _prov.lastName,
        onFieldChanged: (term) {
          _prov.lastName = term;
        });
  }

  Widget _emailField() {
    return TextFieldEx(
        hintText: 'Email',
        errorText: 'Enter your email adress',
        init: _prov.email,
        onFieldChanged: (term) {
          _prov.email = term;
        },
        keyboardType: TextInputType.emailAddress);
  }

  Widget _passwordField() {
    return PassWordTextField(
        hintText: 'Password',
        errorText: isAdd() ? 'Enter a password' : null,
        onFieldChanged: (term) {
          _prov.password = term;
        });
  }

  Widget _locales() {
    return FutureBuilder<Response>(
        future: _future,
        builder: (context, snap) {
          if (snap.hasData) {
            final _response = snap.data!;
            if (_response.statusCode == 200) {
              final locale = json.decode(_response.body);

              for (final List c in locale['countries']) {
                _countries.add(c.cast<String>());
              }
              for (final List l in locale['languages']) {
                _languages.add(l.cast<String>());
              }
              _prov.country ??= locale['current_country'];
              _prov.language ??= locale['current_language'];

              return Column(
                  mainAxisSize: MainAxisSize.min, children: <Widget>[_country(), _language()]);
            }
          }
          return const Center(child: CircularProgressIndicator());
        });
  }

  Widget _country() {
    return LocalePicker(
        current: _prov.country,
        values: _countries,
        title: 'Country',
        onChanged: (c) {
          _prov.country = c;
        });
  }

  Widget _language() {
    return LocalePicker(
        current: _prov.language,
        values: _languages,
        title: 'Language',
        onChanged: (c) {
          _prov.language = c;
        });
  }

  Widget _status() {
    return DropdownButtonEx<ProviderStatus>(
        hint: _prov.status.toHumanReadable(),
        value: _prov.status,
        values: ProviderStatus.values,
        onChanged: (c) {
          setState(() {
            _prov.status = c!;
          });
        },
        itemBuilder: (ProviderStatus value) {
          return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
        });
  }

  Widget _type() {
    return DropdownButtonEx<ProviderType>(
        hint: _prov.type.toHumanReadable(),
        value: _prov.type,
        values: ProviderType.values,
        onChanged: (c) {
          setState(() {
            _prov.type = c!;
          });
        },
        itemBuilder: (ProviderType value) {
          return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
        });
  }

  Widget _credits() {
    return NumberTextField.integer(
        minInt: Credits.MIN,
        maxInt: Credits.MAX,
        hintText: 'Credits',
        canBeEmpty: false,
        initInt: _prov.credits,
        onFieldChangedInt: (term) {
          if (term != null) _prov.credits = term;
        });
  }

  void _init() {
    final fetcher = locator<Fetcher>();
    _future = fetcher.fetchGet('/locale');
  }

  void _save() {
    if (!_prov.isValid()) {
      return;
    }

    final Future<Response> _response =
        isAdd() ? widget.loader.addProvider(_prov) : widget.loader.editProvider(_prov);
    _response.then((response) {
      Navigator.of(context).pop();
    }, onError: (error) {
      showError(context, error);
    });
  }
}
