import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/dashboard/providers/data_table/data_source.dart';
import 'package:fastocloud_pro_panel/dashboard/providers/provider_dialog.dart';
import 'package:fastocloud_pro_panel/dashboard/providers/providers_loader.dart';
import 'package:fastocloud_pro_panel/localization/translations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/data_table.dart';
import 'package:flutter_common/flutter_common.dart';

class ProvidersListLayout extends StatefulWidget {
  final ProviderSource dataSource;
  final ProvidersLoader loader;

  const ProvidersListLayout(this.dataSource, this.loader);

  @override
  _ProvidersListLayoutState createState() {
    return _ProvidersListLayoutState();
  }
}

class _ProvidersListLayoutState extends State<ProvidersListLayout> {
  @override
  Widget build(BuildContext context) {
    return DataLayout<Provider>.withHeader(
        dataSource: widget.dataSource,
        header: _header(),
        singleItemActions: singleStreamActions,
        multipleItemActions: multipleStreamActions,
        canScrollTable: true,
        title: TR_PROVIDERS);
  }

  Widget _header() {
    return DataTableSearchHeader(
        source: widget.dataSource,
        actions: <Widget>[FlatButtonEx.filled(text: TR_ADD, onPressed: _add)]);
  }

  List<IconButton> singleStreamActions(Provider p) {
    return [
      CommonActionIcon.edit(() => edit(p)),
      CommonActionIcon.remove(() => remove([p])),
    ];
  }

  List<IconButton> multipleStreamActions(List<Provider> p) {
    return [CommonActionIcon.remove(() => remove(p))];
  }

  void _add() {
    showDialog(context: context, builder: (context) => ProviderDialog.add(widget.loader));
  }

  void edit(Provider provider) {
    showDialog(
        context: context, builder: (context) => ProviderDialog.edit(widget.loader, provider));
  }

  void remove(List<Provider> providers) {
    for (final provider in providers) {
      final future = widget.loader.removeProviderByID(provider.id!);
      future.then((response) {}, onError: (error) {
        showError(context, error);
      });
    }
  }
}
