import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/localization/translations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

String _date(int milliseconds) => DateTime.fromMillisecondsSinceEpoch(milliseconds).toString();

class ProviderSource extends DataSource<Provider> {
  ProviderSource({required List<DataEntry<Provider>> items}) : super(items: items);

  @override
  String get itemsName => 'providers';

  @override
  bool searchCondition(String text, Provider sub) {
    return sub.email.toLowerCase().contains(text.toLowerCase());
  }

  @override
  Widget get noItems => _NoSubsOnline();

  @override
  List<Widget> headers() =>
      const [Text(TR_EMAIL), Text(TR_CREATED_DATE), Text(TR_STATUS), Text(TR_TYPE)];

  @override
  List<Widget> tiles(Provider s) => [
        Text(s.email),
        Text(_date(s.createdDate)),
        Text(s.status.toHumanReadable()),
        Text(s.type.toHumanReadable())
      ];

  @override
  bool equalItemsCondition(Provider item, Provider listItem) => listItem.id == item.id;
}

class _NoSubsOnline extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Center(
        child: NonAvailableBuffer(icon: Icons.error, message: 'Please create Providers'));
  }
}
