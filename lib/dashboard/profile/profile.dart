import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/profile.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class Profile extends StatelessWidget {
  const Profile();

  @override
  Widget build(BuildContext context) {
    return Align(
        alignment: Alignment.topCenter,
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ScreenTypeLayout(desktop: _desktop(context), mobile: _mobile(context))));
  }

  Widget _mobile(BuildContext context) {
    final Provider profile = ProfileInfo.of(context)!.profile!;
    final String creditsText =
        profile.isAdmin() ? 'Unlimited' : profile.creditsRemaining.toString();
    final content = Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(child: ProfileTile('First name', profile.firstName)),
          Expanded(child: ProfileTile('Last name', profile.lastName)),
          Expanded(child: ProfileTile('Email', profile.email)),
          Expanded(child: ProfileTile('Status', profile.status.toHumanReadable())),
          Expanded(child: ProfileTile('Type', profile.type.toHumanReadable())),
          Expanded(child: ProfileTile('Credits', creditsText))
        ]);
    return content;
  }

  Widget _desktop(BuildContext context) {
    final Provider profile = ProfileInfo.of(context)!.profile!;
    final String creditsText =
        profile.isAdmin() ? 'Unlimited' : profile.creditsRemaining.toString();
    final content = Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Expanded(flex: 4, child: ProfileTile('First name', profile.firstName)),
                Expanded(flex: 4, child: ProfileTile('Last name', profile.lastName)),
                Expanded(flex: 4, child: ProfileTile('Email', profile.email))
              ]),
          Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Expanded(flex: 4, child: ProfileTile('Status', profile.status.toHumanReadable())),
                Expanded(flex: 4, child: ProfileTile('Type', profile.type.toHumanReadable())),
                Expanded(flex: 4, child: ProfileTile('Credits', creditsText))
              ])
        ]);
    return content;
  }
}

class BoldText extends StatelessWidget {
  static const BOLD_TEXT_SIZE = 20.0;

  final String title;

  const BoldText(this.title);

  @override
  Widget build(BuildContext context) {
    return Text(title,
        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: BOLD_TEXT_SIZE));
  }
}

class ProfileTile extends StatelessWidget {
  static const TILE_HEIGHT = 98.0;

  final String title;
  final String value;

  const ProfileTile(this.title, this.value);

  @override
  Widget build(BuildContext context) {
    final child = Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(title, maxLines: 1, overflow: TextOverflow.fade),
          const SizedBox(height: 4, width: double.infinity),
          BoldText(value)
        ]);
    final Widget content = SizedBox(
        height: TILE_HEIGHT, child: Padding(padding: const EdgeInsets.all(16), child: child));
    return Card(child: content);
  }
}
