import 'dart:async';
import 'dart:convert';

import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter_common/loader.dart';

class ProviderLoader extends ItemBloc<Provider> {
  @override
  Future<ProviderDataState> loadDataRoute() {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchGet('/provider/profile');
    return response.then((value) {
      final data = json.decode(value.body);
      final provider = Provider.fromJson(data['user']);
      return ProviderDataState(provider);
    }, onError: (error) {
      throw error;
    });
  }
}

class ProviderDataState extends ItemDataState<Provider> {
  ProviderDataState(Provider data) : super(data);
}
