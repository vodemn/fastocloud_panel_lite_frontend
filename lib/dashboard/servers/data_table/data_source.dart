import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

class ServerSource extends DataSource<Server> {
  ServerSource({required List<DataEntry<Server>> items}) : super(items: items);

  @override
  String get itemsName {
    return 'servers';
  }

  @override
  bool searchCondition(String text, Server server) =>
      server.name.toLowerCase().contains(text.toLowerCase());

  @override
  Widget get noItems {
    return _NoServers();
  }

  @override
  List<Widget> headers() {
    return const [
      Text('Name'),
      Text('Host'),
      Text('Http host'),
      Text('VODs host'),
      Text('CODs host'),
      Text('Nginx host'),
      Text('Feedback'),
      Text('Timeshifts'),
      Text('HLS'),
      Text('VODs'),
      Text('CODs'),
      Text('External'),
      Text('Data')
    ];
  }

  @override
  List<Widget> tiles(Server s) {
    return [
      Text(s.name),
      Text(s.host.toString()),
      Text(s.httpHost.toString()),
      Text(s.vodsHost.toString()),
      Text(s.codsHost.toString()),
      Text(s.nginxHost.toString()),
      Text(s.feedbackDirectory),
      Text(s.timeshiftsDirectory),
      Text(s.hlsDirectory),
      Text(s.vodsDirectory),
      Text(s.codsDirectory),
      Text(s.proxyDirectory),
      Text(s.dataDirectory)
    ];
  }

  @override
  bool equalItemsCondition(Server item, Server listItem) {
    return listItem.id == item.id;
  }
}

class _NoServers extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Center(
        child: NonAvailableBuffer(icon: Icons.error, message: 'Please create Servers'));
  }
}
