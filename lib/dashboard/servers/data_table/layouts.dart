import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/provider_dialog.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/data_table/data_source.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/server_dialog.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/servers_loader.dart';
import 'package:fastocloud_pro_panel/localization/translations.dart';
import 'package:fastocloud_pro_panel/profile.dart';
import 'package:fastocloud_pro_panel/routing/route_config.dart';
import 'package:fastocloud_pro_panel/routing/router_delegate.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/data_table.dart';
import 'package:flutter_common/flutter_common.dart';

class ServersListLayout extends StatefulWidget {
  final ServerSource dataSource;
  final ServersLoader loader;

  const ServersListLayout(this.dataSource, this.loader);

  @override
  _ServersListLayoutState createState() {
    return _ServersListLayoutState();
  }
}

class _ServersListLayoutState extends State<ServersListLayout> {
  @override
  Widget build(BuildContext context) {
    return DataLayout<Server>.withHeader(
        dataSource: widget.dataSource,
        header: _header(context),
        singleItemActions: singleStreamActions,
        multipleItemActions: multipleStreamActions,
        canScrollTable: true,
        title: TR_SERVERS);
  }

  Widget _header(BuildContext context) {
    final List<Widget> _actions = [FlatButtonEx.filled(text: 'Add', onPressed: _add)];
    return DataTableSearchHeader(source: widget.dataSource, actions: _actions);
  }

  List<Widget> singleStreamActions(Server server) {
    final ProviderRole _role = server.getProviderRoleById(ProfileInfo.of(context)!.profile!.id!);
    if (_role == ProviderRole.ADMIN) {
      return [
        detailsButton(server),
        addProviderButton(server),
        removeProviderButton(server),
        CommonActionIcon.edit(() => _edit(server)),
        CommonActionIcon.remove(() => _remove([server])),
      ];
    }
    return [detailsButton(server)];
  }

  List<Widget> multipleStreamActions(List<Server> servers) {
    ProviderRole _role;
    for (final Server server in servers) {
      _role = server.getProviderRoleById(ProfileInfo.of(context)!.profile!.id!);
      if (_role != ProviderRole.ADMIN) {
        return [];
      }
    }
    return [CommonActionIcon.remove(() => _remove(servers))];
  }

  Widget addProviderButton(Server s) {
    return CommonActionIcon.addProvider(() => _addProvider(s));
  }

  Widget removeProviderButton(Server s) {
    return CommonActionIcon.removeProvider(() => _removeProvider(s));
  }

  Widget detailsButton(Server s) {
    return CommonActionIcon.details(() => _toDetails(s));
  }

  void _edit(Server server) {
    showDialog(context: context, builder: (context) => ServerDialog.edit(widget.loader, server));
  }

  void _add() {
    showDialog(context: context, builder: (context) => ServerDialog.add(widget.loader));
  }

  void _remove(List<Server> servers) {
    for (final server in servers) {
      final future = widget.loader.removeServerByID(server.id!);
      future.then((response) {}, onError: (error) {
        showError(context, error);
      });
    }
  }

  void _addProvider(Server server) {
    showDialog(
        context: context,
        builder: (context) {
          return AddRemoveProviderDialog.add(server.id!, 'server', server.providers);
        });
  }

  void _removeProvider(Server server) {
    showDialog(
        context: context,
        builder: (context) {
          return AddRemoveProviderDialog.remove(server.id!, 'server', server.providers);
        });
  }

  void _toDetails(Server s) {
    RouterDelegateEx.of(context).setNewRoutePath(DetailsRouteConfig.servers(s.id!));
  }
}
