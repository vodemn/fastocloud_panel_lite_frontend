import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/server_status_notifier.dart';
import 'package:fastocloud_pro_panel/common/streams/vods/scan_folder_vods.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/server_loader.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/add_button.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/hardware/data_source.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/hardware/layouts.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/istream/data_source.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/proxy/layouts.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/streams_loader.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/streams_notifier.dart';
import 'package:fastocloud_pro_panel/events/server_events.dart';
import 'package:fastocloud_pro_panel/notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

const _PROXY_KEY = 'External';
const _PROXY_VODS_KEY = 'External VODs/Series';
const _STREAMS_KEY = 'Streams';
const _VODS_KEY = 'VODs/Series';
const _CODS_KEY = 'CODs';
const _CATCHUPS_KEY = 'Catchups';
const _EVENTS_KEY = 'Events';
const _TESTS_KEY = 'Test';
const _TIMESHIFTS_KEY = 'Timeshifts';
const _CV_KEY = 'Computer Vision';
const _CHANGER_KEY = 'Changer';

String _streamKeyType(StreamType type) {
  if (type == StreamType.PROXY) {
    return _PROXY_KEY;
  } else if (type == StreamType.VOD_PROXY) {
    return _PROXY_VODS_KEY;
  } else if (type == StreamType.VOD_ENCODE || type == StreamType.VOD_RELAY) {
    return _VODS_KEY;
  } else if (type == StreamType.COD_ENCODE || type == StreamType.COD_RELAY) {
    return _CODS_KEY;
  } else if (type == StreamType.CATCHUP) {
    return _CATCHUPS_KEY;
  } else if (type == StreamType.EVENT) {
    return _EVENTS_KEY;
  } else if (type == StreamType.TEST_LIFE) {
    return _TESTS_KEY;
  } else if (type == StreamType.TIMESHIFT_PLAYER || type == StreamType.TIMESHIFT_RECORDER) {
    return _TIMESHIFTS_KEY;
  } else if (type == StreamType.CV_DATA) {
    return _CV_KEY;
  } else if (type == StreamType.CHANGER_RELAY) {
    return _CHANGER_KEY;
  } else if (type == StreamType.CHANGER_ENCODE) {
    return _CHANGER_KEY;
  }
  return _STREAMS_KEY;
}

class ServerStreamsTabs extends StatefulWidget {
  final Stream<JsonRpcEvent> rpc;
  final StreamsNotifier notify;

  const ServerStreamsTabs(this.rpc, this.notify);

  @override
  _ServerStreamsTabsState createState() {
    return _ServerStreamsTabsState();
  }
}

class _ServerStreamsTabsState extends State<ServerStreamsTabs>
    with IListener<IStream>, IStatusListener {
  late StreamsLoader _loader;
  late ServerLoaderInfo _serverInfo;

  final Map<String, IStreamTableSource<IStream>> streams = {
    _PROXY_KEY: ProxyStreamSource(streams: []),
    _PROXY_VODS_KEY: ProxyStreamSource(streams: []),
    _STREAMS_KEY: HardwareStreamSource(streams: []),
    _VODS_KEY: HardwareStreamSource(streams: []),
    _CODS_KEY: HardwareStreamSource(streams: []),
    _CATCHUPS_KEY: HardwareStreamSource(streams: []),
    _EVENTS_KEY: HardwareStreamSource(streams: []),
    _TESTS_KEY: HardwareStreamSource(streams: []),
    _TIMESHIFTS_KEY: HardwareStreamSource(streams: []),
    _CV_KEY: HardwareStreamSource(streams: []),
    _CHANGER_KEY: HardwareStreamSource(streams: [])
  };

  @override
  void initState() {
    super.initState();
    final String sid = ServerLoaderInfo.of(context)!.loader.sid;
    _loader = StreamsLoader(sid);
    _serverInfo = ServerLoaderInfo.of(context)!;
    _serverInfo.status.addListener(this);
    widget.notify.addListener(this);
    _loader.load();
  }

  @override
  void dispose() {
    _serverInfo.status.removeListener(this);
    widget.notify.removeListener(this);
    _loader.dispose();
    streams.forEach((key, value) {
      value.dispose();
    });
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamsLoaderInfo(
        loader: _loader,
        child: LoaderWidget<StreamsDataState>(
            loader: _loader,
            builder: (context, state) {
              streams.forEach((key, value) {
                value.clearItems();
              });
              for (final IStream proxy in state.data.proxies) {
                onAdd(proxy);
              }
              for (final IStream stream in state.data.streams) {
                onAdd(stream);
              }
              for (final IStream cat in state.data.catchups) {
                onAdd(cat);
              }
              for (final IStream vod in state.data.vods) {
                onAdd(vod);
              }
              for (final IStream cod in state.data.cods) {
                onAdd(cod);
              }
              for (final IStream event in state.data.events) {
                onAdd(event);
              }
              for (final IStream test in state.data.tests) {
                onAdd(test);
              }
              const UpdateScrollBar().dispatch(context);
              return _Tabs(streams, _loader, widget.rpc);
            }));
  }

  @override
  void onAdd(IStream stream) {
    final _type = stream.type();
    final _typeName = _streamKeyType(_type);
    streams[_typeName]!.addItem(stream);
  }

  @override
  void onUpdate(IStream stream) {
    final StreamType _type = stream.type();
    final _typeName = _streamKeyType(_type);
    streams[_typeName]!.updateItem(stream);
  }

  @override
  void onRemove(IStream stream) {
    final StreamType _type = stream.type();
    final _typeName = _streamKeyType(_type);
    streams[_typeName]!.removeItem(stream);
  }

  @override
  void onStatusChanged(ServerStatus status) {
    _loader.load();
  }
}

class _Tabs extends StatefulWidget {
  final Stream<JsonRpcEvent> rpc;
  final Map<String, DataTableSource> streams;
  final StreamsLoader loader;

  const _Tabs(this.streams, this.loader, this.rpc);

  @override
  _TabsState createState() {
    return _TabsState();
  }
}

class _TabsState extends State<_Tabs> {
  String _currentType = _PROXY_KEY;

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_title(), _currentTable(widget.loader, widget.rpc)]);
  }

  Widget _currentTable(StreamsLoader loader, Stream<JsonRpcEvent> rpc) {
    final currentStreams = widget.streams[_currentType];
    if (_currentType == _PROXY_KEY) {
      return ProxyStreamsListLayout(loader, rpc, currentStreams as ProxyStreamSource, (s) {
        return _streamsButtons(s, loader, rpc);
      });
    } else if (_currentType == _PROXY_VODS_KEY) {
      return ProxyStreamsListLayout(loader, rpc, currentStreams as ProxyStreamSource, (s) {
        return _streamsButtons(s, loader, rpc);
      });
    } else if (_currentType == _VODS_KEY) {
      return VodsHardwareStreamsListLayout(loader, rpc, currentStreams as HardwareStreamSource,
          (s) {
        return _streamsButtonsVod(s, loader, rpc);
      });
    } else if (_currentType == _CHANGER_KEY) {
      return ChangerStreamsListLayout(loader, rpc, currentStreams as HardwareStreamSource, (s) {
        return _streamsButtons(s, loader, rpc);
      });
    }
    return HardwareStreamsListLayout(loader, rpc, currentStreams as HardwareStreamSource, (s) {
      return _streamsButtons(s, loader, rpc);
    });
  }

  Widget _title() {
    return SizedBox(
        width: 220,
        child: DropdownButtonEx<String>(
            value: _currentType,
            values: const <String>[
              _PROXY_KEY,
              _PROXY_VODS_KEY,
              _STREAMS_KEY,
              _VODS_KEY,
              _CODS_KEY,
              _CATCHUPS_KEY,
              _EVENTS_KEY,
              _TESTS_KEY,
              _TIMESHIFTS_KEY,
              _CV_KEY,
              _CHANGER_KEY
            ],
            onChanged: (String? newValue) {
              setState(() {
                _currentType = newValue!;
              });
            },
            itemBuilder: (String value) {
              return DropdownMenuItem<String>(value: value, child: Text(value));
            }));
  }

  Widget _scanFolderVodsButton(Server server, Stream<JsonRpcEvent> rpc) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: ScanFolderVodsButton(server, rpc));
  }

  Widget _addStreamButton(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: AddStreamButton(server, loader, rpc));
  }

  List<Widget> _streamsButtons(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc) {
    return <Widget>[_addStreamButton(server, loader, rpc)];
  }

  List<Widget> _streamsButtonsVod(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc) {
    return <Widget>[_scanFolderVodsButton(server, rpc), _addStreamButton(server, loader, rpc)];
  }
}
