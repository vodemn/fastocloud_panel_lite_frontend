import 'dart:async';
import 'dart:convert';

import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/loader.dart';
import 'package:http/http.dart';

class Streams {
  List<IStream> proxies = [];
  List<IStream> streams = [];
  List<IStream> catchups = [];
  List<IStream> vods = [];
  List<IStream> cods = [];
  List<IStream> events = [];
  List<IStream> tests = [];
}

class StreamsLoader extends ItemBloc<Streams> {
  final String sid;

  StreamsLoader(this.sid);

  @override
  Future<StreamsDataState> loadDataRoute() {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchGet('/server/streams/$sid');
    return response.then((value) {
      final Streams result = Streams();
      final data = json.decode(value.body);
      data['proxies'].forEach((s) {
        final stream = makeStream(s)!;
        result.proxies.add(stream);
      });
      data['streams'].forEach((s) {
        final stream = makeStream(s)!;
        result.streams.add(stream);
      });

      data['catchups'].forEach((s) {
        final stream = makeStream(s)!;
        result.catchups.add(stream);
      });
      data['vods'].forEach((s) {
        final stream = makeStream(s)!;
        result.vods.add(stream);
      });
      data['cods'].forEach((s) {
        final stream = makeStream(s)!;
        result.cods.add(stream);
      });
      data['events'].forEach((s) {
        final stream = makeStream(s)!;
        result.events.add(stream);
      });
      data['tests'].forEach((s) {
        final stream = makeStream(s)!;
        result.tests.add(stream);
      });

      return StreamsDataState(result);
    }, onError: (error) {
      throw error;
    });
  }

  Future<bool> play(String streamId) {
    final fetcher = locator<Fetcher>();
    final url = fetcher.launchUrl('/server/stream/play/$sid/$streamId/master.m3u');
    return url;
  }

  Future<Response> add(IStream stream) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchPost('/server/stream/add/$sid', stream.toJson(), [201]);
    return response;
  }

  Future<Response> edit(IStream stream) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchPatch('/server/stream/edit/$sid', stream.toJson(), [201]);
    return response;
  }

  Future<Response> remove(String streamId) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchPost('/server/stream/remove/$sid', {'id': streamId}, [201]);
    return response;
  }

  Future<Response> getLog(String streamId) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchGet('/server/stream/get_log/$sid/$streamId', [201]);
    return response;
  }

  Future<bool> viewPipeline(String streamId) {
    final fetcher = locator<Fetcher>();
    final url = fetcher.launchUrl('/server/stream/view_pipeline/$sid/$streamId');
    return url;
  }

  Future<Response> getPipeline(String streamId) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchGet('/server/stream/get_pipeline/$sid/$streamId', [201]);
    return response;
  }

  Future<bool> viewLog(String streamId) {
    final fetcher = locator<Fetcher>();
    final url = fetcher.launchUrl('/server/stream/view_log/$sid/$streamId');
    return url;
  }

  // hardware
  Future<Response> start(String streamId) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchGet('/server/stream/start/$sid/$streamId', [200, 201]);
    return response;
  }

  Future<Response> stop(String streamId) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchGet('/server/stream/stop/$sid/$streamId', [200, 201]);
    return response;
  }

  Future<Response> kill(String streamId) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchGet('/server/stream/kill/$sid/$streamId', [200, 201]);
    return response;
  }

  Future<Response> restart(String streamId) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchGet('/server/stream/restart/$sid/$streamId', [200, 201]);
    return response;
  }

  Future<Response> changeSource(String streamId, int index) {
    final fetcher = locator<Fetcher>();
    final response = fetcher
        .fetchPost('/server/stream/change_source/$sid/$streamId', {'index': index}, [200, 201]);
    return response;
  }
}

class StreamsDataState extends ItemDataState<Streams> {
  StreamsDataState(Streams data) : super(data);
}

class StreamsLoaderInfo extends InheritedWidget {
  const StreamsLoaderInfo({required this.loader, required Widget child}) : super(child: child);

  final StreamsLoader loader;

  static StreamsLoader of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<StreamsLoaderInfo>()!.loader;
  }

  @override
  bool updateShouldNotify(StreamsLoaderInfo old) => true;
}
