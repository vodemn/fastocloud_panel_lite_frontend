import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/streams/actions.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/istream/data_source.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/make_dialog.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/streams_data_table.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/streams_loader.dart';
import 'package:fastocloud_pro_panel/events/server_events.dart';
import 'package:fastocloud_pro_panel/output_player/dialog.dart';
import 'package:fastocloud_pro_panel/profile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

abstract class StreamsListLayout<S extends IStream> extends StatefulWidget {
  Stream<JsonRpcEvent> get rpc;

  StreamsLoader get loader;

  IStreamTableSource<S> get dataSource;

  List<Widget> Function(Server server) get actions;

  @override
  StreamsListLayoutState createState() {
    return StreamsListLayoutState();
  }
}

class StreamsListLayoutState<T extends StreamsListLayout> extends State<T> {
  @override
  Widget build(BuildContext context) {
    return StreamsDataLayout(
        dataSource: widget.dataSource,
        headerActions: widget.actions,
        singleItemActions: singleStreamActions,
        multipleItemActions: multipleStreamActions);
  }

  List<Widget> singleStreamActions(IStream s, Server server) {
    final _role = server.getProviderRoleById(ProfileInfo.of(context)!.profile!.id!);
    return [
      playOutputButton(s),
      playButton(s, server.id!),
      editButton(s, server, _role),
      copyButton(s, server, _role),
      removeButton([s], server.id!, _role),
    ];
  }

  List<Widget> multipleStreamActions(List<IStream> streams, Server server) {
    final _role = server.getProviderRoleById(ProfileInfo.of(context)!.profile!.id!);
    return [removeButton(streams, server.id!, _role)];
  }

  Widget playOutputButton(IStream stream) {
    return StreamActionIcon.playOutput(() => playOutput(stream));
  }

  Widget playButton(IStream stream, String sid) {
    return StreamActionIcon.play(() => play(stream, sid));
  }

  Widget editButton(IStream stream, Server server, ProviderRole role) {
    if (role == ProviderRole.READ) {
      return const SizedBox();
    }
    return CommonActionIcon.edit(() => edit(stream, server));
  }

  Widget copyButton(IStream stream, Server server, ProviderRole role) {
    if (role == ProviderRole.READ) {
      return const SizedBox();
    }
    return CommonActionIcon.copy(() => copy(stream, server));
  }

  Widget removeButton(List<IStream> streams, String sid, ProviderRole role) {
    if (role == ProviderRole.READ) {
      return const SizedBox();
    }
    return CommonActionIcon.remove(() => remove(streams));
  }

  void play(IStream stream, String sid) {
    StreamsLoaderInfo.of(context).play(stream.id!).then((_) {}, onError: (error) {
      showError(context, error);
    });
  }

  void playOutput(IStream stream) {
    showDialog(
        context: context,
        builder: (context) {
          return OutputPlayerDialog(stream.name, stream.type(), stream.output);
        });
  }

  void edit(IStream stream, Server server) {
    showDialog(
        context: context,
        builder: (context) {
          return makeEditStreamDialog(server, widget.loader, widget.rpc, stream.type(), stream);
        });
  }

  void copy(IStream stream, Server server) {
    final copy = stream.copyWith(id: null, name: 'Copy ${stream.name}');
    edit(copy, server);
  }

  void remove(List<IStream> streams) {
    for (final stream in streams) {
      StreamsLoaderInfo.of(context).remove(stream.id!).then((_) {}, onError: (error) {
        showError(context, error);
      });
    }
  }

  void getLog(IStream stream) {
    StreamsLoaderInfo.of(context).getLog(stream.id!).then((_) {}, onError: (error) {
      showError(context, error);
    });
  }

  void viewLog(IStream stream) {
    StreamsLoaderInfo.of(context).viewLog(stream.id!).then((_) {}, onError: (error) {
      showError(context, error);
    });
  }

  void getPipeline(IStream stream) {
    StreamsLoaderInfo.of(context).getPipeline(stream.id!).then((_) {}, onError: (error) {
      showError(context, error);
    });
  }

  void viewPipeline(IStream stream) {
    StreamsLoaderInfo.of(context).viewPipeline(stream.id!).then((_) {}, onError: (error) {
      showError(context, error);
    });
  }
}
