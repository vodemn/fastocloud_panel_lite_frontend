import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

abstract class IStreamTableSource<S extends IStream> extends DataSource<S> {
  IStreamTableSource({required List<DataEntry<S>> streams}) : super(items: streams);

  @override
  String get itemsName {
    return 'streams';
  }

  @override
  bool searchCondition(String text, IStream stream) {
    return stream.name.toLowerCase().contains(text.toLowerCase());
  }

  @override
  Widget get noItems {
    return _NoStreamsAvailable();
  }

  @override
  bool equalItemsCondition(IStream item, IStream listItem) {
    return listItem.id == item.id;
  }
}

class _NoStreamsAvailable extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Center(
        child: NonAvailableBuffer(icon: Icons.error, message: 'No streams available'));
  }
}
