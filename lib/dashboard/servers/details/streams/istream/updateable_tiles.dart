import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/name_icon.dart';
import 'package:fastocloud_pro_panel/common/streams/stream_icon.dart';
import 'package:flutter/material.dart';

class IStreamUpdatable {
  static Widget nameAndIcon(IStream init) {
    return NameAndIcon(name: Text(init.name), icon: StreamIcon(init.icon!, init.type()));
  }

  static Widget price(IStream init) {
    return Text('${init.price}');
  }

  static Widget views(IStream init) {
    return Text('${init.views}');
  }
}
