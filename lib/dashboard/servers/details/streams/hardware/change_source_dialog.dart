import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/localization/translations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';

class ChangeSourceDialog extends StatefulWidget {
  final List<InputUrl> inputs;

  const ChangeSourceDialog(this.inputs);

  @override
  _ChangeSourceDialogState createState() {
    return _ChangeSourceDialogState();
  }
}

class _ChangeSourceDialogState extends State<ChangeSourceDialog> {
  int _index = 0;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: const Text('Change source'),
        contentPadding: const EdgeInsets.all(8),
        content: SingleChildScrollView(
            child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                    mainAxisSize: MainAxisSize.min, children: <Widget>[_sourceIndexField()]))),
        actions: <Widget>[
          FlatButtonEx.notFilled(text: TR_CANCEL, onPressed: _cancel),
          FlatButtonEx.filled(text: 'Change', onPressed: _save)
        ]);
  }

  Widget _sourceIndexField() {
    return DropdownButtonEx<int>(
        hint: 'Change source',
        value: _index,
        values: List<int>.generate(widget.inputs.length, (index) => index),
        onChanged: (t) {
          setState(() {
            _index = t!;
          });
        },
        itemBuilder: (index) {
          final InputUrl input = widget.inputs[index];
          return DropdownMenuItem(child: Text(input.uri), value: index);
        });
  }

  void _cancel() => Navigator.of(context).pop();

  void _save() {
    Navigator.of(context).pop(_index);
  }
}
