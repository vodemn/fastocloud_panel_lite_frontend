import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/streams/actions.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/hardware/change_source_dialog.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/hardware/data_source.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/istream/layouts.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/streams_loader.dart';
import 'package:fastocloud_pro_panel/events/server_events.dart';
import 'package:fastocloud_pro_panel/profile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/errors.dart';

class HardwareStreamsListLayout extends StreamsListLayout<HardwareStream> {
  @override
  final Stream<JsonRpcEvent> rpc;

  @override
  final StreamsLoader loader;

  final HardwareStreamSource source;

  @override
  final List<Widget> Function(Server server) actions;

  HardwareStreamsListLayout(this.loader, this.rpc, this.source, this.actions);

  @override
  HardwareStreamSource get dataSource {
    return source;
  }

  @override
  HardwareStreamsListLayoutState createState() {
    return HardwareStreamsListLayoutState();
  }
}

class ChangerStreamsListLayout extends HardwareStreamsListLayout {
  ChangerStreamsListLayout(StreamsLoader loader, Stream<JsonRpcEvent> rpc,
      HardwareStreamSource source, List<Widget> Function(Server server) actions)
      : super(loader, rpc, source, actions);

  @override
  ChangerStreamsListLayoutState createState() {
    return ChangerStreamsListLayoutState();
  }
}

class VodsHardwareStreamsListLayout extends HardwareStreamsListLayout {
  VodsHardwareStreamsListLayout(StreamsLoader loader, Stream<JsonRpcEvent> rpc,
      HardwareStreamSource source, List<Widget> Function(Server server) actions)
      : super(loader, rpc, source, actions);

  @override
  VodsHardwareStreamsListLayoutState createState() {
    return VodsHardwareStreamsListLayoutState();
  }
}

class HardwareStreamsListLayoutState<T extends HardwareStreamsListLayout>
    extends StreamsListLayoutState<T> {
  ProviderRole role(Server server) {
    return server.getProviderRoleById(ProfileInfo.of(context)!.profile!.id!);
  }

  @override
  List<Widget> singleStreamActions(IStream s, Server server) {
    final ProviderRole _role = role(server);
    final String sid = server.id!;
    return server.isActive()
        ? [
            startButton([s], sid),
            stopButton([s], sid),
            killButton([s], sid),
            restartButton([s], sid),
            playOutputButton(s),
            playButton(s, sid),
            editButton(s, server, _role),
            copyButton(s, server, _role),
            removeButton([s], sid, _role),
            getLogButton(s, sid, _role),
            viewLogButton(s, sid, _role),
            getPipelineButton(s, sid, _role),
            viewPipelineButton(s, sid, _role)
          ]
        : [
            playOutputButton(s),
            playButton(s, server.id!),
            editButton(s, server, _role),
            copyButton(s, server, _role),
            removeButton([s], server.id!, _role),
            viewLogButton(s, sid, _role),
            viewPipelineButton(s, sid, _role)
          ];
  }

  @override
  List<Widget> multipleStreamActions(List<IStream> s, Server server) {
    final ProviderRole _role = server.getProviderRoleById(ProfileInfo.of(context)!.profile!.id!);
    final String sid = server.id!;
    return server.isActive()
        ? [
            startButton(s, sid),
            stopButton(s, sid),
            killButton(s, sid),
            restartButton(s, sid),
            removeButton(s, server.id!, _role)
          ]
        : [removeButton(s, server.id!, _role)];
  }

  Widget startButton(List<IStream> s, String sid) {
    return StreamActionIcon.start(() => _start(s));
  }

  Widget stopButton(List<IStream> s, String sid) {
    return StreamActionIcon.stop(() => _stop(s));
  }

  Widget killButton(List<IStream> s, String sid) {
    return StreamActionIcon.kill(() => _kill(s));
  }

  Widget restartButton(List<IStream> s, String sid) {
    return StreamActionIcon.restart(() => _restart(s));
  }

  Widget viewLogButton(IStream s, String sid, ProviderRole role) {
    if (role == ProviderRole.READ || role == ProviderRole.WRITE) {
      return const SizedBox();
    }
    return StreamActionIcon.viewLog(() => viewLog(s));
  }

  Widget getLogButton(IStream s, String sid, ProviderRole role) {
    if (role == ProviderRole.READ || role == ProviderRole.WRITE) {
      return const SizedBox();
    }
    return StreamActionIcon.getLog(() => getLog(s));
  }

  Widget viewPipelineButton(IStream s, String sid, ProviderRole role) {
    if (role == ProviderRole.READ || role == ProviderRole.WRITE) {
      return const SizedBox();
    }
    return StreamActionIcon.viewPipeline(() => viewPipeline(s));
  }

  Widget getPipelineButton(IStream s, String sid, ProviderRole role) {
    if (role == ProviderRole.READ || role == ProviderRole.WRITE) {
      return const SizedBox();
    }
    return StreamActionIcon.getPipeline(() => getPipeline(s));
  }

  void _start(List<IStream> streams) {
    for (final stream in streams) {
      StreamsLoaderInfo.of(context).start(stream.id!).then((_) {}, onError: (error) {
        showError(context, error);
      });
    }
  }

  void _stop(List<IStream> streams) {
    for (final stream in streams) {
      StreamsLoaderInfo.of(context).stop(stream.id!).then((_) {}, onError: (error) {
        showError(context, error);
      });
    }
  }

  void _kill(List<IStream> streams) {
    for (final stream in streams) {
      StreamsLoaderInfo.of(context).kill(stream.id!).then((_) {}, onError: (error) {
        showError(context, error);
      });
    }
  }

  void _restart(List<IStream> streams) {
    for (final stream in streams) {
      StreamsLoaderInfo.of(context).restart(stream.id!).then((_) {}, onError: (error) {
        showError(context, error);
      });
    }
  }
}

class ChangerStreamsListLayoutState
    extends HardwareStreamsListLayoutState<ChangerStreamsListLayout> {
  @override
  List<Widget> singleStreamActions(IStream s, Server server) {
    final ProviderRole _role = role(server);
    final String sid = server.id!;
    return server.isActive()
        ? [
            startButton([s], sid),
            stopButton([s], sid),
            killButton([s], sid),
            restartButton([s], sid),
            changeSourceButton(s),
            playOutputButton(s),
            playButton(s, sid),
            editButton(s, server, _role),
            copyButton(s, server, _role),
            removeButton([s], sid, _role),
            getLogButton(s, sid, _role),
            viewLogButton(s, sid, _role),
            getPipelineButton(s, sid, _role),
            viewPipelineButton(s, sid, _role)
          ]
        : [
            playOutputButton(s),
            playButton(s, server.id!),
            editButton(s, server, _role),
            copyButton(s, server, _role),
            removeButton([s], server.id!, _role),
            viewLogButton(s, sid, _role),
            viewPipelineButton(s, sid, _role)
          ];
  }

  Widget changeSourceButton(IStream s) {
    return StreamActionIcon.changeStream(() => _changeSource(s));
  }

  void _changeSource(IStream stream) {
    final hard = stream as HardwareStream;
    final result = showDialog(
        context: context,
        builder: (context) {
          return ChangeSourceDialog(hard.input);
        });
    result.then((value) {
      if (value == null) {
        return;
      }

      StreamsLoaderInfo.of(context).changeSource(stream.id!, value).then((_) {}, onError: (error) {
        showError(context, error);
      });
    });
  }
}

class VodsHardwareStreamsListLayoutState<T extends VodsHardwareStreamsListLayout>
    extends HardwareStreamsListLayoutState<T> {
  Widget refreshButton(List<IStream> s, String sid) {
    return StreamActionIcon.refresh(() => _start(s));
  }

  @override
  List<Widget> singleStreamActions(IStream s, Server server) {
    final ProviderRole _role = role(server);
    final String sid = server.id!;
    return server.isActive()
        ? [
            refreshButton([s], sid),
            stopButton([s], sid),
            killButton([s], sid),
            playOutputButton(s),
            playButton(s, sid),
            editButton(s, server, _role),
            copyButton(s, server, _role),
            removeButton([s], sid, _role),
            getLogButton(s, sid, _role),
            viewLogButton(s, sid, _role),
            getPipelineButton(s, sid, _role),
            viewPipelineButton(s, sid, _role)
          ]
        : [
            playOutputButton(s),
            playButton(s, sid),
            editButton(s, server, _role),
            copyButton(s, server, _role),
            removeButton([s], sid, _role),
            viewLogButton(s, sid, _role),
            viewPipelineButton(s, sid, _role)
          ];
  }

  @override
  List<Widget> multipleStreamActions(List<IStream> s, Server server) {
    final ProviderRole _role = role(server);
    final String sid = server.id!;
    return server.isActive()
        ? [
            refreshButton(s, sid),
            stopButton(s, sid),
            killButton(s, sid),
            removeButton(s, server.id!, _role)
          ]
        : [removeButton(s, server.id!, _role)];
  }
}
