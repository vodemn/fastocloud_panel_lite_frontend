import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/istream/data_source.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/istream/updateable_tiles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/data_table.dart';

class HardwareStreamSource extends IStreamTableSource<HardwareStream> {
  HardwareStreamSource({required List<DataEntry<HardwareStream>> streams})
      : super(streams: streams);

  @override
  List<Widget> headers() {
    return const [
      Text('Name'),
      Text('Type'),
      Text('Status'),
      Text('Restarts'),
      Text('CPU (%)'),
      Text('RSS (Mb)'),
      Text('Net In (Mbps)'),
      Text('Net Out (Mbps)'),
      Text('Start time'),
      Text('Loop time'),
      Text('Quality (%)'),
      Text('Price (\$)'),
      Text('Views')
    ];
  }

  @override
  List<Widget> tiles(HardwareStream s) {
    return [
      IStreamUpdatable.nameAndIcon(s),
      Text(s.type().toHumanReadable()),
      Text(s.stats!.status.toHumanReadable()),
      Text('${s.stats!.restarts}'),
      Text('${s.fixedCpu()}'),
      Text('${s.rssInMegabytes()}'),
      Text('${s.fixedInputMBitsPerSecond()}'),
      Text('${s.fixedOutputMBitsPerSecond()}'),
      Text('${s.fixedStartTime()}'),
      Text('${s.fixedLoopTime()}'),
      Text('${s.fixedQuality()}'),
      IStreamUpdatable.price(s),
      IStreamUpdatable.views(s)
    ];
  }
}
