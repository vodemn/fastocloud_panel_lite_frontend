import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/istream/data_source.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/istream/layouts.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/istream/updateable_tiles.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/streams_loader.dart';
import 'package:fastocloud_pro_panel/events/server_events.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/data_table.dart';

class ProxyStreamSource extends IStreamTableSource<ProxyStream> {
  ProxyStreamSource({required List<DataEntry<ProxyStream>> streams}) : super(streams: streams);

  @override
  List<Widget> headers() {
    return const [Text('Name'), Text('Type'), Text('Price (\$)'), Text('Views')];
  }

  @override
  List<Widget> tiles(ProxyStream s) {
    return [
      IStreamUpdatable.nameAndIcon(s),
      Text(s.type().toHumanReadable()),
      IStreamUpdatable.price(s),
      IStreamUpdatable.views(s)
    ];
  }
}

class ProxyStreamsListLayout extends StreamsListLayout {
  @override
  final Stream<JsonRpcEvent> rpc;

  @override
  final StreamsLoader loader;

  final ProxyStreamSource source;

  @override
  final List<Widget> Function(Server server) actions;

  ProxyStreamsListLayout(this.loader, this.rpc, this.source, this.actions);

  @override
  ProxyStreamSource get dataSource {
    return source;
  }
}
