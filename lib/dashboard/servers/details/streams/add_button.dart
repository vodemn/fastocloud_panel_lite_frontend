import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/add_edit_dialog.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/make_dialog.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/streams_loader.dart';
import 'package:fastocloud_pro_panel/events/server_events.dart';
import 'package:fastocloud_pro_panel/localization/translations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:responsive_builder/responsive_builder.dart';

class AddStreamButton extends StatelessWidget {
  final Server server;
  final Stream<JsonRpcEvent> rpc;
  final StreamsLoader loader;

  const AddStreamButton(this.server, this.loader, this.rpc);

  @override
  Widget build(BuildContext context) {
    return FlatButtonEx.filled(
        text: 'Add',
        onPressed: () {
          _onTap(context);
        });
  }

  void _onTap(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return const StreamTypeDialog();
        }).then((_type) {
      if (_type == null) {
        return;
      }

      showDialog(
          context: context,
          builder: (context) {
            return makeAddStreamDialog(server, loader, rpc, _type);
          });
    });
  }
}

class StreamTypeDialog extends StatefulWidget {
  const StreamTypeDialog();

  @override
  _StreamTypeDialogState createState() {
    return _StreamTypeDialogState();
  }
}

class _StreamTypeDialogState extends State<StreamTypeDialog> {
  StreamType _type = StreamType.values[0];

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInformation) {
      return AlertDialog(
          title: const Text('Stream type'),
          contentPadding: const EdgeInsets.symmetric(vertical: 8),
          content: SizedBox(
              width: AddEditDialog.getNarrow(sizingInformation.isMobile),
              child: SingleChildScrollView(
                  child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: List<Widget>.generate(StreamType.values.length, (index) {
                        final current = StreamType.values[index];
                        return RadioListTile<StreamType>(
                            title: Text(current.toHumanReadable()),
                            value: current,
                            groupValue: _type,
                            onChanged: (value) {
                              setState(() {
                                _type = value!;
                              });
                            });
                      })))),
          actions: <Widget>[
            FlatButtonEx.notFilled(text: TR_CANCEL, onPressed: () => Navigator.of(context).pop()),
            FlatButtonEx.filled(text: 'Submit', onPressed: () => Navigator.of(context).pop(_type))
          ]);
    });
  }
}
