import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/hardware_page.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/proxy_page.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/streams_loader.dart';
import 'package:fastocloud_pro_panel/events/server_events.dart';
import 'package:flutter/material.dart';

Widget makeAddStreamDialog(
    Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, StreamType type) {
  switch (type) {
    case StreamType.PROXY:
      return ProxyStreamPage.add(server, loader, rpc);
    case StreamType.VOD_PROXY:
      return ProxyVodStreamPage.add(server, loader, rpc);
    case StreamType.RELAY:
      return RelayStreamPage.add(server, loader, rpc);
    case StreamType.ENCODE:
      return EncodeStreamPage.add(server, loader, rpc);
    case StreamType.TIMESHIFT_PLAYER:
      return TimeshiftRecorderStreamPage.add(server, loader, rpc);
    case StreamType.TIMESHIFT_RECORDER:
      return TimeshiftRecorderStreamPage.add(server, loader, rpc);
    case StreamType.CATCHUP:
      return CatchupStreamPage.add(server, loader, rpc);
    case StreamType.TEST_LIFE:
      return TestLifeStreamPage.add(server, loader, rpc);
    case StreamType.VOD_RELAY:
      return VodRelayStreamPage.add(server, loader, rpc);
    case StreamType.VOD_ENCODE:
      return VodEncodeStreamPage.add(server, loader, rpc);
    case StreamType.COD_RELAY:
      return CodRelayStreamPage.add(server, loader, rpc);
    case StreamType.COD_ENCODE:
      return CodEncodeStreamPage.add(server, loader, rpc);
    case StreamType.EVENT:
      return EventStreamPage.add(server, loader, rpc);
    case StreamType.CV_DATA:
      return CvDataStreamPage.add(server, loader, rpc);
    case StreamType.CHANGER_RELAY:
      return ChangerRelayStreamPage.add(server, loader, rpc);
    case StreamType.CHANGER_ENCODE:
      return ChangerEncoderStreamPage.add(server, loader, rpc);
    default:
      throw 'Inimplemented type';
  }
}

Widget makeEditStreamDialog(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc,
    StreamType type, IStream stream) {
  switch (type) {
    case StreamType.PROXY:
      return ProxyStreamPage.edit(server, loader, rpc, stream as ProxyStream);
    case StreamType.VOD_PROXY:
      return ProxyVodStreamPage.edit(server, loader, rpc, stream as ProxyStream);
    case StreamType.RELAY:
      return RelayStreamPage.edit(server, loader, rpc, stream as RelayStream);
    case StreamType.ENCODE:
      return EncodeStreamPage.edit(server, loader, rpc, stream as EncodeStream);
    case StreamType.TIMESHIFT_PLAYER:
      return TimeshiftRecorderStreamPage.edit(
          server, loader, rpc, stream as TimeshiftRecorderStream);
    case StreamType.TIMESHIFT_RECORDER:
      return TimeshiftRecorderStreamPage.edit(
          server, loader, rpc, stream as TimeshiftRecorderStream);
    case StreamType.CATCHUP:
      return CatchupStreamPage.edit(server, loader, rpc, stream as CatchupStream);
    case StreamType.TEST_LIFE:
      return TestLifeStreamPage.edit(server, loader, rpc, stream as TestLifeStream);
    case StreamType.VOD_RELAY:
      return VodRelayStreamPage.edit(server, loader, rpc, stream as VodRelayStream);
    case StreamType.VOD_ENCODE:
      return VodEncodeStreamPage.edit(server, loader, rpc, stream as VodEncodeStream);
    case StreamType.COD_RELAY:
      return CodRelayStreamPage.edit(server, loader, rpc, stream as CodRelayStream);
    case StreamType.COD_ENCODE:
      return CodEncodeStreamPage.edit(server, loader, rpc, stream as CodEncodeStream);
    case StreamType.EVENT:
      return EventStreamPage.edit(server, loader, rpc, stream as EventStream);
    case StreamType.CV_DATA:
      return CvDataStreamPage.edit(server, loader, rpc, stream as CvDataStream);
    case StreamType.CHANGER_RELAY:
      return ChangerRelayStreamPage.edit(server, loader, rpc, stream as ChangerRelayStream);
    case StreamType.CHANGER_ENCODE:
      return ChangerEncoderStreamPage.edit(server, loader, rpc, stream as ChangerEncodeStream);
    default:
      throw 'Inimplemented type';
  }
}
