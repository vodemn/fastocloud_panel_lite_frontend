import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/server_loader.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

class StreamsDataLayout<T> extends StatelessWidget {
  final DataSource<T> dataSource;
  final List<Widget> Function(Server server) headerActions;
  final List<Widget> Function(T, Server) singleItemActions;
  final List<Widget> Function(List<T>, Server) multipleItemActions;

  const StreamsDataLayout(
      {required this.dataSource,
      required this.headerActions,
      required this.singleItemActions,
      required this.multipleItemActions});

  @override
  Widget build(BuildContext context) {
    return DataTableEx.customActions(
        dataSource, _Header<T>(dataSource, headerActions), () => _actions(context));
  }

  Widget _actions(BuildContext context) {
    return StreamBuilder<ItemState>(
        initialData: ServerLoaderInfo.of(context)!.loader.init(),
        stream: ServerLoaderInfo.of(context)!.loader.serverActiveStatus()
            as Stream<ItemState<dynamic>>?,
        builder: (context, snapshot) {
          if (snapshot.data is ServerDataState) {
            final ServerDataState state = snapshot.data as ServerDataState;
            return _actionsRow(state.data);
          }
          return _actionsRow(null);
        });
  }

  Widget _actionsRow(Server? server) {
    return Row(
        mainAxisSize: MainAxisSize.min, children: server == null ? [] : _actionsBuilder(server));
  }

  List<Widget> _actionsBuilder(Server server) {
    if (dataSource.selectedRowCount == 1) {
      return singleItemActions(dataSource.selectedItems().first, server);
    } else if (dataSource.selectedRowCount > 1) {
      return multipleItemActions(dataSource.selectedItems(), server);
    }
    return [];
  }
}

class _Header<T> extends StatelessWidget {
  final DataSource<T> source;
  final List<Widget> Function(Server server) actions;

  const _Header(this.source, this.actions);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<ItemState>(
        initialData: ServerLoaderInfo.of(context)!.loader.init(),
        stream: ServerLoaderInfo.of(context)!.loader.serverActiveStatus()
            as Stream<ItemState<dynamic>>?,
        builder: (context, snapshot) {
          if (snapshot.data is ServerDataState) {
            final ServerDataState state = snapshot.data as ServerDataState;
            return DataTableSearchHeader(source: source, actions: actions(state.data));
          }
          return DataTableSearchHeader(source: source);
        });
  }
}
