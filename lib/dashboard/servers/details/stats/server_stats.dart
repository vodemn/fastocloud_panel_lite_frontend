import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/server_status_notifier.dart';
import 'package:fastocloud_pro_panel/common/servers/server_stats.dart';
import 'package:fastocloud_pro_panel/common/servers/stats_header.dart';
import 'package:fastocloud_pro_panel/common/servers/stats_layout.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/server_loader.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/stats/select_m3u_dialog.dart';
import 'package:fastocloud_pro_panel/profile.dart';
import 'package:fastocloud_pro_panel/routing/route_config.dart';
import 'package:fastocloud_pro_panel/routing/router_delegate.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

class ServerStatsWidget extends StatefulWidget {
  const ServerStatsWidget();

  @override
  _ServerStatsWidgetState createState() {
    return _ServerStatsWidgetState();
  }
}

class _ServerStatsWidgetState extends State<ServerStatsWidget> with IStatusListener {
  late ServerLoaderInfo _info;

  @override
  void initState() {
    super.initState();
    _info = ServerLoaderInfo.of(context)!;
    _info.status.addListener(this);
  }

  @override
  void dispose() {
    _info.status.removeListener(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final stats = ServerLoaderInfo.of(context)!.loader;
    return StreamBuilder<ItemState>(
        stream: stats.stream(),
        initialData: stats.init(),
        builder: (context, snapshot) {
          if (snapshot.data is ServerDataState) {
            final ServerDataState state = snapshot.data as ServerDataState;
            const UpdateScrollBar().dispatch(context);
            return _statsWidget(state.data);
          } else if (snapshot.data is ItemErrorState) {
            final ItemErrorState? state = snapshot.data as ItemErrorState<dynamic>?;
            WidgetsBinding.instance!
                .addPostFrameCallback((_) => showError(context, state!.error as ErrorEx));
          }
          return const Center(child: CircularProgressIndicator());
        });
  }

  Widget _statsWidget(Server server) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[_Buttons(server, server.status), _StatsTiles(server)]);
  }

  @override
  void onStatusChanged(ServerStatus status) {
    _info.loader.load();
  }
}

class _Buttons extends StatsHeader {
  final Server server;

  const _Buttons(this.server, ServerStatus? status) : super(status);

  String? get sid => server.id;

  @override
  Widget build(BuildContext context) {
    return StatsButtons('Server stats', _buttonsRow(context));
  }

  List<Widget> _buttonsRow(BuildContext context) {
    List<Widget> _buttons = [];
    final _role = server.getProviderRoleById(ProfileInfo.of(context)!.profile!.id!);
    if (_role == ProviderRole.READ) {
      _buttons = [_playListButton(context)];
    } else if (_role == ProviderRole.WRITE) {
      _buttons = [_playListButton(context), _uploadM3uFilesButton(context)];
    } else if (_role == ProviderRole.SUPPORT || _role == ProviderRole.ADMIN) {
      _buttons = [
        connectionButton(context),
        activateButton(context),
        _uploadM3uFilesButton(context),
        _playListButton(context),
        getLogButton(context),
        viewLogButton(context)
      ];
    }
    _buttons += [historyButton(context)];
    return _buttons;
  }

  // buttons
  Widget _uploadM3uFilesButton(BuildContext context) {
    return FlatButtonEx.filled(text: 'Upload m3u', onPressed: () => _uploadM3uFiles(context));
  }

  Widget _playListButton(BuildContext context) {
    return FlatButtonEx.filled(text: 'Playlist', onPressed: () => _playList(context));
  }

  @override
  void connect(BuildContext context) {
    ServerLoaderInfo.of(context)!.loader.connect().then((_) {}, onError: (error) {
      showError(context, error);
    });
  }

  @override
  void disconnect(BuildContext context) {
    ServerLoaderInfo.of(context)!.loader.disconnect().then((_) {}, onError: (error) {
      showError(context, error);
    });
  }

  @override
  void activate(BuildContext context, String key) {
    ServerLoaderInfo.of(context)!.loader.activate(key).then((_) {}, onError: (error) {
      showError(context, error);
    });
  }

  void _uploadM3uFiles(BuildContext context) {
    final loader = ServerLoaderInfo.of(context)!.loader.uploadM3uFiles;
    showDialog(
        context: context,
        builder: (context) {
          return SelectM3uDialog(loader);
        });
  }

  void _playList(BuildContext context) {
    ServerLoaderInfo.of(context)!.loader.playList().then((_) {}, onError: (error) {
      showError(context, error);
    });
  }

  @override
  void getLog(BuildContext context) {
    ServerLoaderInfo.of(context)!.loader.getLog().then((_) {}, onError: (error) {
      showError(context, error);
    });
  }

  @override
  void viewLog(BuildContext context) {
    ServerLoaderInfo.of(context)!.loader.viewLog().then((_) {}, onError: (error) {
      showError(context, error);
    });
  }

  @override
  void toHistory(BuildContext context) {
    RouterDelegateEx.of(context).setNewRoutePath(HistoryRouteConfig.servers(sid!));
  }
}

class _StatsTiles extends StatsLayout {
  final Server stats;

  const _StatsTiles(this.stats);

  @override
  ServerStatus? get status => stats.status;

  @override
  int? get syncTime => stats.syncTime;

  @override
  int get timestamp => stats.timestamp;

  @override
  String? get project => stats.project;

  @override
  String? get version => stats.version;

  @override
  double get cpu => stats.cpu;

  @override
  double get gpu => stats.gpu;

  @override
  int get memoryTotal => stats.memoryTotal;

  @override
  int get memoryFree => stats.memoryFree;

  @override
  int get hddTotal => stats.hddTotal;

  @override
  int get hddFree => stats.hddFree;

  @override
  int get bandwidthIn => stats.bandwidthIn;

  @override
  int get bandwidthOut => stats.bandwidthOut;

  @override
  int get uptime => stats.uptime;

  @override
  int? get expirationTime => stats.expirationTime;

  @override
  OperationSystem? get os => stats.os;

  @override
  Widget onlineUsers({double? height}) {
    final online = stats.onlineUsers;
    return OnlineUsersServerStatsTile.server(
        online?.daemon, online?.cods, online?.vods, online?.http,
        height: height);
  }
}
