import 'dart:convert';
import 'dart:developer';

import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:http/http.dart';
import 'package:universal_html/html.dart' as html;

class SelectM3uDialog extends StatefulWidget {
  final Future<StreamedResponse> Function(Map<String, List<int>> data, Map<String, dynamic> fields)
      uploadFiles;

  const SelectM3uDialog(this.uploadFiles);

  @override
  _SelectM3uDialogState createState() {
    return _SelectM3uDialogState();
  }
}

class _SelectM3uDialogState extends State<SelectM3uDialog> {
  Map<String, List<int>> _filesData = {};
  StreamType _type = StreamType.PROXY;
  final List<String> _tags = [];
  final GlobalKey<TagsState> _tagStateKey = GlobalKey<TagsState>();
  bool _parsingStreams = false;
  double? _imageCheckTime = 0.05;
  bool _skipGroups = false;

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
        title: const Text('M3U Upload'),
        children: _parsingStreams
            ? [
                const Center(
                    child:
                        Padding(padding: EdgeInsets.all(8.0), child: CircularProgressIndicator()))
              ]
            : _fields());
  }

  List<Widget> _fields() {
    return <Widget>[
      _typeField(),
      _tagsField(),
      _skipFileGroups(),
      Padding(padding: const EdgeInsets.symmetric(horizontal: 8.0), child: _timeImageCheckFiled()),
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: FlatButtonEx.filled(text: 'Select a file', onPressed: _startWebFilePicker)),
      const SizedBox(height: 8),
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: FlatButtonEx.filled(
              text: 'Upload', onPressed: _filesData.isEmpty ? null : _makeRequest))
    ];
  }

  // private:
  void _startWebFilePicker() async {
    _filesData = {};
    if (kIsWeb) {
      final html.FileUploadInputElement uploadInput = html.FileUploadInputElement();
      uploadInput.multiple = true;
      uploadInput.draggable = true;
      uploadInput.click();

      uploadInput.addEventListener('change', (e) {
        for (int i = 0; i < uploadInput.files!.length; ++i) {
          final file = uploadInput.files![i];
          final reader = html.FileReader();
          reader.onLoadEnd.listen((e) {
            final base64 = reader.result.toString().split(",").last;
            final contents = const Base64Decoder().convert(base64);
            _filesData[file.name] = contents;
            if (mounted) {
              setState(() {});
            }
          });
          reader.readAsDataUrl(file);
        }
        if (mounted) {
          setState(() {});
        }
      });
    } else {
      try {
        final FilePickerResult? result = await FilePicker.platform.pickFiles();
        if (result != null) {
          return;
        }

        for (int i = 0; i < result!.files.length; ++i) {
          final PlatformFile file = result.files[i];
          _filesData[file.name!] = file.bytes!;
        }
        if (mounted) {
          setState(() {});
        }
      } on PlatformException catch (e) {
        log("Unsupported operation: $e");
      }
    }
  }

  Widget _typeField() {
    return DropdownButtonEx<StreamType>(
        hint: _type.toHumanReadable(),
        value: _type,
        values: const <StreamType>[
          StreamType.PROXY,
          StreamType.VOD_PROXY,
          StreamType.RELAY,
          StreamType.ENCODE,
          //StreamType.CATCHUP,
          StreamType.TEST_LIFE,
          StreamType.VOD_RELAY,
          StreamType.VOD_ENCODE,
          StreamType.COD_RELAY,
          StreamType.COD_ENCODE,
          StreamType.EVENT
        ],
        onChanged: (c) => setState(() {
              _type = c!;
            }),
        itemBuilder: (StreamType value) {
          return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
        });
  }

  Widget _timeImageCheckFiled() {
    return NumberTextField.decimal(
        hintText: 'Image check time(sec)',
        initDouble: _imageCheckTime,
        minDouble: 0.01,
        maxDouble: 1,
        onFieldChangedDouble: (term) {
          _imageCheckTime = term;
        });
  }

  Widget _skipFileGroups() {
    return StateCheckBox(
        title: 'Skip m3u groups',
        init: _skipGroups,
        onChanged: (value) {
          _skipGroups = value;
        });
  }

  Widget _tagsField() {
    return Tags(
        key: _tagStateKey,
        textField: TagsTextField(
            hintText: 'Add group',
            constraintSuggestion: false,
            suggestions: [],
            onSubmitted: (String str) {
              setState(() {
                _tags.add(str);
              });
            }),
        itemCount: _tags.length,
        itemBuilder: (int index) {
          final item = _tags[index];

          return ItemTags(
              key: Key(index.toString()),
              index: index,
              title: item,
              combine: ItemTagsCombine.withTextBefore,
              icon: ItemTagsIcon(icon: Icons.add),
              removeButton: ItemTagsRemoveButton(onRemoved: () {
                setState(() {
                  _tags.remove(item);
                });
                return true;
              }));
        });
  }

  void _makeRequest() {
    if (mounted) {
      setState(() {
        _parsingStreams = true;
      });
    }

    final fetcher = locator<Fetcher>();
    final defaults = fetcher.defaults();
    final fields = {
      'type': _type.toInt(),
      'stream_logo_url': defaults.streamLogoIcon,
      'trailer_url': defaults.vodTrailerUrl,
      'image_time_to_check': _imageCheckTime,
      'skip_groups': _skipGroups,
      'groups': _tags
    };
    widget.uploadFiles(_filesData, fields).then((value) {
      Navigator.of(context).pop();
    }, onError: (error) {
      if (mounted) {
        setState(() {
          _parsingStreams = false;
        });
      }
      showError(context, error);
    });
  }
}
