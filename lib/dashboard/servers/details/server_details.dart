import 'dart:async';

import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/server_status_notifier.dart';
import 'package:fastocloud_pro_panel/common/servers/server_details.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/server_loader.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/stats/server_stats.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/server_streams.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/streams_notifier.dart';
import 'package:fastocloud_pro_panel/events/server_events.dart';
import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:socket_io_client/socket_io_client.dart' as io;

class ServerDetailsPage extends StatefulWidget {
  final String sid;

  const ServerDetailsPage(this.sid);

  @override
  _ServerDetailsPageState createState() {
    return _ServerDetailsPageState();
  }
}

class _ServerDetailsPageState extends State<ServerDetailsPage> {
  late io.Socket _socketIO;
  late ServerLoader _loader;
  final StreamsNotifier _streamsNotify = StreamsNotifier();
  final BehaviorSubject<JsonRpcEvent> _rpcNotify = BehaviorSubject<JsonRpcEvent>();
  final StatusNotifier _statusNotify = StatusNotifier();

  Stream<JsonRpcEvent> rpc() {
    return _rpcNotify.stream;
  }

  @override
  void initState() {
    super.initState();
    _loader = ServerLoader(widget.sid);
    _loader.load();
    _initSocket();
  }

  @override
  void dispose() {
    _loader.dispose();
    _socketIO.disconnect();
    _socketIO.destroy();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ServerLoaderInfo(
        loader: _loader,
        status: _statusNotify,
        child: ServerDetailsBuilder(
            const ServerStatsWidget(), ServerStreamsTabs(_rpcNotify, _streamsNotify), 'Streams'));
  }

  void _initSocket() {
    final fetcher = locator<Fetcher>();
    const serverUpdateChannel = 'service_data_changed';
    const status = 'service_status_changed';
    const streamAddChannel = 'stream_added';
    const streamUpdateChannel = 'stream_updated';
    const streamRemoveChannel = 'stream_removed';
    const serverRPC = 'rpc';
    final route = '/service_${widget.sid}';
    _socketIO = fetcher.createSocketIO(nsp: route);
    _socketIO.on('connect', (data) {});
    _socketIO.on('disconnect', (data) {});
    _socketIO.on('connect_error', (data) {});
    _socketIO.on(serverUpdateChannel, (json) {
      final Server _server = Server.fromJson(json);
      _loader.publishState(ServerDataState(_server));
    });
    _socketIO.on(status, (json) {
      final ServerStatus newStatus = ServerStatus.fromInt(json['status']);
      _statusNotify.updateStatus(newStatus);
    });
    _socketIO.on(streamAddChannel, (json) {
      final IStream stream = makeStream(json)!;
      _streamsNotify.addItem(stream);
    });
    _socketIO.on(streamUpdateChannel, (json) {
      final IStream stream = makeStream(json)!;
      _streamsNotify.updateItem(stream);
    });
    _socketIO.on(streamRemoveChannel, (json) {
      final IStream stream = makeStream(json)!;
      _streamsNotify.removeItem(stream);
    });
    _socketIO.on(serverRPC, (json) {
      _rpcNotify.add(JsonRpcEvent(json));
    });
  }
}
