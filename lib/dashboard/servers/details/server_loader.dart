import 'dart:async';
import 'dart:convert';

import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/server_status_notifier.dart';
import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/loader.dart';
import 'package:http/http.dart';

class ServerLoader extends ItemBloc<Server> {
  final String sid;

  bool? _isActive;

  ServerLoader(this.sid);

  @override
  Future<ServerDataState> loadDataRoute() {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchGet('/server/stats/$sid');
    return response.then((value) {
      final data = json.decode(value.body);
      final stats = Server.fromJson(data['server']);
      return ServerDataState(stats);
    }, onError: (error) {
      throw error;
    });
  }

  Stream serverActiveStatus() {
    return stream().where((event) {
      if (event is ServerDataState) {
        if (event.data.isActive() != _isActive) {
          return true;
        }
      }
      return false;
    }).asBroadcastStream();
  }

  Future<Response> connect() {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchPatch('/server/connect/$sid', {});
    return response;
  }

  Future<Response> disconnect() {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchPatch('/server/disconnect/$sid', {});
    return response;
  }

  Future<Response> activate(String key) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchPatch('/server/activate/$sid', {'license': key}, [201]);
    return response;
  }

  Future<StreamedResponse> uploadM3uFiles(
      Map<String, List<int>> data, Map<String, dynamic> fields) {
    final fetcher = locator<Fetcher>();
    final resp = fetcher.sendFiles('/server/upload_files/$sid', data, fields);
    return resp;
  }

  Future<bool> playList() {
    final fetcher = locator<Fetcher>();
    final url = fetcher.launchUrl('/server/playlist/$sid/master.m3u');
    return url;
  }

  Future<Response> getLog() {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchGet('/server/get_log/$sid', [201]);
    return response;
  }

  Future<bool> viewLog() {
    final fetcher = locator<Fetcher>();
    final url = fetcher.launchUrl('/server/view_log/$sid');
    return url;
  }
}

class ServerDataState extends ItemDataState<Server> {
  ServerDataState(Server data) : super(data);
}

class ServerLoaderInfo extends InheritedWidget {
  const ServerLoaderInfo({required this.loader, required this.status, required Widget child})
      : super(child: child);

  final ServerLoader loader;
  final StatusNotifier status;

  static ServerLoaderInfo? of(BuildContext context) {
    return context.findAncestorWidgetOfExactType<ServerLoaderInfo>();
  }

  @override
  bool updateShouldNotify(ServerLoaderInfo old) => true;
}
