import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/add_edit_dialog.dart';
import 'package:fastocloud_pro_panel/common/license_key_fields.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/servers_loader.dart';
import 'package:fastocloud_pro_panel/models/widgets/base.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:http/http.dart';

class ServerDialog extends StatefulWidget {
  final Server init;
  final ServersLoader loader;

  ServerDialog.add(this.loader) : init = Server.createDefault();

  ServerDialog.edit(this.loader, Server server) : init = server.copy();

  @override
  _ServerDialogState createState() {
    return _ServerDialogState();
  }
}

class _ServerDialogState extends State<ServerDialog> {
  late Server _server;

  bool isAdd() {
    return _server.id == null;
  }

  @override
  void initState() {
    super.initState();
    _server = widget.init;
  }

  @override
  Widget build(BuildContext context) {
    return AddEditDialog.wide(
        add: isAdd(),
        children: <Widget>[
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                _nameField(),
                _header('Host'),
                Row(children: <Widget>[
                  Expanded(child: _hostField()),
                  Expanded(child: _httpHostField())
                ]),
                _header('VODs'),
                Row(children: <Widget>[
                  Expanded(child: _vodsHostField()),
                  Expanded(child: _vodsDirectoryField())
                ]),
                _header('CODs'),
                Row(children: <Widget>[
                  Expanded(child: _codsHostField()),
                  Expanded(child: _codsDirectoryField())
                ]),
                _header('Nginx'),
                Row(children: <Widget>[Expanded(child: _nginxHostField())]),
                _header('Rtmp'),
                Row(children: <Widget>[Expanded(child: _rtmpHostField())]),
                _header('Other'),
                Row(children: <Widget>[
                  Expanded(child: _feedbackDirectory()),
                  Expanded(child: _timeshiftDirectory())
                ]),
                Row(children: <Widget>[
                  Expanded(child: _hlsDirectory()),
                  Expanded(child: _proxyDirectory())
                ]),
                Row(children: <Widget>[Expanded(child: _dataDirectory())]),
                Row(children: <Widget>[
                  Expanded(child: _descriptionField()),
                  Expanded(child: _pricePackage())
                ]),
                Row(children: <Widget>[Expanded(child: _monitoring())]),
                Row(children: <Widget>[
                  Expanded(child: _autoStartField()),
                  Expanded(child: _activationKeyField())
                ])
              ])
        ],
        onSave: _save);
  }

  Widget _header(String text) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Text(text, style: const TextStyle(fontWeight: FontWeight.bold)));
  }

  Widget _hostField() {
    return _hostAndPortField('Host', _server.host);
  }

  Widget _httpHostField() {
    return _hostAndPortField('Http host', _server.httpHost);
  }

  Widget _vodsHostField() {
    return _hostAndPortField('VODs host', _server.vodsHost);
  }

  Widget _vodsDirectoryField() {
    return _directoryField('VODs directory', _server.vodsDirectory);
  }

  Widget _codsHostField() {
    return _hostAndPortField('CODs host', _server.codsHost);
  }

  Widget _codsDirectoryField() {
    return _directoryField('CODs directory', _server.codsDirectory);
  }

  Widget _nginxHostField() {
    return _hostAndPortField('Nginx host', _server.nginxHost);
  }

  Widget _rtmpHostField() {
    return _hostAndPortField('Rtmp host', _server.rtmpHost);
  }

  Widget _feedbackDirectory() {
    return _directoryField('Feedback directory', _server.feedbackDirectory);
  }

  Widget _timeshiftDirectory() {
    return _directoryField('Timeshift directory', _server.timeshiftsDirectory);
  }

  Widget _hlsDirectory() {
    return _directoryField('HLS directory', _server.hlsDirectory);
  }

  Widget _proxyDirectory() {
    return _directoryField('External directory', _server.proxyDirectory);
  }

  Widget _dataDirectory() {
    return _directoryField('Data directory', _server.dataDirectory);
  }

  Widget _nameField() {
    return TextFieldEx(
        hintText: 'Name',
        errorText: 'Enter name',
        minSymbols: Server.MIN_NAME_LENGTH,
        maxSymbols: Server.MAX_NAME_LENGTH,
        init: _server.name,
        onFieldChanged: (term) {
          _server.name = term;
        });
  }

  Widget _monitoring() {
    return StateCheckBox(
        title: 'Monitoring',
        init: _server.monitoring,
        onChanged: (value) {
          _server.monitoring = value;
        });
  }

  Widget _pricePackage() {
    return NumberTextField.decimal(
        minDouble: Price.MIN,
        maxDouble: Price.MAX,
        hintText: 'Price package(\$)',
        canBeEmpty: false,
        initDouble: _server.pricePackage,
        onFieldChangedDouble: (term) {
          if (term != null) _server.pricePackage = term;
        });
  }

  Widget _activationKeyField() {
    return LicenseKeyField(
        init: _server.activationKey,
        onFieldChanged: (term, isValid) {
          if (isValid) {
            _server.activationKey = term;
          }
        });
  }

  Widget _descriptionField() {
    return TextFieldEx(
        hintText: 'Package description',
        init: _server.description,
        onFieldChanged: (term) {
          _server.description = term;
        });
  }

  Widget _autoStartField() {
    return StateCheckBox(
        title: 'Auto Start',
        init: _server.autoStart,
        onChanged: (value) {
          _server.autoStart = value;
        });
  }

  // field builders
  Widget _hostAndPortField(String hint, HostAndPort init) {
    return HostAndPortTextField(hostHint: hint, hostError: 'Enter host URL', value: init);
  }

  Widget _directoryField(String hint, String init) {
    return TextFieldEx(
        hintText: hint,
        errorText: 'Enter directory',
        init: init,
        onFieldChanged: (term) {
          init = term;
        });
  }

  void _save() {
    if (!_server.isValid()) {
      return;
    }

    final Future<Response> _response =
        isAdd() ? widget.loader.addServer(_server) : widget.loader.editServer(_server);
    _response.then((response) {
      Navigator.of(context).pop();
    }, onError: (error) {
      showError(context, error);
    });
  }
}
