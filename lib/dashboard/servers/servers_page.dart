import 'dart:async';

import 'package:fastocloud_pro_panel/dashboard/servers/data_table/data_source.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/data_table/layouts.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/servers_loader.dart';
import 'package:fastocloud_pro_panel/event_bus.dart';
import 'package:fastocloud_pro_panel/events/server_events.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/loader.dart';

class ServersPage extends StatefulWidget {
  const ServersPage({Key? key}) : super(key: key);

  @override
  _ServersPageState createState() {
    return _ServersPageState();
  }
}

class _ServersPageState extends State<ServersPage> {
  final ServerSource _servers = ServerSource(items: []);
  final ServersLoader _loader = ServersLoader();
  late StreamSubscription<ServerAddEvent> _add;
  late StreamSubscription<ServerEditEvent> _edit;
  late StreamSubscription<ServerRemoveEvent> _remove;

  @override
  void initState() {
    super.initState();
    _loader.load();
    _initEvents();
  }

  @override
  void dispose() {
    _disposeEvents();
    _loader.dispose();
    _servers.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LoaderWidget<ServersDataState>(
        loader: _loader,
        builder: (context, state) {
          _servers.clearItems();
          _servers.addItems(state.data);
          return ServersListLayout(_servers, _loader);
        });
  }

  void _initEvents() {
    final eventBus = locator<FastoEventBus>();
    _add = eventBus.subscribe<ServerAddEvent>().listen((event) {
      _servers.addItem(event.server);
    });
    _edit = eventBus.subscribe<ServerEditEvent>().listen((event) {
      _servers.updateItem(event.server);
    });
    _remove = eventBus.subscribe<ServerRemoveEvent>().listen((event) {
      _servers.removeItem(event.server);
    });
  }

  void _disposeEvents() {
    _add.cancel();
    _edit.cancel();
    _remove.cancel();
  }
}
