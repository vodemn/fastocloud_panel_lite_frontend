import 'dart:async';
import 'dart:convert';

import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter_common/loader.dart';
import 'package:http/http.dart';

class ServersLoader extends ItemBloc<List<Server>> {
  @override
  Future<ServersDataState> loadDataRoute() {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchGet('/provider/servers');
    return response.then((value) {
      final List<Server> result = [];
      final data = json.decode(value.body);
      data['servers'].forEach((s) {
        final res = Server.fromJson(s);
        result.add(res);
      });
      return ServersDataState(result);
    }, onError: (error) {
      throw error;
    });
  }

  Future<Response> removeServerByID(String sid) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchDelete('/server/remove/$sid', [201]);
    return response;
  }

  Future<Response> addServer(Server server) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchPost('/server/add', server.toJson(), [201]);
    return response;
  }

  Future<Response> editServer(Server server) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchPatch('/server/edit/${server.id}', server.toJson(), [201]);
    return response;
  }
}

class ServersDataState extends ItemDataState<List<Server>> {
  ServersDataState(List<Server> data) : super(data);
}
