abstract class IListener<T> {
  void onAdd(T item);

  void onUpdate(T item);

  void onRemove(T item);
}

abstract class INotifier {
  void addListener(IListener listener);

  void removeListener(IListener listener);
}

class Notifier<T> extends INotifier {
  final List<IListener> _observers = [];

  @override
  void addListener(IListener listener) {
    _observers.add(listener);
  }

  @override
  void removeListener(IListener listener) {
    _observers.remove(listener);
  }

  void addItem(T item) {
    if (item == null) {
      return;
    }

    _onAddItem(item);
  }

  void updateItem(T item) {
    if (item == null) {
      return;
    }

    _onUpdateItem(item);
  }

  void removeItem(T item) {
    if (item == null) {
      return;
    }

    _onRemoveItem(item);
  }

  // private:
  void _onAddItem(T item) {
    for (final IListener observer in _observers) {
      observer.onAdd(item);
    }
  }

  void _onUpdateItem(T item) {
    for (final IListener observer in _observers) {
      observer.onUpdate(item);
    }
  }

  void _onRemoveItem(T item) {
    for (final IListener observer in _observers) {
      observer.onRemove(item);
    }
  }
}
