import "package:universal_html/html.dart";

class LocalStorageService {
  static LocalStorageService? _instance;
  Storage? _storage;

  static Future<LocalStorageService> getInstance() async {
    _instance ??= LocalStorageService();
    _instance!._storage ??= window.localStorage;
    return _instance!;
  }

  static const String _checkField = 'check';
  static const String _emailField = 'email';
  static const String _passwordField = 'password';
  static const String _langCodeKey = 'lang_code';
  static const String _countryCodeKey = 'country_code';

  bool check() {
    return _storage![_checkField] == true.toString();
  }

  void setCheck(bool? value) {
    if (value == null) {
      _storage!.remove(_checkField);
    } else {
      _storage![_checkField] = value.toString();
    }
  }

  String? email() {
    return _storage![_emailField];
  }

  void setEmail(String? value) {
    if (value == null) {
      _storage!.remove(_emailField);
    } else {
      _storage![_emailField] = value;
    }
  }

  String? password() {
    return _storage![_passwordField];
  }

  void setPassword(String? value) {
    if (value == null) {
      _storage!.remove(_passwordField);
    } else {
      _storage![_passwordField] = value;
    }
  }

  String? langCode() {
    return _storage![_langCodeKey];
  }

  void setLangCode(String code) {
    _storage![_langCodeKey] = code;
  }

  String? countryCode() {
    return _storage![_countryCodeKey];
  }

  void setCountryCode(String code) {
    _storage![_countryCodeKey] = code;
  }
}
