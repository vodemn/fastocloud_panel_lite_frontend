import 'package:fastocloud_pro_panel/config.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:http/http.dart';
import 'package:socket_io_client/socket_io_client.dart' as io;

class Defaults {
  static const String INVALID_TRAILER_URL =
      'https://fastocloud.com/static/video/invalid_trailer.m3u8';
  final String streamLogoIcon;
  final String vodTrailerUrl;

  Defaults(this.streamLogoIcon, this.vodTrailerUrl);
}

class Fetcher extends IFetcher {
  static const _API_VERSION = '/panel_media/api';
  static const _SOCKET_IO_PATH = '/panel_media/socket.io';
  static const _LOG_SOCKET_IO_PATH = '/panel_media/logsocket.io';

  static Fetcher? _instance;
  final String _frontendServerUrl = FRONTEND_SERVER_ENDPOINT;
  final String _backendServerUrl = BACKEND_SERVER_ENDPOINT;

  Fetcher._();

  Fetcher.getInstance() {
    _instance ??= Fetcher._();
  }

  @override
  Uri getBackendEndpoint(String path) {
    return _generateBackEndEndpoint(path);
  }

  io.Socket createSocketIO({String nsp = '/'}) {
    return io.io(_backendServerUrl + nsp, <String, dynamic>{
      'path': _SOCKET_IO_PATH,
      'transports': ['websocket'],
      'forceNew': true
    });
  }

  io.Socket createLogSocketIO() {
    return io.io(_backendServerUrl, <String, dynamic>{
      'path': _LOG_SOCKET_IO_PATH,
      'transports': ['websocket'],
      'forceNew': true
    });
  }

  Defaults defaults() {
    return Defaults(
        _generateFrontEndEndpoint('/images/unknown_channel.png'), Defaults.INVALID_TRAILER_URL);
  }

  Future<Response> getInfo() {
    final response = fetchGet('/info', [200]);
    return response;
  }

  // private:
  String _generateFrontEndEndpoint(String path) {
    return '$_frontendServerUrl$path';
  }

  Uri _generateBackEndEndpoint(String path) {
    final String base = '$_backendServerUrl$_API_VERSION';
    return Uri.parse('$base$path');
  }
}
