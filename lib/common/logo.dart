import 'package:fastocloud_pro_panel/constants.dart';
import 'package:fastocloud_pro_panel/routing/router_delegate.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';

class LogoButton extends StatelessWidget {
  final double size;
  final String path;

  const LogoButton({this.size = 96, this.path = LOGO_PATH});

  @override
  Widget build(BuildContext context) {
    return InkWell(
        highlightColor: Colors.transparent,
        hoverColor: Colors.transparent,
        splashColor: Colors.transparent,
        onTap: () {
          RouterDelegateEx.of(context).toHome();
        },
        child: CustomAssetLogo(path, height: size, width: size));
  }
}
