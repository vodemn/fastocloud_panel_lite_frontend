import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/widgets.dart';

class LicenseKeyField extends StatefulWidget {
  static const KEY_LENGTH = ActivationKey.KEY_LENGTH;

  final String? init;
  final void Function(String term, bool isValid)? onFieldChanged;

  const LicenseKeyField({this.init, this.onFieldChanged});

  @override
  _LicenseKeyFieldState createState() => _LicenseKeyFieldState();
}

class _LicenseKeyFieldState extends State<LicenseKeyField> {
  late TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController(text: widget.init);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextFieldEx(
        controller: _controller,
        formatters: <TextInputFormatter>[FilteringTextInputFormatter.allow(RegExp("[a-f0-9]"))],
        init: widget.init,
        maxSymbols: LicenseKeyField.KEY_LENGTH,
        hintText: 'License key',
        errorText: 'Enter license key',
        onFieldChanged: (String term) {
          widget.onFieldChanged?.call(term, term.length == LicenseKeyField.KEY_LENGTH);
        });
  }
}
