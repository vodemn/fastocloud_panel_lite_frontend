import 'dart:async';
import 'dart:convert';

import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter_common/loader.dart';

class HistoryLoader extends ItemBloc<List<Machine>> {
  final String path;

  HistoryLoader(this.path);

  @override
  Future<HistoryDataState> loadDataRoute() {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchGet(path);
    return response.then((value) {
      final List<Machine> result = [];
      final data = json.decode(value.body);
      data['history'].forEach((s) {
        final res = Machine.fromJson(s);
        result.add(res);
      });
      return HistoryDataState(result);
    }, onError: (error) {
      throw error;
    });
  }
}

class HistoryDataState extends ItemDataState<List<Machine>> {
  HistoryDataState(List<Machine> data) : super(data);
}
