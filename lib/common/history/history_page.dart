import 'dart:math';

import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:fastocloud_pro_panel/common/history/history_loader.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

class HistoryPage extends StatefulWidget {
  final String sid;
  final String path;

  const HistoryPage.server(this.sid) : path = '/server/history/$sid';

  const HistoryPage.loadBalancer(this.sid) : path = '/load_balance/history/$sid';

  const HistoryPage.epg(this.sid) : path = '/epg/history/$sid';

  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  late HistoryLoader _loader;

  String _currentType = CPU_GPU;

  final List<DateTime> _dates = [];

  final Map<String, List<num>> _data = {};

  static const CPU_FIELD = 'cpu';
  static const GPU_FIELD = 'gpu';
  static const HDD_USED = 'hdd_used';
  static const MEMORY_TOTAL_FIELD = 'memory_total';
  static const MEMORY_FREE_FIELD = 'memory_free';
  static const MEMORY_USED = 'memory_used';
  static const HDD_TOTAL_FIELD = 'hdd_total';
  static const HDD_FREE_FIELD = 'hdd_free';
  static const BANDWIDTH_IN_FIELD = 'bandwidth_in';
  static const BANDWIDTH_OUT_FIELD = 'bandwidth_out';

  static const CPU_GPU = 'CPU/GPU';
  static const MEMORY = 'Memory';
  static const HDD = 'HDD';
  static const BANDWIDTH = 'Network';

  double _maxHardware = 0;
  int _maxMemory = 0;
  int _maxHDD = 0;
  int _maxBandwidth = 0;

  bool _shouldUpdate = true;

  @override
  void initState() {
    super.initState();
    _loader = HistoryLoader(widget.path);
    _loader.load();
  }

  @override
  void dispose() {
    super.dispose();
    _loader.dispose();
  }

  void _updateStats(List<Machine> stats) {
    _data[CPU_FIELD] = <double>[];
    _data[GPU_FIELD] = <double>[];
    _data[MEMORY_TOTAL_FIELD] = <int>[];
    _data[MEMORY_FREE_FIELD] = <int>[];
    _data[MEMORY_USED] = <int>[];
    _data[HDD_TOTAL_FIELD] = <int>[];
    _data[HDD_FREE_FIELD] = <int>[];
    _data[HDD_USED] = <int>[];
    _data[BANDWIDTH_IN_FIELD] = <int>[];
    _data[BANDWIDTH_OUT_FIELD] = <int>[];

    if (stats.isNotEmpty) {
      stats.forEach((element) {
        _dates.add(DateTime.fromMillisecondsSinceEpoch(element.timestamp));

        _data[CPU_FIELD]!.add(element.cpu);
        _data[GPU_FIELD]!.add(element.gpu);
        final _maxCpuGpu = max<double>(element.cpu, element.gpu);
        _maxHardware = max<double>(_maxCpuGpu, _maxHardware);

        _data[MEMORY_TOTAL_FIELD]!.add(element.memoryTotal);
        _data[MEMORY_FREE_FIELD]!.add(element.memoryFree);
        _data[MEMORY_USED]!.add(element.memoryTotal - element.memoryFree);
        _maxMemory = max<int>(element.memoryTotal, _maxMemory);

        _data[HDD_TOTAL_FIELD]!.add(element.hddTotal);
        _data[HDD_FREE_FIELD]!.add(element.hddFree);
        _data[HDD_USED]!.add(element.hddTotal - element.hddFree);
        _maxHDD = max<int>(element.hddTotal, _maxHDD);

        _data[BANDWIDTH_IN_FIELD]!.add(element.bandwidthIn);
        _data[BANDWIDTH_OUT_FIELD]!.add(element.bandwidthOut);
        final _maxBandwidthIO = max<int>(element.bandwidthIn, element.bandwidthOut);
        _maxBandwidth = max<int>(_maxBandwidthIO, _maxBandwidth);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                    child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
                      Row(children: [
                        _typePicker(),
                        Text(_getUnit()!),
                        const Spacer(),
                        _refreshButton()
                      ]),
                      _plotBuilder()
                    ])))));
  }

  Widget _typePicker() {
    return DropdownButtonEx<String>(
        value: _currentType,
        values: const [CPU_GPU, MEMORY, HDD, BANDWIDTH],
        onChanged: (c) {
          _shouldUpdate = false;
          setState(() {
            _currentType = c!;
          });
        },
        itemBuilder: (String type) {
          return DropdownMenuItem(child: Text(type), value: type);
        });
  }

  Widget _refreshButton() {
    return FlatButtonEx.filled(text: 'Refresh', onPressed: _loader.load);
  }

  Widget _plotBuilder() {
    return LoaderWidget<HistoryDataState>(
        loader: _loader,
        builder: (context, state) {
          if (_shouldUpdate) {
            _updateStats(state.data);
          } else {
            _shouldUpdate = true;
          }
          return state.data.isEmpty ? _NoHistory() : _plot(state.data);
        });
  }

  Widget _plot(List<Machine> stats) {
    return Row(children: [
      Expanded(
          child: LineChart(
              LineChartData(
                  minY: 0,
                  maxY: _getMaxY(),
                  lineTouchData: LineTouchData(enabled: false),
                  lineBarsData: _getCurrentData(stats),
                  gridData: FlGridData(show: true, horizontalInterval: _getInterval()),
                  titlesData: FlTitlesData(
                      bottomTitles: SideTitles(
                          interval: _dates.length / 10,
                          showTitles: true,
                          getTitles: (value) => _formatTime(_dates[value.toInt()])),
                      leftTitles: _getLabelY())),
              swapAnimationDuration: Duration.zero)),
      const SizedBox(width: 16),
      Column(mainAxisSize: MainAxisSize.min, children: _getLegend())
    ]);
  }

  List<LineChartBarData> _getCurrentData(List<Machine> stats) {
    switch (_currentType) {
      case CPU_GPU:
        return [
          _getBarData(stats, CPU_FIELD, Colors.green),
          _getBarData(stats, GPU_FIELD, Colors.blue)
        ];
      case MEMORY:
        return [
          _getBarData(stats, MEMORY_FREE_FIELD, Colors.green),
          _getBarData(stats, MEMORY_USED, Colors.orange),
          _getBarData(stats, MEMORY_TOTAL_FIELD, Colors.blue)
        ];
      case HDD:
        return [
          _getBarData(stats, HDD_FREE_FIELD, Colors.green),
          _getBarData(stats, HDD_USED, Colors.orange),
          _getBarData(stats, HDD_TOTAL_FIELD, Colors.blue)
        ];
      case BANDWIDTH:
        return [
          _getBarData(stats, BANDWIDTH_IN_FIELD, Colors.green),
          _getBarData(stats, BANDWIDTH_OUT_FIELD, Colors.blue)
        ];
      default:
        return [];
    }
  }

  LineChartBarData _getBarData(List<Machine> stats, String key, Color color) {
    return LineChartBarData(
        spots: List<FlSpot>.generate(
            stats.length, (index) => FlSpot(index.toDouble(), _data[key]![index].toDouble())),
        isCurved: false,
        barWidth: 2,
        colors: [color],
        dotData: FlDotData(show: false));
  }

  List<Widget> _getLegend() {
    switch (_currentType) {
      case CPU_GPU:
        return const [_LegendTile('CPU', Colors.green), _LegendTile('GPU', Colors.blue)];
      case MEMORY:
        return const [
          _LegendTile('Free', Colors.green),
          _LegendTile('Used', Colors.orange),
          _LegendTile('Total', Colors.blue)
        ];
      case HDD:
        return const [
          _LegendTile('Free', Colors.green),
          _LegendTile('Used', Colors.orange),
          _LegendTile('Total', Colors.blue)
        ];
      case BANDWIDTH:
        return const [_LegendTile('In', Colors.green), _LegendTile('Out', Colors.blue)];
      default:
        return const [];
    }
  }

  SideTitles? _getLabelY() {
    switch (_currentType) {
      case CPU_GPU:
        return SideTitles(
            getTitles: (value) => '${fixedDouble(value)}%',
            showTitles: true,
            interval: _getInterval());
      case MEMORY:
        return SideTitles(
            getTitles: (value) => '${fixedDouble(value / (1024 * 1024 * 1024))}',
            showTitles: true,
            interval: _getInterval());
      case HDD:
        return SideTitles(
            getTitles: (value) => '${fixedDouble(value / (1024 * 1024 * 1024))}',
            showTitles: true,
            interval: _getInterval());
      case BANDWIDTH:
        return SideTitles(
            getTitles: (value) => '${fixedDouble(value / (1024 * 1024))}',
            showTitles: true,
            interval: _getInterval());
      default:
        return null;
    }
  }

  String? _getUnit() {
    switch (_currentType) {
      case CPU_GPU:
        return '%';
      case MEMORY:
        return 'Gb';
      case HDD:
        return 'Gb';
      case BANDWIDTH:
        return 'Mb/s';
      default:
        return null;
    }
  }

  double? _getInterval() {
    switch (_currentType) {
      case CPU_GPU:
        return 10.0;
      case MEMORY:
        return _maxMemory / 10;
      case HDD:
        return _maxHDD / 10;
      case BANDWIDTH:
        return _maxBandwidth / 10;
      default:
        return null;
    }
  }

  double? _getMaxY() {
    switch (_currentType) {
      case CPU_GPU:
        return 100.0;
      case MEMORY:
        return _maxMemory * 1.05;
      case HDD:
        return _maxHDD * 1.05;
      case BANDWIDTH:
        return _maxBandwidth * 1.05;
      default:
        return null;
    }
  }

  String _formatTime(DateTime time) {
    final _parts = time.toString().split(' ');
    return "${_parts.first}\n${_parts.last}";
  }
}

class _LegendTile extends StatelessWidget {
  final String title;
  final Color color;

  const _LegendTile(this.title, this.color);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: [
      Container(color: color, width: 16, height: 16),
      Padding(padding: const EdgeInsets.all(8.0), child: Text(title))
    ]);
  }
}

class _NoHistory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Center(child: NonAvailableBuffer(icon: Icons.data_usage, message: 'No data yet'));
  }
}
