import 'package:fastocloud_dart_models/models.dart';

abstract class IStatusListener {
  void onStatusChanged(ServerStatus status);
}

class StatusNotifier {
  final List<IStatusListener> _observers = [];

  void addListener(IStatusListener listener) {
    _observers.add(listener);
  }

  void removeListener(IStatusListener listener) {
    _observers.remove(listener);
  }

  void updateStatus(ServerStatus status) {
    _onStatusUpdate(status);
  }

  // private:
  void _onStatusUpdate(ServerStatus status) {
    for (final IStatusListener observer in _observers) {
      observer.onStatusChanged(status);
    }
  }
}
