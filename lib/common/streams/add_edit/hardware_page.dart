import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/base_page.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/istream_page.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/sections/audio.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/sections/input.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/sections/other.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/sections/video.dart';
import 'package:fastocloud_pro_panel/common/streams/vods/vod_bundle.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/streams_loader.dart';
import 'package:fastocloud_pro_panel/events/server_events.dart';
import 'package:flutter/material.dart';

abstract class HardwareStreamPage<S extends HardwareStream> extends IStreamPage<S> {
  HardwareStreamPage.add(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, StreamType type)
      : super.add(server, loader, rpc, type);

  HardwareStreamPage.edit(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, HardwareStream stream)
      : super.edit(server, loader, rpc, stream as S);
}

abstract class _HardwareStreamPageState<T extends HardwareStreamPage, S extends HardwareStream>
    extends IStreamPageState<T, S> {
  String? scanFolderOid;

  @override
  List<TabWidget> tabs() {
    return [
      TabWidget('Base', baseFields()),
      TabWidget('Meta urls', metaUrls()),
      TabWidget('Input urls', inputUrls()),
      TabWidget('Output urls', outputUrls()),
      TabWidget('Audio', audio()),
      TabWidget('Video', video()),
      TabWidget('Other', other())
    ];
  }

  Widget inputUrls() {
    return InputUrlSection<S>(stream, widget.server, probeUrl, widget.rpc);
  }

  Widget audio() {
    return AudioSection<S>(stream);
  }

  Widget video() {
    return VideoSection<S>(stream);
  }

  Widget other() {
    return OtherSection<S>(stream);
  }

  @override
  bool validate() {
    return stream.isValid();
  }
}

// relay
abstract class RelayStreamPageBase<S extends RelayStream> extends HardwareStreamPage<S> {
  RelayStreamPageBase.add(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, StreamType type)
      : super.add(server, loader, rpc, type);

  RelayStreamPageBase.edit(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, S stream)
      : super.edit(server, loader, rpc, stream);
}

abstract class _RelayStreamPageBaseState<T extends RelayStreamPageBase, S extends RelayStream>
    extends _HardwareStreamPageState<T, S> {
  @override
  Widget audio() => AudioSectionRelay<S>(stream);

  @override
  Widget video() => VideoSectionRelay<S>(stream);

  @override
  Widget other() => OtherSectionRelay<S>(stream);
}

class RelayStreamPage extends RelayStreamPageBase<RelayStream> {
  RelayStreamPage.add(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc)
      : super.add(server, loader, rpc, StreamType.RELAY);

  RelayStreamPage.edit(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, RelayStream stream)
      : super.edit(server, loader, rpc, stream);

  @override
  _RelayStreamPageState createState() => _RelayStreamPageState();
}

class _RelayStreamPageState extends _RelayStreamPageBaseState<RelayStreamPage, RelayStream> {}

abstract class EncodeStreamPageBase<S extends EncodeStream> extends HardwareStreamPage<S> {
  EncodeStreamPageBase.add(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, StreamType type)
      : super.add(server, loader, rpc, type);

  EncodeStreamPageBase.edit(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, EncodeStream stream)
      : super.edit(server, loader, rpc, stream);
}

abstract class _EncodeStreamPageBaseState<T extends EncodeStreamPageBase, S extends EncodeStream>
    extends _HardwareStreamPageState<T, S> {
  @override
  Widget audio() => AudioSectionEncode<S>(stream);

  @override
  Widget video() => VideoSectionEncode<S>(stream);

  @override
  Widget other() => OtherSectionEncode<S>(stream);
}

// encode
class EncodeStreamPage extends EncodeStreamPageBase<EncodeStream> {
  EncodeStreamPage.add(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc)
      : super.add(server, loader, rpc, StreamType.ENCODE);

  EncodeStreamPage.edit(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, EncodeStream stream)
      : super.edit(server, loader, rpc, stream);

  @override
  _EncodeStreamPageState createState() {
    return _EncodeStreamPageState();
  }
}

class _EncodeStreamPageState extends _EncodeStreamPageBaseState<EncodeStreamPage, EncodeStream> {}

// timeshift recorder
abstract class TimeshiftRecorderStreamPageBase<S extends TimeshiftRecorderStream>
    extends RelayStreamPageBase<S> {
  TimeshiftRecorderStreamPageBase.add(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, StreamType type)
      : super.add(server, loader, rpc, type);

  TimeshiftRecorderStreamPageBase.edit(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, S stream)
      : super.edit(server, loader, rpc, stream);
}

abstract class _TimeshiftRecorderStreamPageBaseState<T extends TimeshiftRecorderStreamPageBase,
    S extends TimeshiftRecorderStream> extends _RelayStreamPageBaseState<T, S> {}

class TimeshiftRecorderStreamPage extends TimeshiftRecorderStreamPageBase<TimeshiftRecorderStream> {
  TimeshiftRecorderStreamPage.add(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc)
      : super.add(server, loader, rpc, StreamType.TIMESHIFT_RECORDER);

  TimeshiftRecorderStreamPage.edit(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, TimeshiftRecorderStream stream)
      : super.edit(server, loader, rpc, stream);

  @override
  _TimeshiftRecorderStreamPageState createState() {
    return _TimeshiftRecorderStreamPageState();
  }
}

class _TimeshiftRecorderStreamPageState extends _TimeshiftRecorderStreamPageBaseState<
    TimeshiftRecorderStreamPage, TimeshiftRecorderStream> {
  @override
  List<TabWidget> tabs() {
    return [
      TabWidget('Base', baseFields()),
      TabWidget('Meta urls', metaUrls()),
      TabWidget('Input urls', inputUrls()),
      TabWidget('Audio', audio()),
      TabWidget('Video', video()),
      TabWidget('Other', other())
    ];
  }
}

// catchup
class CatchupStreamPage extends TimeshiftRecorderStreamPageBase<CatchupStream> {
  CatchupStreamPage.add(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc)
      : super.add(server, loader, rpc, StreamType.CATCHUP);

  CatchupStreamPage.edit(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, CatchupStream stream)
      : super.edit(server, loader, rpc, stream);

  @override
  _CatchupStreamPageState createState() {
    return _CatchupStreamPageState();
  }
}

class _CatchupStreamPageState
    extends _TimeshiftRecorderStreamPageBaseState<CatchupStreamPage, CatchupStream> {}

// timeshift player
class TimeshiftPlayerStreamPage extends RelayStreamPageBase<TimeshiftPlayerStream> {
  TimeshiftPlayerStreamPage.add(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc)
      : super.add(server, loader, rpc, StreamType.TIMESHIFT_PLAYER);

  TimeshiftPlayerStreamPage.edit(
      Server server, Stream<JsonRpcEvent> rpc, StreamsLoader loader, TimeshiftPlayerStream stream)
      : super.edit(server, loader, rpc, stream);

  @override
  _TimeshiftPlayerStreamPageState createState() {
    return _TimeshiftPlayerStreamPageState();
  }
}

class _TimeshiftPlayerStreamPageState
    extends _RelayStreamPageBaseState<TimeshiftPlayerStreamPage, TimeshiftPlayerStream> {}

// test
class TestLifeStreamPage extends RelayStreamPageBase<TestLifeStream> {
  TestLifeStreamPage.add(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc)
      : super.add(server, loader, rpc, StreamType.TEST_LIFE);

  TestLifeStreamPage.edit(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, TestLifeStream stream)
      : super.edit(server, loader, rpc, stream);

  @override
  _TestLifeStreamPageState createState() {
    return _TestLifeStreamPageState();
  }
}

class _TestLifeStreamPageState
    extends _RelayStreamPageBaseState<TestLifeStreamPage, TestLifeStream> {
  @override
  List<TabWidget> tabs() {
    return [
      TabWidget('Base', baseFields()),
      TabWidget('Meta urls', metaUrls()),
      TabWidget('Input urls', inputUrls()),
      TabWidget('Audio', audio()),
      TabWidget('Video', video()),
      TabWidget('Other', other())
    ];
  }
}

// cvdata
class CvDataStreamPage extends EncodeStreamPageBase<CvDataStream> {
  CvDataStreamPage.add(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc)
      : super.add(server, loader, rpc, StreamType.CV_DATA);

  CvDataStreamPage.edit(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, CvDataStream stream)
      : super.edit(server, loader, rpc, stream);

  @override
  _CvDataStreamPageState createState() {
    return _CvDataStreamPageState();
  }
}

class _CvDataStreamPageState extends _EncodeStreamPageBaseState<CvDataStreamPage, CvDataStream> {
  @override
  List<TabWidget> tabs() {
    return [
      TabWidget('Base', baseFields()),
      TabWidget('Meta urls', metaUrls()),
      TabWidget('Input urls', inputUrls()),
      TabWidget('Audio', audio()),
      TabWidget('Video', video()),
      TabWidget('Other', other())
    ];
  }
}

// changer
class ChangerRelayStreamPage extends RelayStreamPageBase<ChangerRelayStream> {
  ChangerRelayStreamPage.add(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc)
      : super.add(server, loader, rpc, StreamType.CHANGER_RELAY);

  ChangerRelayStreamPage.edit(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, ChangerRelayStream stream)
      : super.edit(server, loader, rpc, stream);

  @override
  _ChangerRelayStreamPageState createState() {
    return _ChangerRelayStreamPageState();
  }
}

class _ChangerRelayStreamPageState
    extends _RelayStreamPageBaseState<ChangerRelayStreamPage, ChangerRelayStream> {}

class ChangerEncoderStreamPage extends EncodeStreamPageBase<ChangerEncodeStream> {
  ChangerEncoderStreamPage.add(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc)
      : super.add(server, loader, rpc, StreamType.CHANGER_ENCODE);

  ChangerEncoderStreamPage.edit(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, ChangerEncodeStream stream)
      : super.edit(server, loader, rpc, stream);

  @override
  _ChangerEncoderStreamPageState createState() {
    return _ChangerEncoderStreamPageState();
  }
}

class _ChangerEncoderStreamPageState
    extends _EncodeStreamPageBaseState<ChangerEncoderStreamPage, ChangerEncodeStream> {}

// vod relay
//? only adds vods fields
class VodRelayStreamPage extends RelayStreamPageBase<VodRelayStream> {
  VodRelayStreamPage.add(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc)
      : super.add(server, loader, rpc, StreamType.VOD_RELAY);

  VodRelayStreamPage.edit(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, VodRelayStream stream)
      : super.edit(server, loader, rpc, stream);

  @override
  _VodRelayStreamPage createState() {
    return _VodRelayStreamPage();
  }
}

class _VodRelayStreamPage extends _RelayStreamPageBaseState<VodRelayStreamPage, VodRelayStream>
    with VodDialogBundle {
  @override
  void initState() {
    super.initState();
    initVod(origStream.name, origStream.description, origStream.userScore, origStream.icon);
  }

  @override
  List<TabWidget> tabs() {
    return [
      TabWidget('Base', baseFields()),
      TabWidget('Meta urls', metaUrls()),
      TabWidget('Input urls', inputUrls()),
      TabWidget('Output urls', outputUrls()),
      TabWidget('VOD', _vodFields()),
      TabWidget('Audio', audio()),
      TabWidget('Video', video()),
      TabWidget('Other', other())
    ];
  }

  @override
  Widget baseFields() {
    return ListView(children: [
      nameField().copyWith(controller: nameController),
      iconField().copyWith(controller: iconController),
      descriptionField().copyWith(controller: descriptionController),
      groupField(),
      priceField(),
      visibleField(),
      iarcField()
    ]);
  }

  Widget _vodFields() {
    return ListView(children: [
      typeField(stream.vodType),
      tmdbField(context, () => setState(() {})),
      trailerUrlField(stream.trailerUrl),
      userScoreField(stream.userScore),
      primeDateField(stream.primeDate),
      countryField(stream.country),
      durationField(stream.duration)
    ]);
  }

  @override
  void onVodDetailsChanged(VodDetails? details) {
    setState(() {
      stream.name = details!.name;
      stream.description = details.description;
      stream.userScore = details.userScore;
      stream.primeDate = details.primeDate;
      stream.icon = details.icon;
    });
  }

  @override
  void onType(VodType? type) {
    stream.vodType = type!;
  }

  @override
  void onTrailerUrl(String term) {
    stream.trailerUrl = term;
  }

  @override
  void onUserScore(double? term) {
    stream.userScore = term!;
  }

  @override
  void onPrimeDate(int date) {
    stream.primeDate = date;
  }

  @override
  void onCountry(String term) {
    stream.country = term;
  }

  @override
  void onDuration(int date) {
    stream.duration = date;
  }
}

// vod encode
//? only adds vods fields
class VodEncodeStreamPage extends EncodeStreamPageBase<VodEncodeStream> {
  VodEncodeStreamPage.add(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc)
      : super.add(server, loader, rpc, StreamType.VOD_ENCODE);

  VodEncodeStreamPage.edit(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, VodEncodeStream stream)
      : super.edit(server, loader, rpc, stream);

  @override
  _VodEncodeStreamPageState createState() {
    return _VodEncodeStreamPageState();
  }
}

class _VodEncodeStreamPageState
    extends _EncodeStreamPageBaseState<VodEncodeStreamPage, VodEncodeStream> with VodDialogBundle {
  @override
  void initState() {
    super.initState();
    initVod(origStream.name, origStream.description, origStream.userScore, origStream.icon);
  }

  @override
  List<TabWidget> tabs() {
    return [
      TabWidget('Base', baseFields()),
      TabWidget('Meta urls', metaUrls()),
      TabWidget('Input urls', inputUrls()),
      TabWidget('Output urls', outputUrls()),
      TabWidget('VOD', _vodFields()),
      TabWidget('Audio', audio()),
      TabWidget('Video', video()),
      TabWidget('Other', other())
    ];
  }

  @override
  Widget baseFields() {
    return ListView(children: [
      nameField().copyWith(controller: nameController),
      iconField().copyWith(controller: iconController),
      descriptionField().copyWith(controller: descriptionController),
      groupField(),
      priceField(),
      visibleField(),
      iarcField()
    ]);
  }

  Widget _vodFields() {
    return ListView(children: [
      typeField(stream.vodType),
      tmdbField(context, () => setState(() {})),
      trailerUrlField(stream.trailerUrl),
      userScoreField(stream.userScore),
      primeDateField(stream.primeDate),
      countryField(stream.country),
      durationField(stream.duration)
    ]);
  }

  @override
  void onVodDetailsChanged(VodDetails details) {
    setState(() {
      stream.name = details.name;
      stream.description = details.description;
      stream.userScore = details.userScore;
      stream.primeDate = details.primeDate;
      stream.icon = details.icon;
    });
  }

  @override
  void onType(VodType? type) {
    stream.vodType = type!;
  }

  @override
  void onTrailerUrl(String term) {
    stream.trailerUrl = term;
  }

  @override
  void onUserScore(double? term) {
    stream.userScore = term!;
  }

  @override
  void onPrimeDate(int date) {
    stream.primeDate = date;
  }

  @override
  void onCountry(String term) {
    stream.country = term;
  }

  @override
  void onDuration(int date) {
    stream.duration = date;
  }
}

// cod relay
class CodRelayStreamPage extends RelayStreamPageBase<CodRelayStream> {
  CodRelayStreamPage.add(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc)
      : super.add(server, loader, rpc, StreamType.COD_RELAY);

  CodRelayStreamPage.edit(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, CodRelayStream stream)
      : super.edit(server, loader, rpc, stream);

  @override
  _CodRelayStreamPageState createState() {
    return _CodRelayStreamPageState();
  }
}

class _CodRelayStreamPageState
    extends _RelayStreamPageBaseState<CodRelayStreamPage, CodRelayStream> {}

// cod encode
class CodEncodeStreamPage extends EncodeStreamPageBase<CodEncodeStream> {
  CodEncodeStreamPage.add(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc)
      : super.add(server, loader, rpc, StreamType.COD_ENCODE);

  CodEncodeStreamPage.edit(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, CodEncodeStream stream)
      : super.edit(server, loader, rpc, stream);

  @override
  _CodEncodeStreamPageState createState() {
    return _CodEncodeStreamPageState();
  }
}

class _CodEncodeStreamPageState
    extends _EncodeStreamPageBaseState<CodEncodeStreamPage, CodEncodeStream> {}

// event stream
class EventStreamPage extends EncodeStreamPageBase<EventStream> {
  EventStreamPage.add(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc)
      : super.add(server, loader, rpc, StreamType.EVENT);

  EventStreamPage.edit(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, EventStream stream)
      : super.edit(server, loader, rpc, stream);

  @override
  _EventStreamPageState createState() {
    return _EventStreamPageState();
  }
}

class _EventStreamPageState extends _EncodeStreamPageBaseState<EventStreamPage, EventStream> {}
