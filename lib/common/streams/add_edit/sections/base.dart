import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/pages/home/common/layout.dart';
import 'package:flutter_common/widgets.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';

class EpgIdField<S extends IStream> extends StatefulWidget {
  final S stream;

  const EpgIdField(this.stream);

  @override
  _EpgIdFieldState<S> createState() => _EpgIdFieldState<S>();
}

class _EpgIdFieldState<S extends IStream> extends State<EpgIdField<S>> {
  bool get hasEpg => (widget.stream.epgId ?? '').isNotEmpty;
  final BehaviorSubject<bool> _hasEpg = BehaviorSubject<bool>();

  @override
  void dispose() {
    super.dispose();
    _hasEpg.close();
  }

  @override
  Widget build(BuildContext context) {
    return HomeLayoutBuilder(Column(children: [epgField(), archive()]),
        Row(children: [Expanded(child: epgField()), SizedBox(width: 144, child: archive())]));
  }

  Widget epgField() {
    return TextFieldEx(
        key: const ValueKey<String>('Epg field'),
        maxSymbols: EpgName.MAX_LENGTH,
        hintText: 'Epg ID',
        init: widget.stream.epgId,
        onFieldChanged: (term) {
          if (hasEpg != term.isNotEmpty) {
            _hasEpg.add(term.isNotEmpty);
          }
          widget.stream.epgId = term;
        });
  }

  Widget archive() {
    return StreamBuilder(
        initialData: hasEpg,
        stream: _hasEpg.stream,
        builder: (context, snapshot) {
          if (hasEpg) {
            return StateCheckBox(
                title: 'Archive',
                init: widget.stream.archive,
                onChanged: (value) {
                  widget.stream.archive = value;
                });
          }
          return IgnorePointer(
              ignoring: !hasEpg,
              child:
                  Opacity(opacity: hasEpg ? 1 : 0.5, child: const StateCheckBox(title: 'Archive')));
        });
  }
}
