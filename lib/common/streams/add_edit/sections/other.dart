import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/sections/common.dart';
import 'package:fastocloud_pro_panel/models/widgets/base.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';

class OtherSection<S extends HardwareStream> extends StatefulWidget {
  final S stream;

  const OtherSection(this.stream);

  @override
  _OtherSectionState<S> createState() => _OtherSectionState<S>();
}

class _OtherSectionState<S extends HardwareStream> extends State<OtherSection<S>> {
  S get stream => widget.stream;

  @override
  Widget build(BuildContext context) {
    return ListView(children: fields());
  }

  List<Widget> fields() {
    return [
      loopField(),
      autoExitField(),
      phoenixField(),
      logLevelField(),
      feedbackField(),
      restartAttemptsField()
    ];
  }

  Widget loopField() {
    return StateCheckBox(
        title: 'Loop',
        init: stream.loop,
        onChanged: (value) {
          stream.loop = value;
        });
  }

  Widget autoExitField() {
    return OptionalFieldTile(
        title: 'Change Stream Time Work',
        init: stream.autoExit != null,
        onChanged: (value) {
          stream.autoExit = value ? 3600 : null;
        },
        builder: () {
          return NumberTextField.integer(
              minInt: AutoExitTime.MIN,
              maxInt: AutoExitTime.MAX,
              hintText: 'Stream Time Work',
              initInt: stream.autoExit,
              onFieldChangedInt: (term) {
                stream.autoExit = term;
              });
        });
  }

  Widget phoenixField() {
    return StateCheckBox(
        title: 'Phoenix',
        init: stream.phoenix,
        onChanged: (value) {
          stream.phoenix = value;
        });
  }

  Widget logLevelField() {
    return DropdownButtonEx<StreamLogLevel>(
        hint: 'Audio codes',
        value: stream.logLevel,
        values: const <StreamLogLevel>[
          StreamLogLevel.EMERG,
          StreamLogLevel.ALERT,
          StreamLogLevel.CRIT,
          StreamLogLevel.ERR,
          StreamLogLevel.WARNING,
          StreamLogLevel.NOTICE,
          StreamLogLevel.INFO,
          StreamLogLevel.DEBUG
        ],
        onChanged: (t) {
          setState(() {
            stream.logLevel = t!;
          });
        },
        itemBuilder: (StreamLogLevel logLevel) {
          return DropdownMenuItem(child: Text(logLevel.toHumanReadable()), value: logLevel);
        });
  }

  Widget feedbackField() {
    return TextFieldEx.readOnly(hint: 'Feedback directory', init: stream.feedbackDirectory!);
  }

  Widget restartAttemptsField() {
    return NumberTextField.integer(
        minInt: RestartAttempts.MIN,
        maxInt: RestartAttempts.MAX,
        hintText: 'Max restart attempts',
        canBeEmpty: false,
        initInt: stream.restartAttempts,
        onFieldChangedInt: (term) {
          if (term != null) stream.restartAttempts = term;
        });
  }

  Widget autoStartField() {
    return StateCheckBox(
        title: 'Auto start',
        init: stream.autoStart,
        onChanged: (value) {
          stream.autoStart = value;
        });
  }
}

class OtherSectionRelay<S extends RelayStream> extends OtherSection<S> {
  const OtherSectionRelay(S stream) : super(stream);

  @override
  _OtherSectionRelayState<S> createState() => _OtherSectionRelayState<S>();
}

class _OtherSectionRelayState<S extends RelayStream> extends _OtherSectionState<S> {
  @override
  List<Widget> fields() {
    return super.fields() + [autoStartField()];
  }
}

class OtherSectionEncode<S extends EncodeStream> extends OtherSection<S> {
  const OtherSectionEncode(S stream) : super(stream);

  @override
  _OtherSectionEncodeState<S> createState() => _OtherSectionEncodeState<S>();
}

class _OtherSectionEncodeState<S extends EncodeStream> extends _OtherSectionState<S> {
  @override
  List<Widget> fields() {
    return super.fields() + [_machineLearningField(), autoStartField()];
  }

  Widget _machineLearningField() {
    return OptionalFieldTile(
        title: 'Machine Learning',
        init: stream.machineLearning != null,
        onChanged: (value) {
          stream.machineLearning = value ? MachineLearning.createYoloV3TinyExample() : null;
        },
        builder: () => MachineLearningField(stream.machineLearning!));
  }
}
