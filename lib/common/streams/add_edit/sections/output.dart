import 'dart:async';

import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/sections/common.dart';
import 'package:fastocloud_pro_panel/events/server_events.dart';
import 'package:fastocloud_pro_panel/models/widgets/urls/output.dart';
import 'package:flutter/material.dart';

class OutputUrlSection<S extends IStream> extends StatefulWidget {
  final S stream;
  final Server server;
  final void Function(OutputUrl) probe;
  final Stream<JsonRpcEvent> rpc;

  const OutputUrlSection(this.stream, this.server, this.probe, this.rpc);

  @override
  _OutputUrlSectionState<S> createState() => _OutputUrlSectionState<S>();
}

class _OutputUrlSectionState<S extends IStream> extends State<OutputUrlSection<S>> {
  S get stream => widget.stream;

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      _addOutputUrl(),
      Expanded(
          child: ListView.builder(
              itemCount: stream.output.length,
              itemBuilder: (context, index) {
                return IOFieldBorder(
                    child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: _createTile(index),
                ));
              }))
    ]);
  }

  Widget _createTile(int index) {
    final OutputUrl url = stream.output[index];
    final bool isHttp = url is HttpOutputUrl;
    final bool isSrt = url is SrtOutputUrl;
    final bool isRtmp = url is RtmpOutputUrl;
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(children: [
            IOTypeRadioTile(
                title: 'HLS',
                selected: isHttp,
                onChanged: () {
                  _setUrlType(
                      index,
                      HttpOutputUrl(
                          id: index,
                          uri: '',
                          hlsSinkType: HlsSinkType.HLSSINK,
                          httpRoot: '',
                          hlsType: HlsType.HLS_PULL));
                }),
            IOTypeRadioTile(
                title: 'SRT',
                selected: isSrt,
                onChanged: () {
                  _setUrlType(index, SrtOutputUrl(id: index, uri: '', mode: SrtMode.CALLER));
                }),
            IOTypeRadioTile(
                title: 'RTMP',
                selected: isRtmp,
                onChanged: () {
                  _setUrlType(index, RtmpOutputUrl(id: index, uri: ''));
                }),
            IOTypeRadioTile(
                title: 'Other',
                selected: !(isHttp || isSrt || isRtmp),
                onChanged: () {
                  _setUrlType(index, OutputUrl.createInvalid(id: index));
                })
          ])),
      _createOutputField(index)
    ]);
  }

  Widget _createOutputField(int index) {
    final OutputUrl url = stream.output[index];
    final canDelete = index == 0
        ? null
        : () {
            _deleteOutput(url);
          };
    final probeCB = widget.server.isPro() ? widget.probe : null;
    if (url is HttpOutputUrl) {
      final bool isProxy =
          stream.type() == StreamType.PROXY || stream.type() == StreamType.VOD_PROXY;
      if (isProxy) {
        return HLSOutputUrlField(url, canDelete, probeCB,
            sid: widget.server.id,
            rpc: widget.rpc,
            proxyDirectory: widget.server.proxyDirectory, onPrepareOutputTemplate: (url) {
          return _convertProxyFileToHttp(url)!;
        }, isExternal: true);
      }

      if (!url.isHls()) {
        return HttpOutputUrlField(url, canDelete, probeCB);
      }

      final nginxUrl = _generateNginxUrl(url);
      return NginxHttpOutputUrlField(url, canDelete, probeCB, nginxUrl?.toString());
    } else if (url is SrtOutputUrl) {
      return SrtOutputUrlField(url, canDelete, probeCB);
    } else if (url is RtmpOutputUrl) {
      return RtmpOutputUrlField(url, canDelete, probeCB);
    }
    return OutputUrlField(url, canDelete, probeCB);
  }

  Widget _addOutputUrl() {
    return TextButton.icon(
        icon: const Icon(Icons.add), label: const Text('Add output URL'), onPressed: _addOutput);
  }

  String? _convertProxyFileToHttp(String path) {
    final url = widget.server.generateHttpProxyUrl(path);
    if (url == null) {
      return null;
    }
    return url.toString();
  }

  Uri? _generateNginxUrl(HttpOutputUrl url) {
    return widget.server.generateNginxUrl(url.uri);
  }

  void _addOutput() {
    setState(() {
      final int index = stream.output.length;
      stream.output.add(OutputUrl.createInvalid(id: index));
    });
  }

  void _deleteOutput(OutputUrl url) {
    setState(() {
      stream.output.remove(url);
    });
  }

  void _setUrlType(int index, OutputUrl url) {
    setState(() {
      stream.output[index] = url;
    });
  }
}
