import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/models/widgets/base.dart';
import 'package:fastotv_dart/commands_info/meta_url.dart';
import 'package:flutter/material.dart';

class MetaUrlSection<S extends IStream> extends StatefulWidget {
  final S stream;

  const MetaUrlSection(this.stream);

  @override
  _MetaUrlSectionState<S> createState() => _MetaUrlSectionState<S>();
}

class _MetaUrlSectionState<S extends IStream> extends State<MetaUrlSection<S>> {
  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [_addMetaUrl(), Expanded(child: ListView(children: _metaUrls()))]);
  }

  List<Widget> _metaUrls() {
    return List<Widget>.generate(widget.stream.meta.length, (int index) {
      final MetaUrl url = widget.stream.meta[index];
      return MetaUrlField(url, () {
        setState(() {
          widget.stream.meta.remove(url);
        });
      });
    });
  }

  Widget _addMetaUrl() {
    return TextButton.icon(
        icon: const Icon(Icons.add),
        label: const Text('Add meta URL'),
        onPressed: () {
          setState(() {
            widget.stream.meta.insert(0, MetaUrl('Test', 'http://localhost/test.doc'));
          });
        });
  }
}
