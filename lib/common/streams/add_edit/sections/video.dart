import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/sections/common.dart';
import 'package:fastocloud_pro_panel/models/widgets/base.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';

class VideoSection<S extends HardwareStream> extends StatefulWidget {
  final S stream;

  const VideoSection(this.stream);

  @override
  _VideoSectionState<S> createState() => _VideoSectionState<S>();
}

class _VideoSectionState<S extends HardwareStream> extends State<VideoSection<S>> {
  S get stream => widget.stream;

  @override
  Widget build(BuildContext context) {
    if (!stream.haveVideo) {
      return ListView(children: [haveVideoField()]);
    }
    return ListView(children: fields());
  }

  List<Widget> fields() {
    return [haveVideoField()];
  }

  Widget haveVideoField() {
    return StateCheckBox(
        title: 'Have video',
        init: stream.haveVideo,
        onChanged: (value) {
          setState(() {
            stream.haveVideo = value;
          });
        });
  }

  Widget relayVideoTypeField() {
    return DropdownButtonEx<RelayType>(
        hint: stream.relayVideoType.toString(),
        value: stream.relayVideoType,
        values: const [RelayType.LITE, RelayType.DEEP],
        onChanged: (t) {
          setState(() {
            stream.relayVideoType = t!;
          });
        },
        itemBuilder: (RelayType type) {
          return DropdownMenuItem(child: Text(type.toHumanReadable()), value: type);
        });
  }
}

class VideoSectionRelay<S extends RelayStream> extends VideoSection<S> {
  const VideoSectionRelay(S stream) : super(stream);

  @override
  _VideoSectionRelayState<S> createState() => _VideoSectionRelayState<S>();
}

class _VideoSectionRelayState<S extends RelayStream> extends _VideoSectionState<S> {
  @override
  List<Widget> fields() {
    return super.fields() + [relayVideoTypeField(), _videoParser()];
  }

  Widget _videoParser() {
    return OptionalFieldTile(
        title: 'Video parser',
        init: stream.videoParser != null,
        onChanged: (value) {
          stream.videoParser = value ? VideoParser.H264 : null;
        },
        builder: () {
          return DropdownButtonEx<VideoParser>(
              hint: 'Video parser',
              value: stream.videoParser!,
              values: VideoParser.values,
              onChanged: (t) {
                setState(() {
                  stream.videoParser = t;
                });
              },
              itemBuilder: (VideoParser parser) {
                return DropdownMenuItem(child: Text(parser.toHumanReadable()), value: parser);
              });
        });
  }
}

class VideoSectionEncode<S extends EncodeStream> extends VideoSection<S> {
  const VideoSectionEncode(S stream) : super(stream);

  @override
  _VideoSectionEncodeState<S> createState() => _VideoSectionEncodeState<S>();
}

class _VideoSectionEncodeState<S extends EncodeStream> extends _VideoSectionState<S> {
  @override
  List<Widget> fields() {
    return [
      ...super.fields(),
      _frameRateField(),
      _relayVideoField(),
      _videoCodecField(),
      _videoBitrateField(),
      _aspectRatioField(),
      _deinterlaceField(),
      _sizeField(),
      _logoField(),
      _rsvgLogoField()
    ];
  }

  Widget _relayVideoField() {
    return OptionalFieldTile(
        title: 'Relay video',
        init: stream.relayVideo,
        onChanged: (value) {
          stream.relayVideo = value;
        },
        builder: () => relayVideoTypeField());
  }

  Widget _deinterlaceField() {
    return StateCheckBox(
        title: 'Deinterlace',
        init: stream.deinterlace,
        onChanged: (value) {
          stream.deinterlace = value;
        });
  }

  Widget _videoCodecField() {
    return DropdownButtonEx<VideoCodec>(
        hint: 'Video codec',
        value: stream.videoCodec,
        values: VideoCodec.values,
        onChanged: (t) {
          setState(() {
            stream.videoCodec = t!;
          });
        },
        itemBuilder: (VideoCodec codec) {
          return DropdownMenuItem(child: Text(codec.toHumanReadable()), value: codec);
        });
  }

  Widget _frameRateField() {
    return OptionalFieldTile(
        title: 'Change framerate',
        init: stream.frameRate != null,
        onChanged: (value) {
          stream.frameRate = value ? 25 : null;
        },
        builder: () {
          return NumberTextField.integer(
              minInt: FrameRate.MIN,
              maxInt: FrameRate.MAX,
              hintText: 'Frame Rate',
              initInt: stream.frameRate,
              onFieldChangedInt: (term) {
                stream.frameRate = term;
              });
        });
  }

  Widget _videoBitrateField() {
    return OptionalFieldTile(
        title: 'Change bitrate',
        init: stream.videoBitRate != null,
        onChanged: (value) {
          stream.videoBitRate = value ? 2048 * 1024 : null;
        },
        builder: () {
          return NumberTextField.integer(
              hintText: 'Bitrate',
              initInt: stream.videoBitRate,
              onFieldChangedInt: (term) {
                stream.videoBitRate = term;
              });
        });
  }

  Widget _sizeField() {
    return OptionalFieldTile(
        title: 'Change resolution',
        init: stream.size != null,
        onChanged: (value) {
          stream.size = value ? Size.create640x480() : null;
        },
        builder: () => SizeField(stream.size!));
  }

  Widget _logoField() {
    return OptionalFieldTile(
        title: 'Insert logo',
        init: stream.logo != null,
        onChanged: (value) {
          stream.logo = value ? Logo.createExample() : null;
        },
        builder: () => LogoField(stream.logo!));
  }

  Widget _rsvgLogoField() {
    return OptionalFieldTile(
        title: 'Insert RSVG Logo',
        init: stream.rsvgLogo != null,
        onChanged: (value) {
          stream.rsvgLogo = value ? RsvgLogo.createExample() : null;
        },
        builder: () => RsvgLogoField(stream.rsvgLogo!));
  }

  Widget _aspectRatioField() {
    return OptionalFieldTile(
        title: 'Change Aspect Ratio',
        init: stream.aspectRatio != null,
        onChanged: (value) {
          stream.aspectRatio = value ? Rational.create4x3() : null;
        },
        builder: () => AspectRatioField(stream.aspectRatio!));
  }
}

class VideoSectionChanger<S extends EncodeStream> extends VideoSection<S> {
  const VideoSectionChanger(S stream) : super(stream);

  @override
  _VideoSectionChangerState<S> createState() => _VideoSectionChangerState<S>();
}

class _VideoSectionChangerState<S extends EncodeStream> extends _VideoSectionEncodeState<S> {
  @override
  List<Widget> fields() {
    return [
      ...super.fields(),
      _frameRateField(),
      _videoCodecField(),
      _videoBitrateField(),
      _aspectRatioField(),
      _deinterlaceField(),
      _sizeField(),
      _logoField(),
      _rsvgLogoField()
    ];
  }
}
