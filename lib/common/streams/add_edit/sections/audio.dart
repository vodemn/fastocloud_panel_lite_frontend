import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/sections/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';

class AudioSection<S extends HardwareStream> extends StatefulWidget {
  final S stream;

  const AudioSection(this.stream);

  @override
  _AudioSectionState<S> createState() => _AudioSectionState<S>();
}

class _AudioSectionState<S extends HardwareStream> extends State<AudioSection<S>> {
  S get stream => widget.stream;

  @override
  Widget build(BuildContext context) {
    if (!stream.haveAudio) {
      return ListView(children: [haveAudioField()]);
    }
    return ListView(children: fields());
  }

  List<Widget> fields() {
    return [haveAudioField(), audioSelectField(), audioTracksCountField()];
  }

  Widget haveAudioField() {
    return StateCheckBox(
        title: 'Have audio',
        init: stream.haveAudio,
        onChanged: (value) {
          setState(() {
            stream.haveAudio = value;
          });
        });
  }

  Widget audioTracksCountField() {
    return NumberTextField.integer(
        minInt: AudioTracksCount.MIN,
        maxInt: AudioTracksCount.MAX,
        hintText: 'Audio tracks count',
        canBeEmpty: false,
        initInt: stream.audioTracksCount,
        onFieldChangedInt: (term) {
          if (term != null) stream.audioTracksCount = term;
        });
  }

  Widget audioSelectField() {
    return OptionalFieldTile(
        title: 'Change Audio Track',
        init: stream.audioSelect != null,
        onChanged: (value) {
          stream.audioSelect = value ? 0 : null;
        },
        builder: () {
          return NumberTextField.integer(
              minInt: AudioSelect.MIN,
              maxInt: AudioSelect.MAX,
              hintText: 'Audio Track',
              initInt: stream.audioSelect,
              onFieldChangedInt: (term) {
                stream.audioSelect = term;
              });
        });
  }

  Widget relayAudioTypeField() {
    return DropdownButtonEx<RelayType>(
        hint: stream.relayAudioType.toString(),
        value: stream.relayAudioType,
        values: const [RelayType.LITE, RelayType.DEEP],
        onChanged: (t) {
          setState(() {
            stream.relayAudioType = t!;
          });
        },
        itemBuilder: (RelayType type) {
          return DropdownMenuItem(child: Text(type.toHumanReadable()), value: type);
        });
  }
}

class AudioSectionRelay<S extends RelayStream> extends AudioSection<S> {
  const AudioSectionRelay(S stream) : super(stream);

  @override
  _AudioSectionRelayState<S> createState() => _AudioSectionRelayState<S>();
}

class _AudioSectionRelayState<S extends RelayStream> extends _AudioSectionState<S> {
  @override
  List<Widget> fields() {
    return super.fields() + [audioParser(), relayAudioTypeField()];
  }

  Widget audioParser() {
    return OptionalFieldTile(
        title: 'Audio parser',
        init: stream.audioParser != null,
        onChanged: (value) {
          stream.audioParser = value ? AudioParser.AAC : null;
        },
        builder: () {
          return DropdownButtonEx<AudioParser>(
              hint: 'Audio parser',
              value: stream.audioParser!,
              values: AudioParser.values,
              onChanged: (t) {
                setState(() {
                  stream.audioParser = t;
                });
              },
              itemBuilder: (AudioParser parser) {
                return DropdownMenuItem(child: Text(parser.toHumanReadable()), value: parser);
              });
        });
  }
}

class AudioSectionEncode<S extends EncodeStream> extends AudioSection<S> {
  const AudioSectionEncode(S stream) : super(stream);

  @override
  _AudioSectionEncodeState<S> createState() => _AudioSectionEncodeState<S>();
}

class _AudioSectionEncodeState<S extends EncodeStream> extends _AudioSectionState<S> {
  @override
  List<Widget> fields() {
    return [
      ...super.fields(),
      _relayAudioField(),
      _volumeField(),
      _audioCodecField(),
      _audioBitrateField(),
      _audioChannelsCountField()
    ];
  }

  Widget _relayAudioField() {
    return OptionalFieldTile(
        title: 'Relay audio',
        init: stream.relayAudio,
        onChanged: (value) {
          stream.relayAudio = value;
        },
        builder: () => relayAudioTypeField());
  }

  Widget _volumeField() {
    return NumberTextField.decimal(
        minDouble: Volume.MIN,
        maxDouble: Volume.MAX,
        hintText: 'Volume',
        canBeEmpty: false,
        initDouble: stream.volume,
        onFieldChangedDouble: (term) {
          if (term != null) stream.volume = term;
        });
  }

  Widget _audioCodecField() {
    return DropdownButtonEx<AudioCodec>(
        hint: 'Audio codec',
        value: stream.audioCodec,
        values: AudioCodec.values,
        onChanged: (t) {
          setState(() {
            stream.audioCodec = t!;
          });
        },
        itemBuilder: (AudioCodec codec) {
          return DropdownMenuItem(child: Text(codec.toHumanReadable()), value: codec);
        });
  }

  Widget _audioBitrateField() {
    return OptionalFieldTile(
        title: 'Change bitrate',
        init: stream.audioBitRate != null,
        onChanged: (value) {
          stream.audioBitRate = value ? 125 * 1024 : null;
        },
        builder: () {
          return NumberTextField.integer(
              hintText: 'Bitrate',
              initInt: stream.audioBitRate,
              onFieldChangedInt: (term) {
                stream.audioBitRate = term;
              });
        });
  }

  Widget _audioChannelsCountField() {
    return OptionalFieldTile(
        title: 'Change channels count',
        init: stream.audioChannelsCount != null,
        onChanged: (value) {
          stream.audioChannelsCount = value ? 2 : null;
        },
        builder: () {
          return NumberTextField.integer(
              minInt: AudioChannelsCount.MIN,
              maxInt: AudioChannelsCount.MAX,
              hintText: 'Channels Count',
              initInt: stream.audioChannelsCount,
              onFieldChangedInt: (term) {
                stream.audioChannelsCount = term;
              });
        });
  }
}

class AudioSectionChanger<S extends ChangerEncodeStream> extends AudioSection<S> {
  const AudioSectionChanger(S stream) : super(stream);

  @override
  _AudioSectionChangerState<S> createState() => _AudioSectionChangerState<S>();
}

class _AudioSectionChangerState<S extends ChangerEncodeStream> extends _AudioSectionEncodeState<S> {
  @override
  List<Widget> fields() {
    return [
      ...super.fields(),
      _volumeField(),
      _audioCodecField(),
      _audioBitrateField(),
      _audioChannelsCountField()
    ];
  }
}
