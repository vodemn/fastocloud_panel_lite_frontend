import 'dart:async';

import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/sections/common.dart';
import 'package:fastocloud_pro_panel/events/server_events.dart';
import 'package:fastocloud_pro_panel/models/widgets/urls/input.dart';
import 'package:flutter/material.dart';

class InputUrlSection<S extends HardwareStream> extends StatefulWidget {
  final S stream;
  final Server server;
  final void Function(InputUrl) probe;
  final Stream<JsonRpcEvent> rpc;

  const InputUrlSection(this.stream, this.server, this.probe, this.rpc);

  @override
  _InputUrlSectionState<S> createState() {
    return _InputUrlSectionState<S>();
  }
}

class _InputUrlSectionState<S extends HardwareStream> extends State<InputUrlSection<S>> {
  S get stream => widget.stream;

  @override
  Widget build(BuildContext context) {
    switch (stream.type()) {
      case StreamType.CHANGER_RELAY:
      case StreamType.CHANGER_ENCODE:
        return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          _addInputUrl(),
          Expanded(
              child: ListView.builder(
                  itemCount: stream.input.length,
                  itemBuilder: (context, index) {
                    return IOFieldBorder(
                        child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: _createTile(index),
                    ));
                  }))
        ]);
      default:
        return ListView(children: [_createTile(0)]);
    }
  }

  Widget _createTile(int index) {
    final InputUrl url = stream.input[index];
    final bool isHttp = url is HttpInputUrl;
    final bool isUdp = url is UdpInputUrl;
    final bool isFile = url is FileInputUrl;
    final bool isSrt = url is SrtInputUrl;
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(children: [
            IOTypeRadioTile(
                title: 'HTTP/HTTPS (HLS)',
                selected: isHttp,
                onChanged: () {
                  _setUrlType(index, HttpInputUrl(id: index, uri: ''));
                }),
            IOTypeRadioTile(
                title: 'UDP',
                selected: isUdp,
                onChanged: () {
                  _setUrlType(index, UdpInputUrl(id: index, uri: ''));
                }),
            IOTypeRadioTile(
                title: 'File',
                selected: isFile,
                onChanged: () {
                  _setUrlType(index, FileInputUrl(id: index, uri: ''));
                }),
            IOTypeRadioTile(
                title: 'SRT',
                selected: isSrt,
                onChanged: () {
                  _setUrlType(index, SrtInputUrl(id: index, uri: '', mode: SrtMode.CALLER));
                }),
            IOTypeRadioTile(
                title: 'Other',
                selected: !(isHttp || isUdp || isFile || isSrt),
                onChanged: () {
                  _setUrlType(index, InputUrl.createInvalid(id: index));
                })
          ])),
      _createInputField(index, _deleteInputUrl)
    ]);
  }

  Widget _createInputField(int index, [void Function(InputUrl)? onDelete]) {
    final InputUrl url = stream.input[index];
    final canDelete = index == 0 || onDelete == null ? null : () => onDelete(url);
    final probeCB = widget.server.isPro() ? widget.probe : null;
    final scanCB = widget.server.isPro() ? (path) => stream.input[index].uri = path : null;
    if (url is HttpInputUrl) {
      return HttpInputUrlField(url, canDelete, probeCB);
    } else if (url is UdpInputUrl) {
      return UdpInputUrlField(url, canDelete, probeCB);
    } else if (url is FileInputUrl) {
      return FileInputUrlField(url, canDelete, probeCB, scanCB, widget.server.id!, widget.rpc);
    } else if (url is SrtInputUrl) {
      return SrtInputUrlField(url, canDelete, probeCB);
    }
    return InputUrlField(url, canDelete, probeCB);
  }

  Widget _addInputUrl() {
    return TextButton.icon(
        onPressed: _addInput, icon: const Icon(Icons.add), label: const Text('Add input URL'));
  }

  void _addInput() {
    setState(() {
      final int index = stream.input.length;
      stream.input.add(InputUrl.createInvalid(id: index));
    });
  }

  void _deleteInputUrl(InputUrl url) {
    setState(() {
      stream.input.remove(url);
    });
  }

  void _setUrlType(int index, InputUrl url) {
    setState(() {
      stream.input[index] = url;
    });
  }
}
