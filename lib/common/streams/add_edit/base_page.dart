import 'dart:async';

import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/add_edit_dialog.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/streams_loader.dart';
import 'package:fastocloud_pro_panel/events/server_events.dart';
import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/localization/translations.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:responsive_builder/responsive_builder.dart';

class TabWidget {
  final String name;
  final Widget widget;

  TabWidget(this.name, this.widget);
}

abstract class BaseStreamPage<S extends IStream> extends StatefulWidget {
  final S init;
  final Server server;
  final Stream<JsonRpcEvent> rpc;
  final StreamsLoader loader;

  String? get sid {
    return server.id;
  }

  static IStream _createDefault(Server server, StreamType type) {
    String defaultStreamLogoIcon() {
      final fetcher = locator<Fetcher>();
      final defaults = fetcher.defaults();
      return defaults.streamLogoIcon;
    }

    String defaultVodTrailerUrl() {
      final fetcher = locator<Fetcher>();
      final defaults = fetcher.defaults();
      return defaults.vodTrailerUrl;
    }

    return server.createDefault(type, defaultStreamLogoIcon(), defaultVodTrailerUrl());
  }

  BaseStreamPage.add(this.server, this.loader, this.rpc, StreamType type)
      : init = _createDefault(server, type) as S;

  BaseStreamPage.edit(this.server, this.loader, this.rpc, S stream) : init = stream.copy() as S;
}

abstract class BaseStreamPageState<T extends BaseStreamPage, S extends IStream> extends State<T> {
  List<TabWidget> tabs();

  int _currentSection = 0;

  TabWidget get currentTab {
    final allTabs = tabs();
    return allTabs[_currentSection];
  }

  late S stream;
  String? testOid;

  S get origStream => widget.init as S;

  bool get isAdd => stream.id == null;

  String get title => '${isAdd ? 'Add' : 'Edit'} stream';

  @override
  void initState() {
    super.initState();
    stream = widget.init as S;
  }

  @override
  Widget build(BuildContext context) {
    final iconTheme = Theme.of(context).iconTheme.copyWith(color: Colors.black);
    final allTabs = tabs();
    final List<Tab> trueTabs = [];
    for (final tab in allTabs) {
      trueTabs.add(Tab(text: tab.name));
    }

    return ResponsiveBuilder(builder: (context, sizingInformation) {
      return Dialog(
          backgroundColor: Colors.white,
          child: SizedBox(
              height: sizingInformation.isMobile ? double.maxFinite : 600,
              width: AddEditDialog.getWide(sizingInformation.isMobile),
              child: DefaultTabController(
                  length: allTabs.length,
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                    Padding(
                        padding: const EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 0.0),
                        child: Text(title,
                            style: Theme.of(context)
                                .textTheme
                                .headline6!
                                .copyWith(color: iconTheme.color))),
                    Padding(
                        padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
                        child: TabBar(
                            labelColor: iconTheme.color,
                            onTap: _setSection,
                            isScrollable: true,
                            tabs: trueTabs)),
                    Expanded(
                        child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0),
                            child: currentTab.widget)),
                    ButtonBar(children: [
                      FlatButtonEx.notFilled(text: TR_CANCEL, onPressed: exit),
                      FlatButtonEx.filled(text: isAdd ? TR_ADD : TR_SAVE, onPressed: save)
                    ])
                  ]))));
    });
  }

  void _setSection(int index) {
    setState(() {
      _currentSection = index;
    });
  }

  bool validate();

  void save() {
    final _validator = validate();

    if (_validator) {
      isAdd ? _addStream() : _editStream();
    }
  }

  void _addStream() {
    final resp = widget.loader.add(stream);
    resp.then((value) {
      exit();
    }, onError: (error) {
      showError(context, error);
    });
  }

  void _editStream() {
    final resp = widget.loader.edit(stream);
    resp.then((value) {
      exit();
    }, onError: (error) {
      showError(context, error);
    });
  }

  void exit() {
    Navigator.of(context).pop();
  }
}
