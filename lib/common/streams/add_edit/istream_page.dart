import 'dart:async';
import 'dart:convert';

import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/base_page.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/sections/base.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/sections/meta.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/sections/output.dart';
import 'package:fastocloud_pro_panel/common/streams/stream_icon.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/streams_loader.dart';
import 'package:fastocloud_pro_panel/events/server_events.dart';
import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:flutter_tags/flutter_tags.dart';

abstract class IStreamPage<S extends IStream> extends BaseStreamPage<S> {
  IStreamPage.add(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, StreamType type)
      : super.add(server, loader, rpc, type);

  IStreamPage.edit(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, S stream)
      : super.edit(server, loader, rpc, stream);
}

abstract class IStreamPageState<T extends IStreamPage, S extends IStream>
    extends BaseStreamPageState<T, S> {
  @override
  void initState() {
    super.initState();
    if (widget.server.isPro()) {
      widget.rpc.listen((event) {
        final rpc = event.data;
        if (testOid == rpc.id) {
          if (rpc.isMessage()) {
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                      title: const Text('Probe Stream'),
                      content: Text(rpc.result.toString(), softWrap: true));
                });
          } else if (rpc.isError()) {
            WidgetsBinding.instance!
                .addPostFrameCallback((_) => _showError(rpc.error!.code, rpc.error!.message));
          }
          testOid = null;
        }
      });
    }
  }

  @override
  List<TabWidget> tabs() {
    return [
      TabWidget('Base', baseFields()),
      TabWidget('Meta urls', metaUrls()),
      TabWidget('Output urls', outputUrls())
    ];
  }

  Widget baseFields() {
    return ListView(children: [
      nameField(),
      iconField(),
      descriptionField(),
      EpgIdField<S>(stream),
      groupField(),
      priceField(),
      visibleField(),
      iarcField()
    ]);
  }

  TextFieldEx nameField() {
    return TextFieldEx(
        maxSymbols: StreamName.MAX_LENGTH,
        hintText: 'Name',
        errorText: 'Enter name',
        init: stream.name,
        onFieldChanged: (term) {
          stream.name = term;
        });
  }

  TextFieldEx descriptionField() {
    return TextFieldEx(
        maxSymbols: StreamDescription.MAX_LENGTH,
        hintText: 'Description',
        init: stream.description,
        onFieldChanged: (term) {
          stream.description = term;
        });
  }

  Widget groupField() {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Tags(
            textField: TagsTextField(
                hintText: 'Add group',
                onSubmitted: (String str) {
                  if (str.isNotEmpty) {
                    setState(() {
                      stream.groups.add(str);
                    });
                  }
                }),
            itemCount: stream.groups.length,
            itemBuilder: (int index) {
              final item = stream.groups[index];
              return ItemTags(
                  key: Key(index.toString()),
                  pressEnabled: false,
                  index: index,
                  elevation: 1,
                  title: item,
                  textStyle: const TextStyle(fontSize: 16),
                  combine: ItemTagsCombine.withTextBefore,
                  removeButton: ItemTagsRemoveButton(onRemoved: () {
                    setState(() {
                      stream.groups.removeAt(index);
                    });
                    return true;
                  }));
            }));
  }

  TextFieldEx iconField() {
    return TextFieldEx(
        key: ValueKey<String>(stream.icon!),
        minSymbols: IconUrl.MIN_LENGTH,
        maxSymbols: IconUrl.MAX_LENGTH,
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'Icon',
        errorText: 'Enter icon URL',
        init: stream.icon,
        decoration: InputDecoration(icon: PreviewIcon.live(stream.icon!, width: 40, height: 40)),
        onFieldSubmit: (term) {
          setState(() {
            stream.icon = term;
          });
        },
        onFieldChanged: (term) {
          stream.icon = term;
        });
  }

  Widget iarcField() {
    return NumberTextField.integer(
        minInt: IARC.MIN,
        maxInt: IARC.MAX,
        hintText: 'IARC',
        canBeEmpty: false,
        initInt: stream.iarc,
        onFieldChangedInt: (term) {
          if (term != null) stream.iarc = term;
        });
  }

  Widget visibleField() {
    return StateCheckBox(
        title: 'Visible for subscribers',
        init: stream.visible,
        onChanged: (value) {
          stream.visible = value;
        });
  }

  Widget priceField() {
    return NumberTextField.decimal(
        minDouble: Price.MIN,
        maxDouble: Price.MAX,
        hintText: 'Price (\$)',
        canBeEmpty: false,
        initDouble: stream.price,
        onFieldChangedDouble: (term) {
          if (term != null) stream.price = term;
        });
  }

  Widget metaUrls() {
    return MetaUrlSection(stream);
  }

  Widget outputUrls() {
    return OutputUrlSection(stream, widget.server, probeRawUrl, widget.rpc);
  }

  void probeRawUrl(OutputUrl url) {
    final fetcher = locator<Fetcher>();
    final resp =
        fetcher.fetchPost('/server/stream/probe_out/${widget.sid}', {'url': url.toJson()}, [201]);
    resp.then((value) {
      final data = json.decode(value.body);
      testOid = data['oid'];
    }, onError: (error) {
      showError(context, error);
    });
  }

  void probeUrl(InputUrl url) {
    final fetcher = locator<Fetcher>();
    final resp =
        fetcher.fetchPost('/server/stream/probe_in/${widget.sid}', {'url': url.toJson()}, [201]);
    resp.then((value) {
      final data = json.decode(value.body);
      testOid = data['oid'];
    }, onError: (error) {
      showError(context, error);
    });
  }

  void _showError(int? statusCode, String? text) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(title: Text('$statusCode'), content: Text(text!, softWrap: true));
        });
  }
}
