import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/base_page.dart';
import 'package:fastocloud_pro_panel/common/streams/add_edit/istream_page.dart';
import 'package:fastocloud_pro_panel/common/streams/vods/vod_bundle.dart';
import 'package:fastocloud_pro_panel/dashboard/servers/details/streams/streams_loader.dart';
import 'package:fastocloud_pro_panel/events/server_events.dart';
import 'package:flutter/material.dart';

class ProxyStreamPage extends IStreamPage<ProxyStream> {
  ProxyStreamPage.add(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc)
      : super.add(server, loader, rpc, StreamType.PROXY);

  ProxyStreamPage.edit(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, ProxyStream stream)
      : super.edit(server, loader, rpc, stream);

  @override
  _ProxyStreamPageState createState() {
    return _ProxyStreamPageState();
  }
}

class _ProxyStreamPageState extends IStreamPageState<ProxyStreamPage, ProxyStream> {
  @override
  bool validate() {
    return stream.isValid();
  }
}

class ProxyVodStreamPage extends IStreamPage<VodProxyStream> {
  ProxyVodStreamPage.add(Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc)
      : super.add(server, loader, rpc, StreamType.VOD_PROXY);

  ProxyVodStreamPage.edit(
      Server server, StreamsLoader loader, Stream<JsonRpcEvent> rpc, ProxyStream stream)
      : super.edit(server, loader, rpc, stream as VodProxyStream);

  @override
  _ProxyVodStreamPageState createState() {
    return _ProxyVodStreamPageState();
  }
}

class _ProxyVodStreamPageState extends IStreamPageState<ProxyVodStreamPage, VodProxyStream>
    with VodDialogBundle {
  @override
  void initState() {
    super.initState();
    initVod(origStream.name, origStream.description, origStream.userScore, origStream.icon);
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      final details = tmdbRequest(context, stream.name);
      details.then((result) {
        setState(() {
          tmdbResults = result;
        });
      });
    });
  }

  @override
  bool validate() {
    return stream.isValid();
  }

  @override
  List<TabWidget> tabs() {
    return [
      TabWidget('Base', baseFields()),
      TabWidget('Meta urls', metaUrls()),
      TabWidget('Output urls', outputUrls()),
      TabWidget('VOD', _vodFields())
    ];
  }

  @override
  Widget baseFields() {
    return ListView(children: [
      nameField().copyWith(controller: nameController),
      iconField().copyWith(controller: iconController),
      descriptionField().copyWith(controller: descriptionController),
      groupField(),
      priceField(),
      visibleField(),
      iarcField()
    ]);
  }

  Widget _vodFields() {
    return ListView(children: [
      typeField(stream.vodType),
      tmdbField(context, () => setState(() {})),
      trailerUrlField(stream.trailerUrl),
      userScoreField(stream.userScore),
      primeDateField(stream.primeDate),
      countryField(stream.country),
      durationField(stream.duration)
    ]);
  }

  @override
  void onVodDetailsChanged(VodDetails details) {
    setState(() {
      stream.name = details.name;
      stream.description = details.description;
      stream.userScore = details.userScore;
      stream.primeDate = details.primeDate;
      stream.icon = details.icon;
    });
  }

  @override
  void onType(VodType? type) {
    setState(() {
      stream.vodType = type!;
    });
  }

  @override
  void onTrailerUrl(String term) {
    stream.trailerUrl = term;
  }

  @override
  void onUserScore(double? term) {
    stream.userScore = term!;
  }

  @override
  void onPrimeDate(int date) {
    stream.primeDate = date;
  }

  @override
  void onCountry(String term) {
    stream.country = term;
  }

  @override
  void onDuration(int date) {
    stream.duration = date;
  }
}
