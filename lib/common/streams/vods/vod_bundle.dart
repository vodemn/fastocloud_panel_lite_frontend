import 'dart:convert';

import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/flutter_common.dart';

mixin VodDialogBundle {
  VodDetails? tmdb;
  List<VodDetails> tmdbResults = [];

  TextEditingController nameController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController userScoreController = TextEditingController();
  TextEditingController iconController = TextEditingController();

  void initVod(String name, String? description, double userScore, String? icon) {
    nameController = TextEditingController(text: name);
    descriptionController = TextEditingController(text: description);
    userScoreController = TextEditingController(text: userScore.toString());
    iconController = TextEditingController(text: icon);
  }

  void onType(VodType? type);

  void onTrailerUrl(String term);

  void onUserScore(double? term);

  void onPrimeDate(int date);

  void onCountry(String term);

  void onDuration(int date);

  void onVodDetailsChanged(VodDetails details);

  Future<List<VodDetails>> tmdbRequest(BuildContext context, String text) {
    final fetcher = locator<Fetcher>();
    final defaults = fetcher.defaults();
    final response = fetcher.fetchPost(
        '/extra/tmdb/search', {'text': text, 'stream_logo_url': defaults.streamLogoIcon});
    return response.then((value) {
      final data = json.decode(value.body);
      final List<VodDetails> result = [];
      data['movies'].forEach((s) {
        final res = VodDetails.fromJson(s);
        result.add(res);
      });
      return result;
    }, onError: (error) {
      showError(context, error);
      return [];
    });
  }

  Widget typeField(VodType init) {
    return DropdownButtonEx<VodType>(
        hint: 'Movie type',
        value: init,
        values: VodType.values,
        onChanged: onType,
        itemBuilder: (VodType type) {
          return DropdownMenuItem(child: Text(type.toHumanReadable()), value: type);
        });
  }

  Widget trailerUrlField(String init) {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        minSymbols: TrailerUrl.MIN_LENGTH,
        maxSymbols: TrailerUrl.MAX_LENGTH,
        hintText: 'Trailer URL',
        init: init,
        onFieldChanged: onTrailerUrl);
  }

  Widget userScoreField(double init) {
    return NumberTextField.decimal(
        textEditingController: userScoreController,
        minDouble: UserScore.MIN,
        maxDouble: UserScore.MAX,
        hintText: 'User score',
        initDouble: init,
        onFieldChangedDouble: onUserScore);
  }

  Widget primeDateField(int init) {
    return DatePicker('Premiere date', init, onPrimeDate);
  }

  Widget countryField(String init) {
    return TextFieldEx(
        maxSymbols: Country.MAX_LENGTH,
        formatters: <TextInputFormatter>[TextFieldFilter.root],
        hintText: 'Country',
        init: init,
        onFieldChanged: onCountry);
  }

  Widget durationField(int msec) {
    final int inSeconds = msec ~/ 1000;
    return NumberTextField.integer(
        minInt: VodDuration.MIN,
        maxInt: VodDuration.MAX,
        hintText: 'Duration (sec)',
        initInt: inSeconds,
        onFieldChangedInt: (term) {
          if (term != null) {
            final int inMsec = term * 1000;
            onDuration(inMsec);
          }
        });
  }

  Widget tmdbField(BuildContext context, void Function() onRefresh) {
    final Widget tmdbWidget = DropdownButtonEx<VodDetails?>(
        padding: const EdgeInsets.all(0),
        hint: 'TMDB Results',
        value: tmdb,
        values: tmdbResults,
        onChanged: (t) {
          tmdb = t;
          nameController.text = t!.name;
          iconController.text = t.icon;
          descriptionController.text = t.description;
          userScoreController.text = t.userScore.toString();
          onVodDetailsChanged(t);
        },
        itemBuilder: (VodDetails? result) {
          return DropdownMenuItem(child: Text(result!.name), value: result);
        });
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(children: <Widget>[
          const SizedBox(width: 8),
          Expanded(child: tmdbWidget),
          IconButton(
              tooltip: 'Refresh',
              icon: const Icon(Icons.refresh),
              onPressed: () {
                final details = tmdbRequest(context, nameController.text);
                details.then((result) {
                  tmdbResults = result;
                  onRefresh();
                });
              })
        ]));
  }
}

class DatePicker extends StatefulWidget {
  final String title;
  final int initMs;
  final void Function(int) onChanged;

  const DatePicker(this.title, this.initMs, this.onChanged);

  @override
  _DatePickerState createState() {
    return _DatePickerState();
  }
}

class _DatePickerState extends State<DatePicker> {
  int _expDate = 0;

  @override
  void initState() {
    super.initState();
    _expDate = widget.initMs;
  }

  @override
  void didUpdateWidget(DatePicker oldWidget) {
    super.didUpdateWidget(oldWidget);
    _expDate = widget.initMs;
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
        title: Text(widget.title),
        subtitle: Text(DateTime.fromMillisecondsSinceEpoch(_expDate).toString()),
        onTap: () {
          final future = showDatePicker(
              context: context,
              initialDate: DateTime.fromMillisecondsSinceEpoch(widget.initMs),
              firstDate: DateTime.fromMillisecondsSinceEpoch(0),
              lastDate: DateTime.now());
          future.then((date) {
            if (date != null) {
              setState(() {
                _expDate = date.millisecondsSinceEpoch;
              });
              widget.onChanged(date.millisecondsSinceEpoch);
            }
          });
        });
  }
}
