import 'dart:convert';

import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/events/server_events.dart';
import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/localization/translations.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:fastotv_dart/json_rpc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:flutter_tags/flutter_tags.dart';

class VodDetailsPath extends VodDetails {
  static const PATH_FIELD = 'path';

  String? path;

  VodDetailsPath(
      {required String name,
      required String icon,
      required int primeDate,
      required String description,
      required double userScore,
      this.path})
      : super(
            name: name,
            icon: icon,
            primeDate: primeDate,
            description: description,
            userScore: userScore);

  factory VodDetailsPath.fromJson(Map<String, dynamic> json) {
    final VodDetails vod = VodDetails.fromJson(json);
    final String? path = json[PATH_FIELD];
    return VodDetailsPath(
        name: vod.name,
        icon: vod.icon,
        primeDate: vod.primeDate,
        description: vod.description,
        userScore: vod.userScore,
        path: path);
  }
}

class ScanFolderVods {
  static const DIRECTORY_FIELD = 'directory';
  static const EXTENSIONS_FIELD = 'extensions';
  static const STREAM_LOGO_URL = 'stream_logo_url';

  String directory;
  List<String> extensions;
  String streamLogoUrl;

  ScanFolderVods({required this.directory, required this.extensions, required this.streamLogoUrl});

  bool isValid() {
    return directory.isNotEmpty && extensions.isNotEmpty && streamLogoUrl.isNotEmpty;
  }

  factory ScanFolderVods.fromJson(Map<String, dynamic> json) {
    final directory = json[DIRECTORY_FIELD];
    final extensions = json[EXTENSIONS_FIELD];
    final logo = json[STREAM_LOGO_URL];
    return ScanFolderVods(directory: directory, extensions: extensions, streamLogoUrl: logo);
  }

  Map<String, dynamic> toJson() {
    return {
      DIRECTORY_FIELD: directory,
      EXTENSIONS_FIELD: extensions,
      STREAM_LOGO_URL: streamLogoUrl
    };
  }
}

class ScanFolderVodsDialog extends StatefulWidget {
  final String sid;
  final String streamLogoUrl;
  final OutputUrl templateOutput;
  final Stream<JsonRpcEvent> rpc;

  const ScanFolderVodsDialog(this.sid, this.rpc, this.streamLogoUrl, this.templateOutput);

  @override
  _ScanFolderDialogState createState() => _ScanFolderDialogState();
}

class _ScanFolderDialogState extends State<ScanFolderVodsDialog> {
  static const EXTENSIONS = ['m3u8', 'mp4', 'mkv', 'mov', 'mp3'];
  late ScanFolderVods _scan;
  final GlobalKey<TagsState> _tagStateKey = GlobalKey<TagsState>();
  String? oid;
  bool _scanningFolder = false;
  final List<VodDetailsPath> _vods = [];
  final List<bool> _checkValues = [];
  StreamType? _type = StreamType.VOD_RELAY;
  bool _makingRequest = false;

  @override
  void initState() {
    super.initState();
    _scan = ScanFolderVods(
        directory: '/home/fastocloud/streamer',
        extensions: [],
        streamLogoUrl: widget.streamLogoUrl);
  }

  @override
  Widget build(BuildContext context) {
    return _scanningFolder ? _filePicker() : _folderFields();
  }

  Widget _folderFields() {
    return AlertDialog(
        title: const Text('Scan folder'),
        contentPadding: const EdgeInsets.all(8),
        content: SingleChildScrollView(
            child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[_typeField(), _directoryField(), _extensionsField()])),
        actions: <Widget>[
          FlatButtonEx.notFilled(text: TR_CANCEL, onPressed: () => _cancel()),
          FlatButtonEx.filled(text: 'Scan', onPressed: () => _onScanFolderPressed())
        ]);
  }

  Widget _directoryField() {
    return TextFieldEx(
        hintText: 'Path',
        errorText: 'Enter path',
        init: _scan.directory,
        onFieldChanged: (term) {
          _scan.directory = term;
        });
  }

  Widget _extensionsField() {
    return Tags(
        key: _tagStateKey,
        textField: TagsTextField(
            hintText: 'Add extensions',
            suggestions: EXTENSIONS,
            onSubmitted: (String str) {
              setState(() {
                if (!_scan.extensions.contains(str)) {
                  _scan.extensions.add(str);
                }
              });
            }),
        itemCount: _scan.extensions.length,
        itemBuilder: (int index) {
          final item = _scan.extensions[index];
          return ItemTags(
              key: Key(index.toString()),
              index: index,
              title: item,
              combine: ItemTagsCombine.withTextBefore,
              icon: ItemTagsIcon(icon: Icons.add),
              removeButton: ItemTagsRemoveButton(onRemoved: () {
                setState(() {
                  _scan.extensions.remove(item);
                });
                return true;
              }));
        });
  }

  Widget _filePicker() {
    return AlertDialog(
        title: const Text('Scan folder'),
        contentPadding: const EdgeInsets.all(8),
        content: SingleChildScrollView(child: _filesList()),
        actions: <Widget>[_changeFolderButton(), _submitButton()]);
  }

  Widget _filesList() {
    if (oid == null || _makingRequest) {
      return const Center(child: CircularProgressIndicator());
    }
    return StreamBuilder<JsonRpcEvent>(
        stream: widget.rpc,
        builder: (context, snapshot) {
          if (snapshot.data is JsonRpcEvent) {
            final JsonRpcResponse rpc = snapshot.data!.data;
            if (oid == rpc.id) {
              if (rpc.isMessage()) {
                final result = rpc.result as Map<String, dynamic>;
                final _files = result['vods'];
                if (_files.isEmpty) {
                  return const Text('No files found');
                }
                return _VodsList(_files, _vods, _checkValues);
              } else if (rpc.isError()) {
                WidgetsBinding.instance!
                    .addPostFrameCallback((_) => _showError(rpc.error!.code, rpc.error!.message));
              }
            }
          }
          return const Center(child: CircularProgressIndicator());
        });
  }

  Widget _changeFolderButton() {
    return FlatButtonEx.notFilled(
        text: 'Change folder',
        onPressed: () {
          setState(() {
            _scanningFolder = false;
          });
        });
  }

  Widget _submitButton() {
    return FlatButtonEx.filled(text: 'Submit', onPressed: _submit);
  }

  Widget _typeField() {
    return DropdownButtonEx<StreamType?>(
        hint: _type.toString(),
        value: _type,
        values: const <StreamType>[StreamType.VOD_RELAY, StreamType.VOD_ENCODE],
        onChanged: (t) {
          setState(() {
            _type = t;
          });
        },
        itemBuilder: (StreamType? type) {
          return DropdownMenuItem(child: Text(type!.toHumanReadable()), value: type);
        });
  }

  void _onScanFolderPressed() {
    if (!_scan.isValid()) {
      return;
    }
    setState(() {
      _scanningFolder = true;
    });
    final fetcher = locator<Fetcher>();
    final response =
        fetcher.fetchPost('/server/scan_folder_vods/${widget.sid}', _scan.toJson(), [201]);
    response.then((resp) {
      final data = json.decode(resp.body);
      setState(() {
        oid = data['oid'];
      });
    }, onError: (error) {
      showError(context, error);
    });
  }

  void _cancel() => Navigator.of(context).pop();

  void _submit() {
    setState(() {
      _makingRequest = true;
    });
    final Fetcher fetcher = locator<Fetcher>();
    for (int i = 0; i < _vods.length;) {
      if (_checkValues[i]) {
        final stream = _createStream(_vods[i]);
        final response =
            fetcher.fetchPost('/server/stream/add/${widget.sid}', stream.toJson(), [201]);
        response.then((resp) {
          i++;
        }, onError: (error) {
          setState(() {
            _makingRequest = false;
          });
          showError(context, error);
          return;
        });
      }
    }
    Navigator.of(context).pop();
  }

  HardwareStream _createStream(VodDetailsPath details) {
    final fetcher = locator<Fetcher>();
    final defaults = fetcher.defaults();
    VodRelayStream streamVod;
    VodEncodeStream streamVodEncode;
    if (_type == StreamType.VOD_RELAY) {
      streamVod = VodRelayStream(
          primeDate: details.primeDate,
          icon: details.icon,
          trailerUrl: defaults.vodTrailerUrl,
          name: details.name,
          description: details.description,
          userScore: details.userScore,
          input: [InputUrl(id: 0, uri: 'file://${details.path}')],
          output: [widget.templateOutput]);
      return streamVod;
    } else {
      streamVodEncode = VodEncodeStream(
          primeDate: details.primeDate,
          icon: details.icon,
          trailerUrl: defaults.vodTrailerUrl,
          name: details.name,
          description: details.description,
          userScore: details.userScore,
          input: [InputUrl(id: 0, uri: 'file://${details.path}')],
          output: [widget.templateOutput]);
      return streamVodEncode;
    }
  }

  void _showError(int? statusCode, String? text) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(title: Text('$statusCode'), content: Text(text!, softWrap: true));
        });
  }
}

class _VodsList extends StatefulWidget {
  final List files;
  final List<VodDetailsPath> vods;
  final List<bool> checkValues;

  const _VodsList(this.files, this.vods, this.checkValues);

  @override
  _VodsListState createState() => _VodsListState();
}

class _VodsListState extends State<_VodsList> {
  @override
  void initState() {
    super.initState();
    widget.files.forEach((file) {
      final vod = VodDetailsPath.fromJson(file);
      widget.vods.add(vod);
      widget.checkValues.add(false);
    });
  }

  @override
  Widget build(BuildContext context) {
    final _tiles = List<Widget>.generate(widget.files.length, (index) {
      return _tile(index);
    });

    return Column(mainAxisSize: MainAxisSize.min, children: _tiles);
  }

  Widget _tile(int index) {
    return CheckboxListTile(
        title: Text(widget.vods[index].name),
        value: widget.checkValues[index],
        onChanged: (value) {
          onCheckBox(value!, index);
        });
  }

  void onCheckBox(bool value, int index) {
    setState(() {
      widget.checkValues[index] = value;
    });
  }
}
