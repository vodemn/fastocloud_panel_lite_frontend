import 'dart:convert';

import 'package:fastocloud_pro_panel/events/server_events.dart';
import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/localization/translations.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:fastotv_dart/json_rpc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:flutter_tags/flutter_tags.dart';

class ScanFolder {
  static const DIRECTORY_FIELD = 'directory';
  static const EXTENSIONS_FIELD = 'extensions';

  String directory;
  List<String> extensions;
  final Stream<JsonRpcEvent> rpc;

  ScanFolder({required this.rpc, required this.directory, required this.extensions});

  bool isValid() {
    return directory.isNotEmpty && extensions.isNotEmpty;
  }

  factory ScanFolder.fromJson(Stream<JsonRpcEvent> rpc, Map<String, dynamic> json) {
    final directory = json[DIRECTORY_FIELD];
    final extensions = json[EXTENSIONS_FIELD];
    return ScanFolder(rpc: rpc, directory: directory, extensions: extensions);
  }

  Map<String, dynamic> toJson() {
    return {DIRECTORY_FIELD: directory, EXTENSIONS_FIELD: extensions};
  }
}

class ScanFolderDialog extends StatefulWidget {
  final String sid;
  final Stream<JsonRpcEvent> rpc;
  final String path;
  final String Function(String file) onPrepareOutputTemplate;
  final bool visiblePath;

  const ScanFolderDialog(this.sid, this.rpc, this.onPrepareOutputTemplate,
      {this.path = '/home/fastocloud/streamer', this.visiblePath = true});

  @override
  _ScanFolderDialogState createState() {
    return _ScanFolderDialogState();
  }
}

class _ScanFolderDialogState extends State<ScanFolderDialog> {
  static const EXTENSIONS = ['m3u8', 'mp4', 'mkv', 'mov', 'mp3'];
  late ScanFolder _scan;
  final GlobalKey<TagsState> _tagStateKey = GlobalKey<TagsState>();
  String? oid;
  bool _scanningFolder = false;

  @override
  void initState() {
    super.initState();
    _scan = ScanFolder(rpc: widget.rpc, directory: widget.path, extensions: [EXTENSIONS[0]]);
  }

  @override
  Widget build(BuildContext context) {
    return _scanningFolder ? _filePicker() : _folderFields();
  }

  Widget _folderFields() {
    return AlertDialog(
        title: const Text('Scan folder'),
        contentPadding: const EdgeInsets.all(8),
        content: SingleChildScrollView(
            child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[_directoryField(), _extensionsField()])),
        actions: <Widget>[
          FlatButtonEx.notFilled(text: TR_CANCEL, onPressed: () => _cancel()),
          FlatButtonEx.filled(text: 'Scan', onPressed: () => _onScanFolderPressed())
        ]);
  }

  Widget _directoryField() {
    if (!widget.visiblePath) {
      return const SizedBox();
    }

    return TextFieldEx(
        hintText: 'Path',
        errorText: 'Enter path',
        init: _scan.directory,
        onFieldChanged: (term) {
          _scan.directory = term;
        });
  }

  Widget _extensionsField() {
    return Tags(
        key: _tagStateKey,
        textField: TagsTextField(
            hintText: 'Add extensions',
            suggestions: EXTENSIONS,
            onSubmitted: (String str) {
              setState(() {
                if (!_scan.extensions.contains(str)) {
                  _scan.extensions.add(str);
                }
              });
            }),
        itemCount: _scan.extensions.length,
        itemBuilder: (int index) {
          final item = _scan.extensions[index];
          return ItemTags(
              key: Key(index.toString()),
              index: index,
              title: item,
              combine: ItemTagsCombine.withTextBefore,
              icon: ItemTagsIcon(icon: Icons.add),
              removeButton: ItemTagsRemoveButton(onRemoved: () {
                setState(() {
                  _scan.extensions.removeAt(index);
                });
                return true;
              }));
        });
  }

  Widget _filePicker() {
    return AlertDialog(
        title: const Text('Scan folder'),
        contentPadding: const EdgeInsets.all(8),
        content: SingleChildScrollView(child: _filesList()),
        actions: <Widget>[_changeFolderButton()]);
  }

  Widget _filesList() {
    if (oid == null) {
      return const Center(child: CircularProgressIndicator());
    }
    return StreamBuilder<JsonRpcEvent>(
        stream: widget.rpc,
        builder: (context, snapshot) {
          if (snapshot.data is JsonRpcEvent) {
            final JsonRpcResponse rpc = snapshot.data!.data;
            if (oid == rpc.id) {
              if (rpc.isMessage()) {
                final result = rpc.result as Map<String, dynamic>;
                final _files = result['files'];
                if (_files.isEmpty) {
                  return const Text('No files found');
                }

                final _tiles = List<Widget>.generate(_files.length, (index) {
                  final file = _files[index];
                  return ListTile(
                      title: Text(file),
                      onTap: () {
                        final String stabled = widget.onPrepareOutputTemplate(file);
                        Navigator.of(context).pop(stabled);
                      });
                });

                return Column(mainAxisSize: MainAxisSize.min, children: _tiles);
              } else if (rpc.isError()) {
                WidgetsBinding.instance!.addPostFrameCallback((_) {
                  showError(context, ErrorExHttp(rpc.error!.code!, rpc.error!.message));
                });
              }
            }
          }
          return const Center(child: CircularProgressIndicator());
        });
  }

  Widget _changeFolderButton() {
    return FlatButtonEx.notFilled(
        text: 'Change folder',
        onPressed: () {
          setState(() {
            _scanningFolder = false;
          });
        });
  }

  void _onScanFolderPressed() {
    if (!_scan.isValid()) {
      return;
    }
    setState(() {
      _scanningFolder = true;
    });
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchPost('/server/scan_folder/${widget.sid}', _scan.toJson(), [201]);
    response.then((resp) {
      final data = json.decode(resp.body);
      setState(() {
        oid = data['oid'];
      });
    }, onError: (error) {
      showError(context, error);
    });
  }

  void _cancel() {
    return Navigator.of(context).pop();
  }
}
