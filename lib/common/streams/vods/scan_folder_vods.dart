import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/streams/vods/scan_folder_vods_dialog.dart';
import 'package:fastocloud_pro_panel/events/server_events.dart';
import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';

class ScanFolderVodsButton extends StatelessWidget {
  final Server server;
  final Stream<JsonRpcEvent> rpc;

  const ScanFolderVodsButton(this.server, this.rpc);

  @override
  Widget build(BuildContext context) {
    return FlatButtonEx.filled(
        text: 'Scan folder (nfo)', onPressed: server.isPro() ? () => _onTap(context) : null);
  }

  void _onTap(BuildContext context) {
    final fetcher = locator<Fetcher>();
    final Defaults def = fetcher.defaults();
    showDialog(
        context: context,
        builder: (context) {
          return ScanFolderVodsDialog(
              server.id!, rpc, def.streamLogoIcon, server.genTemplateVodsHttpOutputUrl(0));
        });
  }
}
