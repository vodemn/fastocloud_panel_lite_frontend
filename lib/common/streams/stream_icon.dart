import 'package:cached_network_image/cached_network_image.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';

enum PreviewType { LIVE, VOD }

class StreamIcon extends StatelessWidget {
  final String url;
  final StreamType type;

  const StreamIcon(this.url, this.type);

  @override
  Widget build(BuildContext context) {
    if (type == StreamType.VOD_RELAY ||
        type == StreamType.VOD_ENCODE ||
        type == StreamType.VOD_PROXY) {
      return PreviewIcon.vod(url, width: 40, height: 60);
    }
    return PreviewIcon.live(url, width: 40, height: 40);
  }
}

class PreviewIcon extends StatelessWidget {
  final String link;
  final double? width;
  final double? height;
  final PreviewType type;

  const PreviewIcon.live(this.link, {this.width, this.height}) : type = PreviewType.LIVE;

  const PreviewIcon.vod(this.link, {this.width, this.height}) : type = PreviewType.VOD;

  String assetsLink() {
    return 'install/assets/unknown_channel.png';
  }

  Widget defaultIcon() {
    return Image.asset(assetsLink(), height: height, width: width);
  }

  @override
  Widget build(BuildContext context) {
    return ClipRect(child: image());
  }

  Widget image() {
    return CachedNetworkImage(
        imageUrl: link,
        placeholder: (context, url) => defaultIcon(),
        errorWidget: (context, url, error) => defaultIcon(),
        width: width,
        height: height,
        imageRenderMethodForWeb: ImageRenderMethodForWeb.HtmlImage);
  }
}
