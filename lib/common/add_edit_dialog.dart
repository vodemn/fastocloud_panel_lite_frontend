import 'package:fastocloud_pro_panel/localization/translations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:responsive_builder/responsive_builder.dart';

class AddEditDialog extends StatelessWidget {
  static const double NARROW = 360;
  static const double WIDE = 720;
  final bool add;
  final void Function() onSave;
  final List<Widget> children;
  final double maxWidth;

  const AddEditDialog(
      {this.add = false, required this.children, required this.onSave, required this.maxWidth});

  const AddEditDialog.narrow({this.add = false, required this.children, required this.onSave})
      : maxWidth = NARROW;

  const AddEditDialog.wide({this.add = false, required this.children, required this.onSave})
      : maxWidth = WIDE;

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInformation) {
      return AlertDialog(
          title: Text(add ? TR_ADD : TR_EDIT),
          contentPadding: const EdgeInsets.all(8),
          content: SingleChildScrollView(
              child: SizedBox(
                  width: sizingInformation.isMobile ? double.maxFinite : maxWidth,
                  child: SingleChildScrollView(
                      child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: children)))),
          actions: <Widget>[
            FlatButtonEx.notFilled(text: TR_CANCEL, onPressed: Navigator.of(context).pop),
            FlatButtonEx.filled(text: add ? TR_ADD : TR_SAVE, onPressed: onSave)
          ]);
    });
  }

  static double getNarrow(bool isMobile) {
    return isMobile ? double.maxFinite : NARROW;
  }

  static double getWide(bool isMobile) {
    return isMobile ? double.maxFinite : WIDE;
  }
}
