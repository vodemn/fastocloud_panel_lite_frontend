import 'package:fastocloud_pro_panel/common/license_key_fields.dart';
import 'package:fastocloud_pro_panel/localization/translations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';

class LicenseDialog extends StatefulWidget {
  const LicenseDialog();

  @override
  _LicenseDialogState createState() => _LicenseDialogState();
}

class _LicenseDialogState extends State<LicenseDialog> {
  bool _canSave = false;
  String _key = '';

  @override
  Widget build(BuildContext context) {
    final actions = [
      FlatButtonEx.notFilled(text: TR_CANCEL, onPressed: () => Navigator.of(context).pop()),
      FlatButtonEx.filled(
          text: TR_ACTIVATE, onPressed: _canSave ? () => Navigator.of(context).pop(_key) : null)
    ];
    return AlertDialog(
        title: const Text('License authorization'),
        contentPadding: const EdgeInsets.all(8),
        content: SingleChildScrollView(
            child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[_keyField()])),
        actions: actions);
  }

  Widget _keyField() {
    return LicenseKeyField(
        init: _key,
        onFieldChanged: (term, isValid) {
          _key = term;
          if (isValid != _canSave) {
            setState(() {
              _canSave = isValid;
            });
          }
        });
  }
}
