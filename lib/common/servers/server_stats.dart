import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_dart_models/utils.dart';
import 'package:fastocloud_pro_panel/localization/translations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';

const CIRCLE_HEIGHT = 94.0;
const BOLD_TEXT_SIZE = 20.0;
const TILE_HEIGHT_1 = BOLD_TEXT_SIZE + 8;
const TILE_HEIGHT_2 = TILE_HEIGHT_1 * 2;
const TILE_HEIGHT_3 = TILE_HEIGHT_1 * 3;
const TILE_HEIGHT_4 = TILE_HEIGHT_1 * 4;

String _subtitle(String value) {
  return '$value: ';
}

String _formatTime(int? msSinceEpoch) {
  return DateTime.fromMillisecondsSinceEpoch(msSinceEpoch ?? 0).toString();
}

// Templates
class ServerStatsTile extends StatelessWidget {
  final String title;
  final Widget content;
  final double? height;

  const ServerStatsTile(this.title, this.content, this.height);

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(title, maxLines: 1, overflow: TextOverflow.fade),
                  const SizedBox(height: 4),
                  SizedBox(height: height, child: content)
                ])));
  }
}

class BoldText extends StatelessWidget {
  final String? title;
  final double size;

  const BoldText(this.title, {this.size = BOLD_TEXT_SIZE});

  @override
  Widget build(BuildContext context) {
    return Text(title ?? 'null', style: TextStyle(fontWeight: FontWeight.bold, fontSize: size));
  }
}

class CombinedText extends StatelessWidget {
  final String title;
  final String? data;

  const CombinedText(this.title, this.data);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[Text(title), BoldText(data)]);
  }
}

// Common tiles

class ExpDateTile extends StatelessWidget {
  static const DAY = 86400000;
  static const WEEK = 7 * DAY;
  static const MONTH = 30 * DAY;

  final int msSinceEpoch;
  final double? height;

  const ExpDateTile(int? expDate, {this.height = CIRCLE_HEIGHT}) : msSinceEpoch = expDate ?? 0;

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile(
        TR_EXP_DATE,
        Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <Widget>[
          CircleIndicator(fixedDouble(_percentage()), CIRCLE_HEIGHT,
              color: _color(), circleText: _text()),
          Flexible(child: BoldText(_time()))
        ]),
        height);
  }

  // private:
  String _time() {
    final List<String> parts = _formatTime(msSinceEpoch).split(' ');
    return "${parts.first}\n${parts.last}";
  }

  double _percentage() {
    final _now = DateTime.now().millisecondsSinceEpoch;
    if (msSinceEpoch - _now > MONTH) {
      return 100;
    } else if (msSinceEpoch > _now) {
      return MONTH / (msSinceEpoch - _now) * 100;
    } else if (msSinceEpoch == 0) {
      return 0;
    }
    return 100;
  }

  Color _color() {
    final _now = DateTime.now().millisecondsSinceEpoch;
    if (_now > msSinceEpoch) {
      return Colors.redAccent;
    } else if (msSinceEpoch - _now <= WEEK) {
      return Colors.orangeAccent;
    } else if (msSinceEpoch - _now <= MONTH) {
      return Colors.yellowAccent;
    }
    return Colors.greenAccent;
  }

  String _text() {
    final _now = DateTime.now().millisecondsSinceEpoch;
    if (msSinceEpoch > _now) {
      final days = ((msSinceEpoch - _now) / DAY).ceil();
      return '$days\ndays';
    }
    return '0\ndays';
  }
}

class HardwareStatsTile extends StatelessWidget {
  final String title;
  final double value;

  const HardwareStatsTile.cpu(this.value) : title = 'CPU';

  const HardwareStatsTile.gpu(this.value) : title = 'GPU';

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile(
        title, CircleIndicator(fixedDouble(value), CIRCLE_HEIGHT), CIRCLE_HEIGHT);
  }
}

class MemoryStatsTile extends StatelessWidget {
  final int total;
  final int free;
  final String title;

  const MemoryStatsTile.hdd(this.total, this.free) : title = TR_HDD;

  const MemoryStatsTile.memory(this.total, this.free) : title = TR_MEMORY;

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile(
        title,
        Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <Widget>[
          CircleIndicator(fixedDouble(((total - free) / total) * 100), CIRCLE_HEIGHT),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
            CombinedText(_subtitle(TR_MEMORY_USED), _format(total - free)),
            CombinedText(_subtitle(TR_MEMORY_FREE), _format(free)),
            CombinedText(_subtitle(TR_MEMORY_TOTAL), _format(total))
          ])
        ]),
        CIRCLE_HEIGHT);
  }

  String _format(int value) {
    final double inGb = fixedDouble(value / (1024 * 1024 * 1024));
    return '$inGb Gb';
  }
}

class NetworkStatsTile extends StatelessWidget {
  final int bandwidthIn;
  final int bandwidthOut;
  final double height;

  const NetworkStatsTile(this.bandwidthIn, this.bandwidthOut, {double? height})
      : height = height ?? TILE_HEIGHT_2;

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile(TR_NETWORK, height < TILE_HEIGHT_2 ? _row() : _column(), height);
  }

  // private:
  String _format(int value) {
    final double inMegabytes = fixedDouble(8 * value / (1024 * 1024));
    return '$inMegabytes Mb/s';
  }

  Widget _column() {
    return Column(children: <Widget>[Expanded(child: _in), Expanded(child: _out)]);
  }

  Widget _row() {
    return Row(children: <Widget>[Expanded(child: _in), Expanded(child: _out)]);
  }

  Widget get _in {
    return Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[const Icon(Icons.arrow_downward), BoldText(_format(bandwidthIn))]);
  }

  Widget get _out {
    return Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[const Icon(Icons.arrow_upward), BoldText(_format(bandwidthOut))]);
  }
}

class OnlineUsersEpgStatsTile extends StatelessWidget {
  final int? daemon;
  final double? height;

  const OnlineUsersEpgStatsTile(this.daemon, {this.height = TILE_HEIGHT_4});

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile(TR_ONLINE_USERS, CombinedText(_subtitle('Daemon'), '$daemon'), height);
  }
}

class OnlineUsersLBStatsTile extends StatelessWidget {
  final int? daemon;
  final int? subscribers;
  final double? height;

  const OnlineUsersLBStatsTile(this.daemon, this.subscribers, {this.height = TILE_HEIGHT_4});

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile(
        TR_ONLINE_USERS,
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
          CombinedText(_subtitle('Daemon'), '$daemon'),
          CombinedText(_subtitle(TR_SUBSCRIBERS), '$subscribers')
        ]),
        height);
  }
}

class OnlineUsersServerStatsTile extends StatelessWidget {
  final int? daemon;
  final int? cods;
  final int? vods;
  final int? http;
  final double height;

  const OnlineUsersServerStatsTile.server(this.daemon, this.cods, this.vods, this.http,
      {double? height})
      : height = height ?? TILE_HEIGHT_4;

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile(TR_ONLINE_USERS, height < TILE_HEIGHT_4 ? _rows() : _columns(), height);
  }

  // private:
  Widget _rows() {
    Widget _rowItem(Widget item1, Widget item2) {
      return Expanded(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[item1, item2]));
    }

    return Row(children: <Widget>[
      _rowItem(_daemon, _cods),
      const SizedBox(width: 4),
      _rowItem(_vods, _http)
    ]);
  }

  Widget _columns() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[_daemon, _cods, _vods, _http]);
  }

  Widget get _daemon {
    return CombinedText(_subtitle('Daemon'), '$daemon');
  }

  Widget get _cods {
    return CombinedText(_subtitle('CODs'), '$cods');
  }

  Widget get _vods {
    return CombinedText(_subtitle('VODs'), '$vods');
  }

  Widget get _http {
    return CombinedText(_subtitle('Http'), '$http');
  }
}

class OsStatsTile extends StatelessWidget {
  final OperationSystem? os;
  final double? height;

  const OsStatsTile(this.os, {this.height = TILE_HEIGHT_4});

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile(
        TR_OS,
        Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Expanded(
              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
            CombinedText(_subtitle(TR_OS_NAME), os?.name),
            CombinedText(_subtitle(TR_OS_ARCH), os?.arch),
            _version()
          ])),
          _logo()
        ]),
        height);
  }

  // private:
  Widget _version() {
    return Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const SizedBox(height: TILE_HEIGHT_1, child: Center(child: Text('$TR_VERSION: '))),
          Expanded(
              child: Text(os?.version ?? '',
                  style: const TextStyle(fontWeight: FontWeight.bold, fontSize: BOLD_TEXT_SIZE),
                  softWrap: true,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis))
        ]);
  }

  Widget _logo() {
    if (os?.name == null) {
      return const SizedBox();
    } else {
      return CustomAssetLogo('install/assets/platforms/${_formatName(os!.name)}.png');
    }
  }

  String _formatName(String name) {
    return name.toLowerCase().replaceAll(RegExp(' '), '_');
  }
}

class StatusTile extends StatelessWidget {
  final ServerStatus? status;
  final double? height;

  const StatusTile(this.status, {this.height = TILE_HEIGHT_1});

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile(TR_STATUS, BoldText(status.toString()), height);
  }
}

class SyncTimeTile extends StatelessWidget {
  final int? syncTimeMs;
  final int timeStampMs;
  final double? height;

  const SyncTimeTile(this.syncTimeMs, this.timeStampMs, {this.height = TILE_HEIGHT_4});

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile(
        '$TR_SYNCTIME / $TR_TIMESTAMP',
        Column(children: [BoldText(_formatTime(syncTimeMs)), BoldText(_formatTime(timeStampMs))]),
        height);
  }
}

class UptimeStatsTile extends StatelessWidget {
  final int sec;
  final double? height;

  const UptimeStatsTile(this.sec, {this.height = TILE_HEIGHT_2});

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile(TR_UPTIME, BoldText(_format), height);
  }

  String get _format {
    final d = Duration(seconds: sec);
    int seconds = d.inSeconds;
    final int days = seconds ~/ Duration.secondsPerDay;
    seconds -= days * Duration.secondsPerDay;
    final int hours = seconds ~/ Duration.secondsPerHour;
    seconds -= hours * Duration.secondsPerHour;
    final int minutes = seconds ~/ Duration.secondsPerMinute;
    seconds -= minutes * Duration.secondsPerMinute;

    final List<String> output = [];
    if (days != 0) {
      output.add('${days}d');
    }
    if (output.isNotEmpty || hours != 0) {
      output.add('${hours}h');
    }
    if (output.isNotEmpty || minutes != 0) {
      output.add('${minutes}m');
    }
    output.add('${seconds}s');

    return output.join(':');
  }
}

class VersionTile extends StatelessWidget {
  final String? project;
  final String? version;
  final double? height;

  const VersionTile(this.project, this.version, {this.height = TILE_HEIGHT_4});

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile('$TR_VERSION ($project)', BoldText(version), height);
  }
}
