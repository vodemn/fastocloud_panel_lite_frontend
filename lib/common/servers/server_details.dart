import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:responsive_builder/responsive_builder.dart';

class ServerDetailsBuilder extends StatefulWidget {
  final Widget stats;
  final Widget table;
  final String tableTitle;

  const ServerDetailsBuilder(this.stats, this.table, this.tableTitle);

  @override
  State createState() {
    return _ServerDetailsBuilder();
  }
}

class _ServerDetailsBuilder extends State<ServerDetailsBuilder> {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(desktop: desktop(), mobile: mobile());
  }

  Widget mobile() {
    return _MobileLayout(widget.stats, widget.table, widget.tableTitle);
  }

  Widget desktop() {
    return _DesktopLayout(widget.stats, widget.table);
  }
}

// layouts
class _MobileLayout extends StatefulWidget {
  final Widget stats;
  final Widget table;
  final String tableTitle;

  const _MobileLayout(this.stats, this.table, this.tableTitle);

  @override
  _MobileLayoutState createState() {
    return _MobileLayoutState();
  }
}

class _MobileLayoutState extends State<_MobileLayout> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      TabBar(
          controller: _tabController,
          tabs: [const Tab(text: 'Statistics'), Tab(text: widget.tableTitle)],
          labelColor: Colors.black),
      Expanded(
          child: TabBarView(controller: _tabController, children: [
        SingleChildScrollView(
            child: Padding(padding: const EdgeInsets.only(top: 4), child: widget.stats)),
        SingleChildScrollView(
            child:
                Padding(padding: const EdgeInsets.symmetric(horizontal: 8.0), child: widget.table))
      ]))
    ]);
  }
}

class _DesktopLayout extends StatelessWidget {
  final Widget stats;
  final Widget table;

  const _DesktopLayout(this.stats, this.table);

  @override
  Widget build(BuildContext context) {
    return ScrollableEx.withBar(builder: (controller) {
      return SingleChildScrollView(
          controller: controller,
          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[stats, table]));
    });
  }
}
