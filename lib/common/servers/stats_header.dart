import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/servers/license_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:responsive_builder/responsive_builder.dart';

class StatsButtons extends StatelessWidget {
  final String title;
  final List<Widget> buttons;

  const StatsButtons(this.title, this.buttons);

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(mobile: mobile(), desktop: desktop());
  }

  Widget mobile() {
    return _buttonsRow();
  }

  Widget desktop() {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          const SizedBox(width: 8),
          Text(title, style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          Expanded(child: Align(alignment: Alignment.centerRight, child: _buttonsRow()))
        ]));
  }

  Widget _buttonsRow() {
    return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Row(children: _wrapButtons())));
  }

  List<Widget> _wrapButtons() {
    final List<Widget> result = [];
    for (final widget in buttons) {
      result.add(Padding(padding: const EdgeInsets.symmetric(horizontal: 8), child: widget));
    }

    return result;
  }
}

abstract class StatsHeader extends StatelessWidget {
  final ServerStatus? status;

  const StatsHeader(this.status);

  String get title => 'Server stats';

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
        mobile: _buttonsRow(context),
        desktop: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              const SizedBox(width: 8),
              Text(title, style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
              Expanded(child: Align(alignment: Alignment.centerRight, child: _buttonsRow(context)))
            ])));
  }

  Widget _buttonsRow(BuildContext context) {
    return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Row(children: buttons(context))));
  }

  List<StatsButton> buttons(BuildContext context) {
    return <StatsButton>[
      connectionButton(context),
      activateButton(context),
      getLogButton(context),
      viewLogButton(context),
      historyButton(context)
    ];
  }

  // buttons
  StatsButton connectionButton(BuildContext context) {
    if (status == ServerStatus.INIT) {
      return StatsButton(
          title: 'Connect',
          onPressed: () {
            connect(context);
          });
    } else {
      return StatsButton(
          title: 'Disconnect',
          onPressed: () {
            disconnect(context);
          });
    }
  }

  StatsButton activateButton(BuildContext context) {
    return StatsButton(
        title: 'Activate',
        onPressed: status == ServerStatus.INIT
            ? null
            : () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return const LicenseDialog();
                    }).then((_key) {
                  if (_key != null) {
                    activate(context, _key);
                  }
                });
              });
  }

  StatsButton getLogButton(BuildContext context) {
    return StatsButton(
        title: 'Get log',
        onPressed: status != ServerStatus.ACTIVE
            ? null
            : () {
                getLog(context);
              });
  }

  StatsButton viewLogButton(BuildContext context) {
    return StatsButton(
        title: 'View log',
        onPressed: () {
          viewLog(context);
        });
  }

  StatsButton historyButton(BuildContext context) {
    return StatsButton(
        title: 'History',
        onPressed: () {
          toHistory(context);
        });
  }

  void connect(BuildContext context);

  void disconnect(BuildContext context);

  void activate(BuildContext context, String key);

  void getLog(BuildContext context);

  void viewLog(BuildContext context);

  void toHistory(BuildContext context);
}

class StatsButton extends StatelessWidget {
  final String title;
  final VoidCallback? onPressed;

  const StatsButton({required this.title, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: FlatButtonEx.filled(text: title, onPressed: onPressed));
  }
}
