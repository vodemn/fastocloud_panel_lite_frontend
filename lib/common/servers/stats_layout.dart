import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/common/servers/server_stats.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

abstract class StatsLayout extends StatelessWidget {
  const StatsLayout();

  ServerStatus? get status;

  int? get syncTime;

  int get timestamp;

  String? get project;

  String? get version;

  double get cpu;

  double get gpu;

  int get memoryTotal;

  int get memoryFree;

  int get hddTotal;

  int get hddFree;

  int get bandwidthIn;

  int get bandwidthOut;

  int get uptime;

  int? get expirationTime;

  OperationSystem? get os;

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(desktop: _desktop(), mobile: _mobile());
  }

  Widget _desktop() {
    const height1 = TILE_HEIGHT_1;
    const height2 = TILE_HEIGHT_4;
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Row(children: <Widget>[
        Expanded(child: _status(height: height1)),
        Expanded(flex: 2, child: _uptime(height: height1)),
        Expanded(flex: 3, child: _version(height: height1)),
        Expanded(flex: 3, child: _network(height: height1))
      ]),
      Row(children: <Widget>[
        Expanded(flex: 11, child: _syncAndStamp(height: height2)),
        Expanded(flex: 20, child: _expTime(height: height2)),
        Expanded(flex: 9, child: onlineUsers(height: height2)),
        Expanded(flex: 20, child: _os(height: height2))
      ]),
      Row(children: <Widget>[
        Expanded(flex: 2, child: _memory()),
        Expanded(flex: 2, child: _hdd()),
        Expanded(child: _cpu()),
        Expanded(child: _gpu())
      ])
    ]);
  }

  Widget _mobile() {
    return SingleChildScrollView(
        child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              Row(children: <Widget>[
                Expanded(child: _status()),
                Expanded(flex: 2, child: _uptime())
              ]),
              Row(children: <Widget>[Expanded(child: _expTime(height: CIRCLE_HEIGHT))]),
              Row(children: <Widget>[Expanded(child: _network(height: TILE_HEIGHT_1))]),
              Row(children: <Widget>[
                Expanded(child: _syncAndStamp()),
                Expanded(child: _version(height: TILE_HEIGHT_4))
              ]),
              Row(children: <Widget>[
                Expanded(child: onlineUsers()),
                Expanded(child: _os(height: TILE_HEIGHT_4))
              ]),
              Row(children: <Widget>[Expanded(flex: 2, child: _memory()), Expanded(child: _cpu())]),
              Row(children: <Widget>[Expanded(flex: 2, child: _hdd()), Expanded(child: _gpu())])
            ])));
  }

  Widget _status({double? height}) {
    return StatusTile(status, height: height);
  }

  Widget _syncAndStamp({double? height}) {
    return SyncTimeTile(syncTime, timestamp, height: height);
  }

  Widget _version({double? height}) {
    return VersionTile(project, version, height: height);
  }

  Widget onlineUsers({double? height});

  Widget _cpu() {
    return HardwareStatsTile.cpu(cpu);
  }

  Widget _gpu() {
    return HardwareStatsTile.gpu(gpu);
  }

  Widget _memory() {
    return MemoryStatsTile.memory(memoryTotal, memoryFree);
  }

  Widget _hdd() {
    return MemoryStatsTile.hdd(hddTotal, hddFree);
  }

  Widget _network({double? height}) {
    return NetworkStatsTile(bandwidthIn, bandwidthOut, height: height);
  }

  Widget _uptime({double? height}) {
    return UptimeStatsTile(uptime, height: height);
  }

  Widget _expTime({double? height}) {
    return ExpDateTile(expirationTime, height: height);
  }

  Widget _os({double? height}) {
    return OsStatsTile(os, height: height);
  }
}
