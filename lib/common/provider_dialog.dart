import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/dashboard/providers/providers_loader.dart';
import 'package:fastocloud_pro_panel/fetcher.dart';
import 'package:fastocloud_pro_panel/localization/translations.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

class AddRemoveProviderDialog extends StatefulWidget {
  final String sid;
  final String path;
  final String title;
  final List<ServerProvider> providers;
  final bool _add;

  const AddRemoveProviderDialog.add(this.sid, this.path, this.providers)
      : _add = true,
        title = 'Add provider';

  const AddRemoveProviderDialog.remove(this.sid, this.path, this.providers)
      : _add = false,
        title = 'Remove provider';

  bool isAdd() {
    return _add;
  }

  @override
  _AddRemoveProviderDialogState createState() {
    return _AddRemoveProviderDialogState();
  }
}

class _AddRemoveProviderDialogState extends State<AddRemoveProviderDialog> {
  ServerProvider? _provider;
  ProviderRole _role = ProviderRole.READ;
  final ProvidersLoader _loader = ProvidersLoader();
  List<ServerProvider>? availableProviders;

  @override
  void initState() {
    super.initState();
    _loader.load();
  }

  @override
  void dispose() {
    _loader.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LoaderWidget<ProvidersDataState>(
        loader: _loader,
        builder: (context, state) {
          if (availableProviders == null) {
            if (widget.isAdd()) {
              availableProviders = [];
              for (final Provider provider in state.data) {
                final ServerProvider? serverProvider = findServerProvider(provider);
                if (serverProvider == null) {
                  final fake = ServerProvider(
                      id: provider.id, email: provider.email, role: ProviderRole.READ);
                  availableProviders!.add(fake);
                }
              }
            } else {
              availableProviders = widget.providers;
            }
          }
          if (availableProviders!.isNotEmpty) {
            _provider = availableProviders![0];
          }
          return AlertDialog(
              title: Text(widget.title),
              contentPadding: const EdgeInsets.all(8),
              content: SingleChildScrollView(child: _body()),
              actions: _actions());
        });
  }

  Widget _body() {
    if (_provider == null) {
      return const Padding(padding: EdgeInsets.all(16.0), child: Text('No providers found'));
    }

    return Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_nameField(availableProviders!), if (widget.isAdd()) _roleField()]);
  }

  List<Widget> _actions() {
    if (_provider == null) {
      return <Widget>[FlatButtonEx.notFilled(text: TR_CLOSE, onPressed: _cancel)];
    }

    return <Widget>[
      FlatButtonEx.notFilled(text: TR_CANCEL, onPressed: _cancel),
      FlatButtonEx.filled(text: widget.isAdd() ? TR_ADD : TR_REMOVE, onPressed: _save)
    ];
  }

  ServerProvider? findServerProvider(Provider provider) {
    for (final ServerProvider member in widget.providers) {
      if (member.id == provider.id) {
        return member;
      }
    }

    return null;
  }

  Widget _nameField(List<ServerProvider> providers) {
    return DropdownButtonEx<ServerProvider>(
        hint: _provider!.email,
        value: _provider!,
        values: providers,
        onChanged: (prov) => setState(() {
              _provider = prov!;
            }),
        itemBuilder: (ServerProvider provider) {
          return DropdownMenuItem<ServerProvider>(child: Text(provider.email!), value: provider);
        });
  }

  Widget _roleField() {
    return DropdownButtonEx<ProviderRole>(
        hint: _role.toHumanReadable(),
        value: _role,
        values: ProviderRole.values,
        onChanged: (role) => setState(() {
              _role = role!;
            }),
        itemBuilder: (ProviderRole role) {
          return DropdownMenuItem<ProviderRole>(child: Text(role.toHumanReadable()), value: role);
        });
  }

  void _cancel() {
    Navigator.of(context).pop();
  }

  void _save() {
    if (_provider!.email!.isEmpty) {
      return;
    }

    final stabled = _provider!.email!.toLowerCase().trim();
    final Fetcher fetcher = locator<Fetcher>();
    final response = widget.isAdd()
        ? fetcher.fetchPost(
            '/${widget.path}/provider/add/${widget.sid}', {'email': stabled, 'role': _role.toInt()})
        : fetcher.fetchPatch('/${widget.path}/provider/remove/${widget.sid}', {'email': stabled});
    response.then((_) {
      Navigator.of(context).pop();
    }, onError: (error) {
      showError(context, error);
    });
  }
}
