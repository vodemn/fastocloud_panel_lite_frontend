import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_pro_panel/dashboard/dashboard_socket.dart';
import 'package:fastocloud_pro_panel/service_locator.dart';
import 'package:fastocloud_pro_panel/shared_prefs.dart';
import 'package:flutter/material.dart';

class ProfileInfo extends StatefulWidget {
  final Widget child;

  const ProfileInfo({required this.child});

  static _ProfileInfoState? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<_InheritedProfile>()!.data;
  }

  @override
  _ProfileInfoState createState() => _ProfileInfoState();
}

class _ProfileInfoState extends State<ProfileInfo> {
  Provider? _profile;
  LogSocket? _socket;

  Provider? get profile => _profile;

  @override
  Widget build(BuildContext context) {
    return _InheritedProfile(data: this, child: widget.child);
  }

  void updateProfile(Provider newProfile) {
    if (_profile == null) {
      _socket?.dispose();
      _socket = LogSocket(newProfile.id!);
      setState(() {
        _profile = newProfile;
      });
    }
  }

  void logout() {
    final settings = locator<LocalStorageService>();
    settings.setCheck(null);
    settings.setEmail(null);
    settings.setPassword(null);
    _socket?.dispose();
    setState(() {
      _profile = null;
    });
  }
}

class _InheritedProfile extends InheritedWidget {
  final _ProfileInfoState? data;

  const _InheritedProfile({
    this.data,
    Key? key,
    required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_InheritedProfile oldWidget) {
    return true;
  }
}
