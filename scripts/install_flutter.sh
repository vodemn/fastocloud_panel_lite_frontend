#!/bin/bash
set -ex

FLUTTER_HOME=$HOME/flutter
FLUTTER_BIN=$FLUTTER_HOME/bin

git clone https://github.com/flutter/flutter.git -b beta $FLUTTER_HOME

export PATH=$PATH:$FLUTTER_BIN

echo $"export PATH=\$PATH:$FLUTTER_BIN" >> $HOME/.bashrc

source $HOME/.bashrc

flutter config --enable-web
